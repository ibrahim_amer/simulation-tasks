﻿namespace InventorySystem.UI
{
    partial class SimulationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Chart = new System.Windows.Forms.TabControl();
            this.Inputs_tab = new System.Windows.Forms.TabPage();
            this.Simulate_btn = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.PoorDemandDist_Mean_txtbox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.PoorDemandDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.PoorDemandDist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.PoorDemandDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.PoorDemandDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.PoorDemandDist_Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.FairDemandDist_Mean_txtbox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.FairDemandDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.FairDemandDist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.FairDemandDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.FairDemandDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.FairDemandDist_UniformA_rbtn = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.GoodDemandDist_Mean_txtbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.GoodDemandDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.GoodDemandDist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.GoodDemandDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.GoodDemandDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.GoodDemandDist_Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.DemandProbability_dataGridView = new System.Windows.Forms.DataGridView();
            this.Demand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Good = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Fair = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Poor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.NewsDayTypeDist_Mean_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.NewsDayTypeDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.NewsDayTypeDist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.NewsDayTypeDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.NewsDayTypeDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.NewsDayTypeDist_Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.NewsDayType_dataGridView = new System.Windows.Forms.DataGridView();
            this.TypeOfNewsday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Probability = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BundlesTo_textBox = new System.Windows.Forms.TextBox();
            this.BundlesFrom_textBox = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NoOfDays_txtbox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PurchasePrice_textBox = new System.Windows.Forms.TextBox();
            this.ScrapValue_textBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SellingPrice_txtBox = new System.Windows.Forms.TextBox();
            this.Outputs_tabPage2 = new System.Windows.Forms.TabPage();
            this.NetProfit_txtbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.DaysFromTo_ComboBox = new System.Windows.Forms.ComboBox();
            this.NumberOfDaysHavingUnsoldPapers_txtbox = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.NumberOfDaysHavingExcessDemand_txtbox = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.TotalSalvageFrom_txtbox = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.TotalLostProfitFromExcessDemand_txtbox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.TotalRevenueFromSales_txtbox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Output_dataGridView = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this.Chart.SuspendLayout();
            this.Inputs_tab.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DemandProbability_dataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewsDayType_dataGridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.Outputs_tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Output_dataGridView)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Chart
            // 
            this.Chart.Controls.Add(this.Inputs_tab);
            this.Chart.Controls.Add(this.Outputs_tabPage2);
            this.Chart.Controls.Add(this.tabPage1);
            this.Chart.Controls.Add(this.tabPage2);
            this.Chart.Location = new System.Drawing.Point(12, 12);
            this.Chart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Chart.Name = "Chart";
            this.Chart.SelectedIndex = 0;
            this.Chart.Size = new System.Drawing.Size(1788, 807);
            this.Chart.TabIndex = 0;
            // 
            // Inputs_tab
            // 
            this.Inputs_tab.Controls.Add(this.Simulate_btn);
            this.Inputs_tab.Controls.Add(this.groupBox4);
            this.Inputs_tab.Controls.Add(this.groupBox2);
            this.Inputs_tab.Controls.Add(this.groupBox1);
            this.Inputs_tab.Location = new System.Drawing.Point(4, 25);
            this.Inputs_tab.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Inputs_tab.Name = "Inputs_tab";
            this.Inputs_tab.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Inputs_tab.Size = new System.Drawing.Size(1780, 778);
            this.Inputs_tab.TabIndex = 0;
            this.Inputs_tab.Text = "Inputs";
            this.Inputs_tab.UseVisualStyleBackColor = true;
            // 
            // Simulate_btn
            // 
            this.Simulate_btn.Location = new System.Drawing.Point(1516, 596);
            this.Simulate_btn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Simulate_btn.Name = "Simulate_btn";
            this.Simulate_btn.Size = new System.Drawing.Size(139, 62);
            this.Simulate_btn.TabIndex = 15;
            this.Simulate_btn.Text = "Simulate";
            this.Simulate_btn.UseVisualStyleBackColor = true;
            this.Simulate_btn.Click += new System.EventHandler(this.Simulate_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.groupBox7);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.DemandProbability_dataGridView);
            this.groupBox4.Location = new System.Drawing.Point(420, 6);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox4.Size = new System.Drawing.Size(1051, 697);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Demand probability distribution ";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(163, 574);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(105, 42);
            this.button5.TabIndex = 16;
            this.button5.Text = "Clear All";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.ClearAll_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.PoorDemandDist_Mean_txtbox);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.PoorDemandDist_UniformB_txtbox);
            this.groupBox7.Controls.Add(this.PoorDemandDist_UniformA_txtbox);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Controls.Add(this.PoorDemandDist_Uniform_rbtn);
            this.groupBox7.Controls.Add(this.PoorDemandDist_Exponential_rbtn);
            this.groupBox7.Controls.Add(this.PoorDemandDist_Discrete_rbtn);
            this.groupBox7.Location = new System.Drawing.Point(573, 443);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox7.Size = new System.Drawing.Size(360, 158);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Distribution Of Poor Type";
            // 
            // PoorDemandDist_Mean_txtbox
            // 
            this.PoorDemandDist_Mean_txtbox.Location = new System.Drawing.Point(267, 84);
            this.PoorDemandDist_Mean_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_Mean_txtbox.Name = "PoorDemandDist_Mean_txtbox";
            this.PoorDemandDist_Mean_txtbox.Size = new System.Drawing.Size(55, 22);
            this.PoorDemandDist_Mean_txtbox.TabIndex = 2;
            this.PoorDemandDist_Mean_txtbox.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(219, 87);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 17);
            this.label16.TabIndex = 14;
            this.label16.Text = "Mean";
            this.label16.Visible = false;
            // 
            // PoorDemandDist_UniformB_txtbox
            // 
            this.PoorDemandDist_UniformB_txtbox.Location = new System.Drawing.Point(145, 121);
            this.PoorDemandDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_UniformB_txtbox.Name = "PoorDemandDist_UniformB_txtbox";
            this.PoorDemandDist_UniformB_txtbox.Size = new System.Drawing.Size(61, 22);
            this.PoorDemandDist_UniformB_txtbox.TabIndex = 2;
            this.PoorDemandDist_UniformB_txtbox.Visible = false;
            // 
            // PoorDemandDist_UniformA_txtbox
            // 
            this.PoorDemandDist_UniformA_txtbox.Location = new System.Drawing.Point(145, 82);
            this.PoorDemandDist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_UniformA_txtbox.Name = "PoorDemandDist_UniformA_txtbox";
            this.PoorDemandDist_UniformA_txtbox.Size = new System.Drawing.Size(61, 22);
            this.PoorDemandDist_UniformA_txtbox.TabIndex = 3;
            this.PoorDemandDist_UniformA_txtbox.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(123, 121);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(16, 17);
            this.label17.TabIndex = 2;
            this.label17.Text = "b";
            this.label17.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(123, 82);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(16, 17);
            this.label18.TabIndex = 3;
            this.label18.Text = "a";
            this.label18.Visible = false;
            // 
            // PoorDemandDist_Uniform_rbtn
            // 
            this.PoorDemandDist_Uniform_rbtn.AutoSize = true;
            this.PoorDemandDist_Uniform_rbtn.Location = new System.Drawing.Point(125, 39);
            this.PoorDemandDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_Uniform_rbtn.Name = "PoorDemandDist_Uniform_rbtn";
            this.PoorDemandDist_Uniform_rbtn.Size = new System.Drawing.Size(78, 21);
            this.PoorDemandDist_Uniform_rbtn.TabIndex = 2;
            this.PoorDemandDist_Uniform_rbtn.TabStop = true;
            this.PoorDemandDist_Uniform_rbtn.Text = "Uniform";
            this.PoorDemandDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.PoorDemandDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.PoorDemandDist_Uniform_rbtn_CheckedChanged);
            // 
            // PoorDemandDist_Exponential_rbtn
            // 
            this.PoorDemandDist_Exponential_rbtn.AutoSize = true;
            this.PoorDemandDist_Exponential_rbtn.Location = new System.Drawing.Point(221, 39);
            this.PoorDemandDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_Exponential_rbtn.Name = "PoorDemandDist_Exponential_rbtn";
            this.PoorDemandDist_Exponential_rbtn.Size = new System.Drawing.Size(102, 21);
            this.PoorDemandDist_Exponential_rbtn.TabIndex = 1;
            this.PoorDemandDist_Exponential_rbtn.TabStop = true;
            this.PoorDemandDist_Exponential_rbtn.Text = "Exponential";
            this.PoorDemandDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.PoorDemandDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.PoorDemandDist_Exponential_rbtn_CheckedChanged);
            // 
            // PoorDemandDist_Discrete_rbtn
            // 
            this.PoorDemandDist_Discrete_rbtn.AutoSize = true;
            this.PoorDemandDist_Discrete_rbtn.Location = new System.Drawing.Point(21, 39);
            this.PoorDemandDist_Discrete_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PoorDemandDist_Discrete_rbtn.Name = "PoorDemandDist_Discrete_rbtn";
            this.PoorDemandDist_Discrete_rbtn.Size = new System.Drawing.Size(81, 21);
            this.PoorDemandDist_Discrete_rbtn.TabIndex = 0;
            this.PoorDemandDist_Discrete_rbtn.TabStop = true;
            this.PoorDemandDist_Discrete_rbtn.Text = "Discrete";
            this.PoorDemandDist_Discrete_rbtn.UseVisualStyleBackColor = true;
            this.PoorDemandDist_Discrete_rbtn.CheckedChanged += new System.EventHandler(this.PoorDemandDist_Discrete_rbtn_CheckedChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.FairDemandDist_Mean_txtbox);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.FairDemandDist_UniformB_txtbox);
            this.groupBox6.Controls.Add(this.FairDemandDist_UniformA_txtbox);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.FairDemandDist_Uniform_rbtn);
            this.groupBox6.Controls.Add(this.FairDemandDist_Exponential_rbtn);
            this.groupBox6.Controls.Add(this.FairDemandDist_UniformA_rbtn);
            this.groupBox6.Location = new System.Drawing.Point(573, 241);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox6.Size = new System.Drawing.Size(360, 158);
            this.groupBox6.TabIndex = 15;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Distribution Of Fair Type";
            // 
            // FairDemandDist_Mean_txtbox
            // 
            this.FairDemandDist_Mean_txtbox.Location = new System.Drawing.Point(267, 84);
            this.FairDemandDist_Mean_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_Mean_txtbox.Name = "FairDemandDist_Mean_txtbox";
            this.FairDemandDist_Mean_txtbox.Size = new System.Drawing.Size(55, 22);
            this.FairDemandDist_Mean_txtbox.TabIndex = 2;
            this.FairDemandDist_Mean_txtbox.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(219, 87);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 17);
            this.label13.TabIndex = 14;
            this.label13.Text = "Mean";
            this.label13.Visible = false;
            // 
            // FairDemandDist_UniformB_txtbox
            // 
            this.FairDemandDist_UniformB_txtbox.Location = new System.Drawing.Point(145, 121);
            this.FairDemandDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_UniformB_txtbox.Name = "FairDemandDist_UniformB_txtbox";
            this.FairDemandDist_UniformB_txtbox.Size = new System.Drawing.Size(61, 22);
            this.FairDemandDist_UniformB_txtbox.TabIndex = 2;
            this.FairDemandDist_UniformB_txtbox.Visible = false;
            // 
            // FairDemandDist_UniformA_txtbox
            // 
            this.FairDemandDist_UniformA_txtbox.Location = new System.Drawing.Point(145, 82);
            this.FairDemandDist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_UniformA_txtbox.Name = "FairDemandDist_UniformA_txtbox";
            this.FairDemandDist_UniformA_txtbox.Size = new System.Drawing.Size(61, 22);
            this.FairDemandDist_UniformA_txtbox.TabIndex = 3;
            this.FairDemandDist_UniformA_txtbox.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(123, 121);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(16, 17);
            this.label14.TabIndex = 2;
            this.label14.Text = "b";
            this.label14.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(123, 82);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(16, 17);
            this.label15.TabIndex = 3;
            this.label15.Text = "a";
            this.label15.Visible = false;
            // 
            // FairDemandDist_Uniform_rbtn
            // 
            this.FairDemandDist_Uniform_rbtn.AutoSize = true;
            this.FairDemandDist_Uniform_rbtn.Location = new System.Drawing.Point(125, 39);
            this.FairDemandDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_Uniform_rbtn.Name = "FairDemandDist_Uniform_rbtn";
            this.FairDemandDist_Uniform_rbtn.Size = new System.Drawing.Size(78, 21);
            this.FairDemandDist_Uniform_rbtn.TabIndex = 2;
            this.FairDemandDist_Uniform_rbtn.TabStop = true;
            this.FairDemandDist_Uniform_rbtn.Text = "Uniform";
            this.FairDemandDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.FairDemandDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.FairDemandDist_Uniform_rbtn_CheckedChanged);
            // 
            // FairDemandDist_Exponential_rbtn
            // 
            this.FairDemandDist_Exponential_rbtn.AutoSize = true;
            this.FairDemandDist_Exponential_rbtn.Location = new System.Drawing.Point(221, 39);
            this.FairDemandDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_Exponential_rbtn.Name = "FairDemandDist_Exponential_rbtn";
            this.FairDemandDist_Exponential_rbtn.Size = new System.Drawing.Size(102, 21);
            this.FairDemandDist_Exponential_rbtn.TabIndex = 1;
            this.FairDemandDist_Exponential_rbtn.TabStop = true;
            this.FairDemandDist_Exponential_rbtn.Text = "Exponential";
            this.FairDemandDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.FairDemandDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.FairDemandDist_Exponential_rbtn_CheckedChanged);
            // 
            // FairDemandDist_UniformA_rbtn
            // 
            this.FairDemandDist_UniformA_rbtn.AutoSize = true;
            this.FairDemandDist_UniformA_rbtn.Location = new System.Drawing.Point(21, 39);
            this.FairDemandDist_UniformA_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FairDemandDist_UniformA_rbtn.Name = "FairDemandDist_UniformA_rbtn";
            this.FairDemandDist_UniformA_rbtn.Size = new System.Drawing.Size(81, 21);
            this.FairDemandDist_UniformA_rbtn.TabIndex = 0;
            this.FairDemandDist_UniformA_rbtn.TabStop = true;
            this.FairDemandDist_UniformA_rbtn.Text = "Discrete";
            this.FairDemandDist_UniformA_rbtn.UseVisualStyleBackColor = true;
            this.FairDemandDist_UniformA_rbtn.CheckedChanged += new System.EventHandler(this.FairDemandDist_Discrete_rbtn_CheckedChanged);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.GoodDemandDist_Mean_txtbox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.GoodDemandDist_UniformB_txtbox);
            this.groupBox5.Controls.Add(this.GoodDemandDist_UniformA_txtbox);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.GoodDemandDist_Uniform_rbtn);
            this.groupBox5.Controls.Add(this.GoodDemandDist_Exponential_rbtn);
            this.groupBox5.Controls.Add(this.GoodDemandDist_Discrete_rbtn);
            this.groupBox5.Location = new System.Drawing.Point(573, 25);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox5.Size = new System.Drawing.Size(360, 158);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Distribution Of Good Type";
            // 
            // GoodDemandDist_Mean_txtbox
            // 
            this.GoodDemandDist_Mean_txtbox.Location = new System.Drawing.Point(267, 84);
            this.GoodDemandDist_Mean_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_Mean_txtbox.Name = "GoodDemandDist_Mean_txtbox";
            this.GoodDemandDist_Mean_txtbox.Size = new System.Drawing.Size(55, 22);
            this.GoodDemandDist_Mean_txtbox.TabIndex = 2;
            this.GoodDemandDist_Mean_txtbox.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(219, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 17);
            this.label10.TabIndex = 14;
            this.label10.Text = "Mean";
            this.label10.Visible = false;
            // 
            // GoodDemandDist_UniformB_txtbox
            // 
            this.GoodDemandDist_UniformB_txtbox.Location = new System.Drawing.Point(145, 121);
            this.GoodDemandDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_UniformB_txtbox.Name = "GoodDemandDist_UniformB_txtbox";
            this.GoodDemandDist_UniformB_txtbox.Size = new System.Drawing.Size(61, 22);
            this.GoodDemandDist_UniformB_txtbox.TabIndex = 2;
            this.GoodDemandDist_UniformB_txtbox.Visible = false;
            // 
            // GoodDemandDist_UniformA_txtbox
            // 
            this.GoodDemandDist_UniformA_txtbox.Location = new System.Drawing.Point(145, 82);
            this.GoodDemandDist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_UniformA_txtbox.Name = "GoodDemandDist_UniformA_txtbox";
            this.GoodDemandDist_UniformA_txtbox.Size = new System.Drawing.Size(61, 22);
            this.GoodDemandDist_UniformA_txtbox.TabIndex = 3;
            this.GoodDemandDist_UniformA_txtbox.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(123, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 17);
            this.label11.TabIndex = 2;
            this.label11.Text = "b";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(123, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 17);
            this.label12.TabIndex = 3;
            this.label12.Text = "a";
            this.label12.Visible = false;
            // 
            // GoodDemandDist_Uniform_rbtn
            // 
            this.GoodDemandDist_Uniform_rbtn.AutoSize = true;
            this.GoodDemandDist_Uniform_rbtn.Location = new System.Drawing.Point(125, 39);
            this.GoodDemandDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_Uniform_rbtn.Name = "GoodDemandDist_Uniform_rbtn";
            this.GoodDemandDist_Uniform_rbtn.Size = new System.Drawing.Size(78, 21);
            this.GoodDemandDist_Uniform_rbtn.TabIndex = 2;
            this.GoodDemandDist_Uniform_rbtn.TabStop = true;
            this.GoodDemandDist_Uniform_rbtn.Text = "Uniform";
            this.GoodDemandDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.GoodDemandDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.GoodDemandDist_Uniform_rbtn_CheckedChanged);
            // 
            // GoodDemandDist_Exponential_rbtn
            // 
            this.GoodDemandDist_Exponential_rbtn.AutoSize = true;
            this.GoodDemandDist_Exponential_rbtn.Location = new System.Drawing.Point(221, 39);
            this.GoodDemandDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_Exponential_rbtn.Name = "GoodDemandDist_Exponential_rbtn";
            this.GoodDemandDist_Exponential_rbtn.Size = new System.Drawing.Size(102, 21);
            this.GoodDemandDist_Exponential_rbtn.TabIndex = 1;
            this.GoodDemandDist_Exponential_rbtn.TabStop = true;
            this.GoodDemandDist_Exponential_rbtn.Text = "Exponential";
            this.GoodDemandDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.GoodDemandDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.GoodDemandDist_Exponential_rbtn_CheckedChanged);
            // 
            // GoodDemandDist_Discrete_rbtn
            // 
            this.GoodDemandDist_Discrete_rbtn.AutoSize = true;
            this.GoodDemandDist_Discrete_rbtn.Location = new System.Drawing.Point(21, 39);
            this.GoodDemandDist_Discrete_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GoodDemandDist_Discrete_rbtn.Name = "GoodDemandDist_Discrete_rbtn";
            this.GoodDemandDist_Discrete_rbtn.Size = new System.Drawing.Size(81, 21);
            this.GoodDemandDist_Discrete_rbtn.TabIndex = 0;
            this.GoodDemandDist_Discrete_rbtn.TabStop = true;
            this.GoodDemandDist_Discrete_rbtn.Text = "Discrete";
            this.GoodDemandDist_Discrete_rbtn.UseVisualStyleBackColor = true;
            this.GoodDemandDist_Discrete_rbtn.CheckedChanged += new System.EventHandler(this.GoodDemandDist_Discrete_rbtn_CheckedChanged);
            // 
            // DemandProbability_dataGridView
            // 
            this.DemandProbability_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DemandProbability_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Demand,
            this.Good,
            this.Fair,
            this.Poor});
            this.DemandProbability_dataGridView.Location = new System.Drawing.Point(5, 21);
            this.DemandProbability_dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DemandProbability_dataGridView.Name = "DemandProbability_dataGridView";
            this.DemandProbability_dataGridView.RowTemplate.Height = 24;
            this.DemandProbability_dataGridView.Size = new System.Drawing.Size(563, 326);
            this.DemandProbability_dataGridView.TabIndex = 0;
            this.DemandProbability_dataGridView.Visible = false;
            // 
            // Demand
            // 
            this.Demand.HeaderText = "Demand";
            this.Demand.Name = "Demand";
            // 
            // Good
            // 
            this.Good.HeaderText = "Good";
            this.Good.Name = "Good";
            // 
            // Fair
            // 
            this.Fair.HeaderText = "Fair";
            this.Fair.Name = "Fair";
            // 
            // Poor
            // 
            this.Poor.HeaderText = "Poor";
            this.Poor.Name = "Poor";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.NewsDayType_dataGridView);
            this.groupBox2.Location = new System.Drawing.Point(5, 6);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(392, 474);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Probability distribution of the predefined news day ";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_Mean_textBox);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_UniformB_txtbox);
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_UniformA_txtbox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_Uniform_rbtn);
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_Exponential_rbtn);
            this.groupBox3.Controls.Add(this.NewsDayTypeDist_Discrete_rbtn);
            this.groupBox3.Location = new System.Drawing.Point(5, 313);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox3.Size = new System.Drawing.Size(360, 158);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Distribution Type";
            // 
            // NewsDayTypeDist_Mean_textBox
            // 
            this.NewsDayTypeDist_Mean_textBox.Location = new System.Drawing.Point(267, 84);
            this.NewsDayTypeDist_Mean_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_Mean_textBox.Name = "NewsDayTypeDist_Mean_textBox";
            this.NewsDayTypeDist_Mean_textBox.Size = new System.Drawing.Size(55, 22);
            this.NewsDayTypeDist_Mean_textBox.TabIndex = 2;
            this.NewsDayTypeDist_Mean_textBox.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(219, 87);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 17);
            this.label9.TabIndex = 14;
            this.label9.Text = "Mean";
            this.label9.Visible = false;
            // 
            // NewsDayTypeDist_UniformB_txtbox
            // 
            this.NewsDayTypeDist_UniformB_txtbox.Location = new System.Drawing.Point(145, 121);
            this.NewsDayTypeDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_UniformB_txtbox.Name = "NewsDayTypeDist_UniformB_txtbox";
            this.NewsDayTypeDist_UniformB_txtbox.Size = new System.Drawing.Size(61, 22);
            this.NewsDayTypeDist_UniformB_txtbox.TabIndex = 2;
            this.NewsDayTypeDist_UniformB_txtbox.Visible = false;
            // 
            // NewsDayTypeDist_UniformA_txtbox
            // 
            this.NewsDayTypeDist_UniformA_txtbox.Location = new System.Drawing.Point(145, 82);
            this.NewsDayTypeDist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_UniformA_txtbox.Name = "NewsDayTypeDist_UniformA_txtbox";
            this.NewsDayTypeDist_UniformA_txtbox.Size = new System.Drawing.Size(61, 22);
            this.NewsDayTypeDist_UniformA_txtbox.TabIndex = 3;
            this.NewsDayTypeDist_UniformA_txtbox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "b";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(123, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 17);
            this.label8.TabIndex = 3;
            this.label8.Text = "a";
            this.label8.Visible = false;
            // 
            // NewsDayTypeDist_Uniform_rbtn
            // 
            this.NewsDayTypeDist_Uniform_rbtn.AutoSize = true;
            this.NewsDayTypeDist_Uniform_rbtn.Location = new System.Drawing.Point(125, 39);
            this.NewsDayTypeDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_Uniform_rbtn.Name = "NewsDayTypeDist_Uniform_rbtn";
            this.NewsDayTypeDist_Uniform_rbtn.Size = new System.Drawing.Size(78, 21);
            this.NewsDayTypeDist_Uniform_rbtn.TabIndex = 2;
            this.NewsDayTypeDist_Uniform_rbtn.TabStop = true;
            this.NewsDayTypeDist_Uniform_rbtn.Text = "Uniform";
            this.NewsDayTypeDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.NewsDayTypeDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.NewsDayTypeDist_Uniform_rbtn_CheckedChanged);
            // 
            // NewsDayTypeDist_Exponential_rbtn
            // 
            this.NewsDayTypeDist_Exponential_rbtn.AutoSize = true;
            this.NewsDayTypeDist_Exponential_rbtn.Location = new System.Drawing.Point(221, 39);
            this.NewsDayTypeDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_Exponential_rbtn.Name = "NewsDayTypeDist_Exponential_rbtn";
            this.NewsDayTypeDist_Exponential_rbtn.Size = new System.Drawing.Size(102, 21);
            this.NewsDayTypeDist_Exponential_rbtn.TabIndex = 1;
            this.NewsDayTypeDist_Exponential_rbtn.TabStop = true;
            this.NewsDayTypeDist_Exponential_rbtn.Text = "Exponential";
            this.NewsDayTypeDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.NewsDayTypeDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.NewsDayTypeDist_Exponential_rbtn_CheckedChanged);
            // 
            // NewsDayTypeDist_Discrete_rbtn
            // 
            this.NewsDayTypeDist_Discrete_rbtn.AutoSize = true;
            this.NewsDayTypeDist_Discrete_rbtn.Location = new System.Drawing.Point(21, 39);
            this.NewsDayTypeDist_Discrete_rbtn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayTypeDist_Discrete_rbtn.Name = "NewsDayTypeDist_Discrete_rbtn";
            this.NewsDayTypeDist_Discrete_rbtn.Size = new System.Drawing.Size(81, 21);
            this.NewsDayTypeDist_Discrete_rbtn.TabIndex = 0;
            this.NewsDayTypeDist_Discrete_rbtn.TabStop = true;
            this.NewsDayTypeDist_Discrete_rbtn.Text = "Discrete";
            this.NewsDayTypeDist_Discrete_rbtn.UseVisualStyleBackColor = true;
            this.NewsDayTypeDist_Discrete_rbtn.CheckedChanged += new System.EventHandler(this.NewsDayTypeDist_Discrete_rbtn_CheckedChanged);
            // 
            // NewsDayType_dataGridView
            // 
            this.NewsDayType_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.NewsDayType_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeOfNewsday,
            this.Probability});
            this.NewsDayType_dataGridView.Location = new System.Drawing.Point(5, 21);
            this.NewsDayType_dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NewsDayType_dataGridView.Name = "NewsDayType_dataGridView";
            this.NewsDayType_dataGridView.RowTemplate.Height = 24;
            this.NewsDayType_dataGridView.Size = new System.Drawing.Size(323, 167);
            this.NewsDayType_dataGridView.TabIndex = 0;
            this.NewsDayType_dataGridView.Visible = false;
            // 
            // TypeOfNewsday
            // 
            this.TypeOfNewsday.HeaderText = "Type Of Newsday";
            this.TypeOfNewsday.Name = "TypeOfNewsday";
            this.TypeOfNewsday.ReadOnly = true;
            // 
            // Probability
            // 
            this.Probability.HeaderText = "Probability";
            this.Probability.Name = "Probability";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BundlesTo_textBox);
            this.groupBox1.Controls.Add(this.BundlesFrom_textBox);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NoOfDays_txtbox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.PurchasePrice_textBox);
            this.groupBox1.Controls.Add(this.ScrapValue_textBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.SellingPrice_txtBox);
            this.groupBox1.Location = new System.Drawing.Point(1488, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(285, 546);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Meta Data";
            // 
            // BundlesTo_textBox
            // 
            this.BundlesTo_textBox.Location = new System.Drawing.Point(139, 420);
            this.BundlesTo_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BundlesTo_textBox.Name = "BundlesTo_textBox";
            this.BundlesTo_textBox.Size = new System.Drawing.Size(100, 22);
            this.BundlesTo_textBox.TabIndex = 15;
            // 
            // BundlesFrom_textBox
            // 
            this.BundlesFrom_textBox.Location = new System.Drawing.Point(139, 357);
            this.BundlesFrom_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BundlesFrom_textBox.Name = "BundlesFrom_textBox";
            this.BundlesFrom_textBox.Size = new System.Drawing.Size(100, 22);
            this.BundlesFrom_textBox.TabIndex = 14;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(29, 425);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 17);
            this.label26.TabIndex = 13;
            this.label26.Text = "Bundles To";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(37, 356);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(95, 17);
            this.label25.TabIndex = 12;
            this.label25.Text = "Bundles From";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "# Of Days";
            // 
            // NoOfDays_txtbox
            // 
            this.NoOfDays_txtbox.Location = new System.Drawing.Point(139, 50);
            this.NoOfDays_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NoOfDays_txtbox.Name = "NoOfDays_txtbox";
            this.NoOfDays_txtbox.Size = new System.Drawing.Size(100, 22);
            this.NoOfDays_txtbox.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Purchace Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(37, 222);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Scrap Value";
            // 
            // PurchasePrice_textBox
            // 
            this.PurchasePrice_textBox.Location = new System.Drawing.Point(139, 108);
            this.PurchasePrice_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PurchasePrice_textBox.Name = "PurchasePrice_textBox";
            this.PurchasePrice_textBox.Size = new System.Drawing.Size(100, 22);
            this.PurchasePrice_textBox.TabIndex = 3;
            // 
            // ScrapValue_textBox
            // 
            this.ScrapValue_textBox.Location = new System.Drawing.Point(139, 218);
            this.ScrapValue_textBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ScrapValue_textBox.Name = "ScrapValue_textBox";
            this.ScrapValue_textBox.Size = new System.Drawing.Size(100, 22);
            this.ScrapValue_textBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 166);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Selling Price";
            // 
            // SellingPrice_txtBox
            // 
            this.SellingPrice_txtBox.Location = new System.Drawing.Point(139, 166);
            this.SellingPrice_txtBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SellingPrice_txtBox.Name = "SellingPrice_txtBox";
            this.SellingPrice_txtBox.Size = new System.Drawing.Size(100, 22);
            this.SellingPrice_txtBox.TabIndex = 5;
            // 
            // Outputs_tabPage2
            // 
            this.Outputs_tabPage2.Controls.Add(this.NetProfit_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label4);
            this.Outputs_tabPage2.Controls.Add(this.label24);
            this.Outputs_tabPage2.Controls.Add(this.DaysFromTo_ComboBox);
            this.Outputs_tabPage2.Controls.Add(this.NumberOfDaysHavingUnsoldPapers_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label23);
            this.Outputs_tabPage2.Controls.Add(this.NumberOfDaysHavingExcessDemand_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label22);
            this.Outputs_tabPage2.Controls.Add(this.TotalSalvageFrom_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label21);
            this.Outputs_tabPage2.Controls.Add(this.TotalLostProfitFromExcessDemand_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label20);
            this.Outputs_tabPage2.Controls.Add(this.TotalRevenueFromSales_txtbox);
            this.Outputs_tabPage2.Controls.Add(this.label19);
            this.Outputs_tabPage2.Controls.Add(this.Output_dataGridView);
            this.Outputs_tabPage2.Location = new System.Drawing.Point(4, 25);
            this.Outputs_tabPage2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Outputs_tabPage2.Name = "Outputs_tabPage2";
            this.Outputs_tabPage2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Outputs_tabPage2.Size = new System.Drawing.Size(1780, 778);
            this.Outputs_tabPage2.TabIndex = 1;
            this.Outputs_tabPage2.Text = "Outputs";
            this.Outputs_tabPage2.UseVisualStyleBackColor = true;
            // 
            // NetProfit_txtbox
            // 
            this.NetProfit_txtbox.Location = new System.Drawing.Point(1573, 450);
            this.NetProfit_txtbox.Name = "NetProfit_txtbox";
            this.NetProfit_txtbox.Size = new System.Drawing.Size(100, 22);
            this.NetProfit_txtbox.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1412, 450);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Net Profit";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(1412, 485);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(114, 17);
            this.label24.TabIndex = 12;
            this.label24.Text = "#Of NewsPapers";
            // 
            // DaysFromTo_ComboBox
            // 
            this.DaysFromTo_ComboBox.FormattingEnabled = true;
            this.DaysFromTo_ComboBox.Location = new System.Drawing.Point(1415, 515);
            this.DaysFromTo_ComboBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DaysFromTo_ComboBox.Name = "DaysFromTo_ComboBox";
            this.DaysFromTo_ComboBox.Size = new System.Drawing.Size(121, 24);
            this.DaysFromTo_ComboBox.TabIndex = 11;
            this.DaysFromTo_ComboBox.SelectedIndexChanged += new System.EventHandler(this.DaysFromTo_ComboBox_SelectedIndexChanged);
            // 
            // NumberOfDaysHavingUnsoldPapers_txtbox
            // 
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Location = new System.Drawing.Point(1573, 393);
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Name = "NumberOfDaysHavingUnsoldPapers_txtbox";
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Size = new System.Drawing.Size(100, 22);
            this.NumberOfDaysHavingUnsoldPapers_txtbox.TabIndex = 10;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1412, 382);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(148, 34);
            this.label23.TabIndex = 9;
            this.label23.Text = "Number of days\r\n having unsold papers";
            // 
            // NumberOfDaysHavingExcessDemand_txtbox
            // 
            this.NumberOfDaysHavingExcessDemand_txtbox.Location = new System.Drawing.Point(1573, 315);
            this.NumberOfDaysHavingExcessDemand_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.NumberOfDaysHavingExcessDemand_txtbox.Name = "NumberOfDaysHavingExcessDemand_txtbox";
            this.NumberOfDaysHavingExcessDemand_txtbox.Size = new System.Drawing.Size(100, 22);
            this.NumberOfDaysHavingExcessDemand_txtbox.TabIndex = 8;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(1409, 304);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(152, 34);
            this.label22.TabIndex = 7;
            this.label22.Text = "Number of days \r\nhaving excess demand";
            // 
            // TotalSalvageFrom_txtbox
            // 
            this.TotalSalvageFrom_txtbox.Location = new System.Drawing.Point(1573, 223);
            this.TotalSalvageFrom_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TotalSalvageFrom_txtbox.Name = "TotalSalvageFrom_txtbox";
            this.TotalSalvageFrom_txtbox.Size = new System.Drawing.Size(100, 22);
            this.TotalSalvageFrom_txtbox.TabIndex = 6;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(1409, 212);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(139, 34);
            this.label21.TabIndex = 5;
            this.label21.Text = "Total Salvage from \r\nsale of Scrap papers";
            // 
            // TotalLostProfitFromExcessDemand_txtbox
            // 
            this.TotalLostProfitFromExcessDemand_txtbox.Location = new System.Drawing.Point(1573, 126);
            this.TotalLostProfitFromExcessDemand_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TotalLostProfitFromExcessDemand_txtbox.Name = "TotalLostProfitFromExcessDemand_txtbox";
            this.TotalLostProfitFromExcessDemand_txtbox.Size = new System.Drawing.Size(100, 22);
            this.TotalLostProfitFromExcessDemand_txtbox.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1405, 114);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 34);
            this.label20.TabIndex = 3;
            this.label20.Text = "Total Lost Profit\r\n from Excess Demand";
            // 
            // TotalRevenueFromSales_txtbox
            // 
            this.TotalRevenueFromSales_txtbox.Location = new System.Drawing.Point(1539, 39);
            this.TotalRevenueFromSales_txtbox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TotalRevenueFromSales_txtbox.Name = "TotalRevenueFromSales_txtbox";
            this.TotalRevenueFromSales_txtbox.Size = new System.Drawing.Size(100, 22);
            this.TotalRevenueFromSales_txtbox.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1403, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(105, 34);
            this.label19.TabIndex = 1;
            this.label19.Text = "Total Revenue \r\nfrom Sales";
            // 
            // Output_dataGridView
            // 
            this.Output_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Output_dataGridView.Location = new System.Drawing.Point(5, 6);
            this.Output_dataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Output_dataGridView.Name = "Output_dataGridView";
            this.Output_dataGridView.RowTemplate.Height = 24;
            this.Output_dataGridView.Size = new System.Drawing.Size(1392, 708);
            this.Output_dataGridView.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.zedGraphControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(1780, 778);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Profit Occurance Chart";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(696, 618);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(177, 64);
            this.button2.TabIndex = 2;
            this.button2.Text = "Clear Chart";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(459, 618);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(181, 64);
            this.button1.TabIndex = 1;
            this.button1.Text = "Draw Chart";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(245, 75);
            this.zedGraphControl1.Margin = new System.Windows.Forms.Padding(5);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(812, 501);
            this.zedGraphControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.zedGraphControl2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(1780, 778);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Bundel/Profit Chart";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(704, 618);
            this.button4.Margin = new System.Windows.Forms.Padding(4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(167, 62);
            this.button4.TabIndex = 2;
            this.button4.Text = "Clear Chart";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(273, 618);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(167, 62);
            this.button3.TabIndex = 1;
            this.button3.Text = "Draw Chart";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // zedGraphControl2
            // 
            this.zedGraphControl2.Location = new System.Drawing.Point(149, 36);
            this.zedGraphControl2.Margin = new System.Windows.Forms.Padding(5);
            this.zedGraphControl2.Name = "zedGraphControl2";
            this.zedGraphControl2.ScrollGrace = 0D;
            this.zedGraphControl2.ScrollMaxX = 0D;
            this.zedGraphControl2.ScrollMaxY = 0D;
            this.zedGraphControl2.ScrollMaxY2 = 0D;
            this.zedGraphControl2.ScrollMinX = 0D;
            this.zedGraphControl2.ScrollMinY = 0D;
            this.zedGraphControl2.ScrollMinY2 = 0D;
            this.zedGraphControl2.Size = new System.Drawing.Size(929, 556);
            this.zedGraphControl2.TabIndex = 0;
            // 
            // SimulationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1749, 750);
            this.Controls.Add(this.Chart);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "SimulationForm";
            this.Text = "SimulationForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Chart.ResumeLayout(false);
            this.Inputs_tab.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DemandProbability_dataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NewsDayType_dataGridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Outputs_tabPage2.ResumeLayout(false);
            this.Outputs_tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Output_dataGridView)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl Chart;
        private System.Windows.Forms.TabPage Inputs_tab;
        private System.Windows.Forms.TextBox SellingPrice_txtBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PurchasePrice_textBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NoOfDays_txtbox;
        private System.Windows.Forms.TabPage Outputs_tabPage2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ScrapValue_textBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox NewsDayTypeDist_Mean_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NewsDayTypeDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox NewsDayTypeDist_UniformA_txtbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton NewsDayTypeDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton NewsDayTypeDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton NewsDayTypeDist_Discrete_rbtn;
        private System.Windows.Forms.DataGridView NewsDayType_dataGridView;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox GoodDemandDist_Mean_txtbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox GoodDemandDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox GoodDemandDist_UniformA_txtbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton GoodDemandDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton GoodDemandDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton GoodDemandDist_Discrete_rbtn;
        private System.Windows.Forms.DataGridView DemandProbability_dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand;
        private System.Windows.Forms.DataGridViewTextBoxColumn Good;
        private System.Windows.Forms.DataGridViewTextBoxColumn Fair;
        private System.Windows.Forms.DataGridViewTextBoxColumn Poor;
        private System.Windows.Forms.DataGridView Output_dataGridView;
        private System.Windows.Forms.Button Simulate_btn;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox PoorDemandDist_Mean_txtbox;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox PoorDemandDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox PoorDemandDist_UniformA_txtbox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.RadioButton PoorDemandDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton PoorDemandDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton PoorDemandDist_Discrete_rbtn;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox FairDemandDist_Mean_txtbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox FairDemandDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox FairDemandDist_UniformA_txtbox;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton FairDemandDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton FairDemandDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton FairDemandDist_UniformA_rbtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeOfNewsday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Probability;
        private System.Windows.Forms.TextBox NumberOfDaysHavingUnsoldPapers_txtbox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox NumberOfDaysHavingExcessDemand_txtbox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox TotalSalvageFrom_txtbox;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox TotalLostProfitFromExcessDemand_txtbox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TotalRevenueFromSales_txtbox;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox DaysFromTo_ComboBox;
        private System.Windows.Forms.TextBox BundlesTo_textBox;
        private System.Windows.Forms.TextBox BundlesFrom_textBox;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TabPage tabPage1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private ZedGraph.ZedGraphControl zedGraphControl2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox NetProfit_txtbox;
        private System.Windows.Forms.Label label4;
    }
}