﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using ZedGraph;
using System.Windows.Forms;
using InventorySystem.Simulation;
using InventorySystem.Distributions;
namespace InventorySystem.UI
{

    public partial class SimulationForm : Form
    {
        int NoOfDays;
        float PurchasePrice;
        float SellingPrice;
        float LostProfitFromExcessDemand;
        float ScrapValue;
        //NewsDayType Data
        List<float> NewsDaydataTypeValues_List;
        List<float> NewsDaydataTypeProbabilities_List;

        //Demand Data
        List<float> GoodDemandValues_List;
        List<float> GoodDemandProbabilities_List;
        List<float> FairDemandValues_List;
        List<float> FairDemandProbabilities_List;
        List<float> PoorDemandValues_List;
        List<float> PoorDemandProbabilities_List;

        IDistribution dayTypeDist;
        DistributionTypes dayTypeDist_enum;
        IDistribution demandGoodDist;
        DistributionTypes demandGoodDist_enum;
        IDistribution demandFairDist;
        DistributionTypes demandFairDist_enum;
        IDistribution demandPoorDist;
        DistributionTypes demandPoorDist_enum;
        SimulationManager Manager;
        public SimulationForm()
        {
            InitializeComponent();
            Manager = new SimulationManager();

            this.NewsDaydataTypeValues_List = new List<float>();
            this.NewsDaydataTypeProbabilities_List = new List<float>();
          
            this.GoodDemandValues_List = new List<float>();
            this.GoodDemandProbabilities_List = new List<float>();
            this.FairDemandValues_List = new List<float>();
            this.FairDemandProbabilities_List = new List<float>();
            this.PoorDemandValues_List = new List<float>();
            this.PoorDemandProbabilities_List = new List<float>();

            this.NewsDayType_dataGridView.Rows.Add();
            this.NewsDayType_dataGridView.Rows.Add();
            this.NewsDayType_dataGridView.Rows[0].Cells[0].Value = "Good";
            this.NewsDayType_dataGridView.Rows[1].Cells[0].Value = "Fair";
            this.NewsDayType_dataGridView.Rows[2].Cells[0].Value = "Poor";
        }
        private void SetNewsDayData()
        {
            this.NewsDaydataTypeValues_List.Add((float)DayType.Good);
            this.NewsDaydataTypeValues_List.Add((float)DayType.Fair);
            this.NewsDaydataTypeValues_List.Add((float)DayType.Poor);
            DataGridViewRowCollection Rows = this.NewsDayType_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells; ;
                this.NewsDaydataTypeProbabilities_List.Add(float.Parse(cells[1].Value.ToString()));
            }
        }

        /// <summary>
        /// Intializes NewsDaydataTypeValues_List andNewsDaydataTypeProbabilities_List
        /// </summary>
        public void SetGoodDemandTypeData()
        {
            DataGridViewRowCollection Rows = this.DemandProbability_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.GoodDemandValues_List.Add(float.Parse(cells[0].Value.ToString()));
                this.GoodDemandProbabilities_List.Add(float.Parse(cells[1].Value.ToString()));
            }
        }

        public void SetFairDemandTypeData()
        {
            DataGridViewRowCollection Rows = this.DemandProbability_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.FairDemandValues_List.Add(float.Parse(cells[0].Value.ToString()));
                this.FairDemandProbabilities_List.Add(float.Parse(cells[2].Value.ToString()));
            }
        }

        public void SetPoorDemandTypeData()
        {
            DataGridViewRowCollection Rows = this.DemandProbability_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.PoorDemandValues_List.Add(float.Parse(cells[0].Value.ToString()));
                this.PoorDemandProbabilities_List.Add(float.Parse(cells[3].Value.ToString()));
            }
        }
        /// <summary>
        /// Initializes MetaData
        /// </summary>
        public void InitializeMetaData()
        {
            this.NoOfDays = int.Parse(this.NoOfDays_txtbox.Text);
            this.PurchasePrice = float.Parse(this.PurchasePrice_textBox.Text);
            this.SellingPrice = float.Parse(this.SellingPrice_txtBox.Text);
            this.ScrapValue = float.Parse(this.ScrapValue_textBox.Text);
            
            for (int j = int.Parse(this.BundlesFrom_textBox.Text);j <= int.Parse(this.BundlesTo_textBox.Text); j += 10)
            {
                this.DaysFromTo_ComboBox.Items.Add(j.ToString() + " Newspapers");
            }
        }

        public void InitializeDistributionData()
        {
            this.SetDayTypeDist();
            this.SetDemandDist();
        }

        private void SetDayTypeDist()
        {
            if (this.dayTypeDist_enum == DistributionTypes.Discrete)
            {
                this.SetNewsDayData();
                this.dayTypeDist = new DiscreteDistribution(NewsDaydataTypeValues_List, this.NewsDaydataTypeProbabilities_List);
            }
            else if (this.dayTypeDist_enum == DistributionTypes.Exponential)
                this.dayTypeDist = new ExponentialDistribution(float.Parse(this.NewsDayTypeDist_Mean_textBox.Text));
            else if (this.dayTypeDist_enum == DistributionTypes.Uniform)
                this.dayTypeDist = new UniformDistribution(float.Parse(this.NewsDayTypeDist_UniformA_txtbox.Text),
               float.Parse(this.NewsDayTypeDist_UniformB_txtbox.Text));
        }

        private void SetDemandDist()
        {
            //Good demand Dist
            if (this.demandGoodDist_enum == DistributionTypes.Discrete)
            {
                this.SetGoodDemandTypeData();
                this.demandGoodDist = new DiscreteDistribution(GoodDemandValues_List, this.GoodDemandProbabilities_List);
            }
            else if (this.demandGoodDist_enum == DistributionTypes.Exponential)
                this.demandGoodDist = new ExponentialDistribution(float.Parse(this.GoodDemandDist_Mean_txtbox.Text));
            else if (this.demandGoodDist_enum == DistributionTypes.Uniform)
                this.demandGoodDist = new UniformDistribution(float.Parse(this.GoodDemandDist_UniformA_txtbox.Text),
               float.Parse(this.GoodDemandDist_UniformB_txtbox.Text));

            //Fair Demand Dist
            if (this.demandFairDist_enum == DistributionTypes.Discrete)
            {
                this.SetFairDemandTypeData();
                this.demandFairDist = new DiscreteDistribution(FairDemandValues_List, this.FairDemandProbabilities_List);
            }
            else if (this.demandFairDist_enum == DistributionTypes.Exponential)
                this.demandFairDist = new ExponentialDistribution(float.Parse(this.FairDemandDist_Mean_txtbox.Text));
            else if (this.demandFairDist_enum == DistributionTypes.Uniform)
                this.demandFairDist = new UniformDistribution(float.Parse(this.FairDemandDist_UniformA_txtbox.Text),
               float.Parse(this.FairDemandDist_UniformB_txtbox.Text));
            //Poor Demand Dist
            if (this.demandPoorDist_enum == DistributionTypes.Discrete)
            {
                this.SetPoorDemandTypeData();
                this.demandPoorDist = new DiscreteDistribution(PoorDemandValues_List, this.PoorDemandProbabilities_List);
            }
            else if (this.demandPoorDist_enum == DistributionTypes.Exponential)
                this.demandPoorDist = new ExponentialDistribution(float.Parse(this.PoorDemandDist_Mean_txtbox.Text));
            else if (this.demandPoorDist_enum == DistributionTypes.Uniform)
                this.demandPoorDist = new UniformDistribution(float.Parse(this.PoorDemandDist_UniformA_txtbox.Text),
               float.Parse(this.PoorDemandDist_UniformB_txtbox.Text));

        }

        private void Simulate_Click(object sender, EventArgs e)
        {
            this.InitializeMetaData();
            this.InitializeDistributionData();
            this.Manager.StartSimulation(this.NoOfDays, int.Parse(this.BundlesFrom_textBox.Text), int.Parse(this.BundlesTo_textBox.Text), this.ScrapValue, this.PurchasePrice, this.SellingPrice,
                this.dayTypeDist, this.demandGoodDist, this.demandFairDist, this.demandPoorDist);
        }

        public void ClearAll()
        {
            this.NoOfDays_txtbox.Clear();
            this.PurchasePrice_textBox.Clear();
            this.SellingPrice_txtBox.Clear();
            this.ScrapValue_textBox.Clear();
            this.BundlesFrom_textBox.Clear();
            this.BundlesTo_textBox.Clear();
            this.NewsDaydataTypeValues_List = new List<float>();
            this.NewsDaydataTypeProbabilities_List = new List<float>();


            //Demand Data
            this.GoodDemandValues_List = new List<float>();
            this.GoodDemandProbabilities_List = new List<float>();
            this.FairDemandValues_List = new List<float>();
            this.FairDemandProbabilities_List = new List<float>();
            this.PoorDemandValues_List = new List<float>();
            this.PoorDemandProbabilities_List = new List<float>();

            this.Output_dataGridView.DataSource = "";
            this.TotalRevenueFromSales_txtbox.Text = "";
            this.TotalLostProfitFromExcessDemand_txtbox.Text = "";
            this.TotalSalvageFrom_txtbox.Text = "";
            this.TotalRevenueFromSales_txtbox.Text = "";
            this.NumberOfDaysHavingExcessDemand_txtbox.Text = "";
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Text = "";
            this.NetProfit_txtbox.Text = "";
            this.DaysFromTo_ComboBox.Items.Clear();
            this.DemandProbability_dataGridView.Rows.Clear();
            //this.DemandProbability_dataGridView.Columns.Clear();
            this.NewsDayType_dataGridView.Rows[0].Cells[1].Value = "";
            this.NewsDayType_dataGridView.Rows[1].Cells[1].Value = "";
            this.NewsDayType_dataGridView.Rows[2].Cells[1].Value = "";
           // this.NewsDayType_dataGridView.Columns.Clear();
        }

        
        private void GoodDemandDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.GoodDemandDist_UniformA_txtbox.Visible = !this.GoodDemandDist_UniformA_txtbox.Visible;
            this.GoodDemandDist_UniformB_txtbox.Visible = !this.GoodDemandDist_UniformB_txtbox.Visible;
            this.label11.Visible = !this.label11.Visible;
            this.label12.Visible = !this.label12.Visible;
            this.demandGoodDist_enum = DistributionTypes.Uniform;
        }

        private void GoodDemandDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.DemandProbability_dataGridView.Visible = true;
            this.demandGoodDist_enum = DistributionTypes.Discrete;
        }

        private void GoodDemandDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label10.Visible = !this.label10.Visible;
            this.GoodDemandDist_Mean_txtbox.Visible = !this.GoodDemandDist_Mean_txtbox.Visible;
            this.demandGoodDist = new ExponentialDistribution(float.Parse(this.GoodDemandDist_Mean_txtbox.Text));
            this.demandGoodDist_enum = DistributionTypes.Exponential;
        }

        private void FairDemandDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.DemandProbability_dataGridView.Visible = true;
            this.demandFairDist_enum = DistributionTypes.Discrete;
        }

        private void FairDemandDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.FairDemandDist_UniformA_txtbox.Visible = !this.FairDemandDist_UniformA_txtbox.Visible;
            this.FairDemandDist_UniformB_txtbox.Visible = !this.FairDemandDist_UniformB_txtbox.Visible;
            this.label14.Visible = !this.label14.Visible;
            this.label15.Visible = !this.label15.Visible;
            this.demandFairDist_enum = DistributionTypes.Uniform;
        }

        private void FairDemandDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label13.Visible = !this.label13.Visible;
            this.FairDemandDist_Mean_txtbox.Visible = !this.FairDemandDist_Mean_txtbox.Visible;
            this.demandFairDist_enum = DistributionTypes.Exponential;
        }

        private void PoorDemandDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.DemandProbability_dataGridView.Visible = true;
            this.demandPoorDist_enum = DistributionTypes.Discrete;
        }

        private void PoorDemandDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.PoorDemandDist_UniformA_txtbox.Visible = !this.PoorDemandDist_UniformA_txtbox.Visible;
            this.PoorDemandDist_UniformB_txtbox.Visible = !this.PoorDemandDist_UniformB_txtbox.Visible;
            this.label17.Visible = !this.label17.Visible;
            this.label18.Visible = !this.label18.Visible;
            this.demandPoorDist_enum = DistributionTypes.Uniform;

        }

        private void PoorDemandDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label16.Visible = !this.label16.Visible;
            this.PoorDemandDist_Mean_txtbox.Visible = !this.PoorDemandDist_Mean_txtbox.Visible;
            this.demandPoorDist_enum = DistributionTypes.Exponential;
        }

        private void NewsDayTypeDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.NewsDayType_dataGridView.Visible = !this.NewsDayType_dataGridView.Visible;
            this.dayTypeDist_enum = DistributionTypes.Discrete;
        }

        private void NewsDayTypeDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label9.Visible = !this.label9.Visible;
            this.dayTypeDist_enum = DistributionTypes.Exponential;
        }

        private void NewsDayTypeDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.NewsDayTypeDist_UniformA_txtbox.Visible = !this.PoorDemandDist_UniformA_txtbox.Visible;
            this.NewsDayTypeDist_UniformB_txtbox.Visible = !this.PoorDemandDist_UniformB_txtbox.Visible;
            this.label7.Visible = !this.label7.Visible;
            this.label8.Visible = !this.label8.Visible;
            this.dayTypeDist_enum = DistributionTypes.Discrete;
        }

        private void DaysFromTo_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = this.DaysFromTo_ComboBox.SelectedIndex;
            this.Output_dataGridView.DataSource = this.Manager.CalculateStatistics(i);
            this.TotalRevenueFromSales_txtbox.Text = this.Manager.SystemModels[i].ToatalRevenueFromSales.ToString();
            this.TotalLostProfitFromExcessDemand_txtbox.Text = this.Manager.SystemModels[i].TotalLostProfitFromExcessDemand.ToString();
            this.TotalSalvageFrom_txtbox.Text = this.Manager.SystemModels[i].TotalSalvage.ToString();
            this.TotalRevenueFromSales_txtbox.Text = this.Manager.SystemModels[i].ToatalRevenueFromSales.ToString();
            this.NumberOfDaysHavingExcessDemand_txtbox.Text = this.Manager.SystemModels[i].NoOfDaysHavingExcessDemand.ToString();
            this.NumberOfDaysHavingUnsoldPapers_txtbox.Text = this.Manager.SystemModels[i].NoOfDaysHavingUnsoldPapers.ToString();
            this.NetProfit_txtbox.Text = this.Manager.SystemModels[i].NetProfit.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int m = this.DaysFromTo_ComboBox.SelectedIndex;
            int[] DayCount;
            DayCount = Manager.DrawCharts1(m);

            // Draw

            GraphPane myPane = zedGraphControl1.GraphPane;
            myPane.Title.Text = "Frequency of Daily Profit";
            myPane.XAxis.Title.Text = "Profit Range";
            myPane.YAxis.Title.Text = "Occurrences";

            // Create Bar
            PointPairList list1 = new PointPairList();

            for (int i = 0; i < NoOfDays; i++)
            {

                list1.Add(Manager.DayCount1[i], DayCount[i]);    
            }


            BarItem bar1 = myPane.AddBar("Bar 1", list1, Color.Red);
            bar1.Bar.Fill = new Fill(Color.Red, Color.White, Color.Red, 90);


            // Set BarBase to the XAxis for horizontal bars
            myPane.BarSettings.Base = BarBase.X;


            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White,
               Color.FromArgb(255, 255, 166), 45.0F);

            zedGraphControl1.AxisChange();

            // Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, true, "f0");
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            zedGraphControl1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.Refresh();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            zedGraphControl2.GraphPane.CurveList.Clear();
            zedGraphControl2.GraphPane.GraphObjList.Clear();
            zedGraphControl2.Refresh();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int n = this.DaysFromTo_ComboBox.SelectedIndex;
            int[] bndel;
            bndel = Manager.DrawCharts2();

            // Draw

            GraphPane myPane = zedGraphControl2.GraphPane;
            myPane.Title.Text = "Bundel/Profit Chart";
            myPane.XAxis.Title.Text = "Bundels";
            myPane.YAxis.Title.Text = "Total Revenu";

            // Create Bar
            PointPairList list1 = new PointPairList();

            for (int i = 0; i < Manager.SystemModels.Count; i++)
            {

                list1.Add(bndel[i], Manager.revenueArray[i]);
            }


            BarItem bar1 = myPane.AddBar("Bar 1", list1, Color.Red);
            bar1.Bar.Fill = new Fill(Color.Red, Color.White, Color.Red, 90);


            // Set BarBase to the XAxis for horizontal bars
            myPane.BarSettings.Base = BarBase.X;


            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White,
               Color.FromArgb(255, 255, 166), 45.0F);

            zedGraphControl2.AxisChange();

            // Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, true, "f0");
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
            zedGraphControl2.Refresh();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void ClearAll_Click(object sender, EventArgs e)
        {
            this.ClearAll();
        }
    }
}
