﻿using System;
using InventorySystem.Simulation;

namespace InventorySystem.Entities
{

    public class Day
    {
        public double profit;
        public DayType dayType;
        public int demand;
        public double sellingPricePerUnit;
        public double buyingPricePerUnit;
        public double scrapPerUint;
        public int bundelAmount;
        public double revenueFromSales;
        public double excessDemand;
        public double SalvageFromSaleOfScrap;
        public double CostOfNewsPapers;
        //ToDo:Document and implement Day Constructor 
        public Day(DayType dayType, int demand, double sellingPricePerUnit,
            double buyingPricePerUnit, double scrapPerUint, int bundelAmount)
        {
            this.dayType = dayType;
            this.demand = demand;
            this.sellingPricePerUnit = sellingPricePerUnit;
            this.buyingPricePerUnit = buyingPricePerUnit;
            this.scrapPerUint = scrapPerUint;
            this.bundelAmount = bundelAmount;
        }

        //ToDo:Document and implement GetDayProfit
        public void GetDayProfit()
        {
            this.revenueFromSales = (demand <= bundelAmount) ? demand * sellingPricePerUnit : bundelAmount * sellingPricePerUnit;
            this.SalvageFromSaleOfScrap = (demand < bundelAmount) ? (bundelAmount - demand) * scrapPerUint : 0;
            excessDemand = (demand < bundelAmount) ? 0 : (demand - bundelAmount) * (this.sellingPricePerUnit - this.buyingPricePerUnit);
            this.CostOfNewsPapers = (bundelAmount * buyingPricePerUnit);
            profit = this.revenueFromSales - this.CostOfNewsPapers - this.excessDemand + this.SalvageFromSaleOfScrap;
        }

    }
}
