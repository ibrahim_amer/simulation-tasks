﻿
using System;
using InventorySystem.Distributions;
using InventorySystem.Entities;
using System.Collections.Generic;
using System.Data;
namespace InventorySystem.Simulation
{
    public class Model
    {
        //ToDo:Document and implement Model Constructor
        int daysNo;
        int daysCounter = 0;
       public int bundelsAmount;
        double scrapPerUnit;
        double buyingPricePerUnit;
        double sellingPricePerUnit;
        IDistribution dayTypeDist;
        IDistribution demandGoodDist;
        IDistribution demandFairDist;
        IDistribution demandPoorDist;
        public List<Day> Days;
        DataTable ModelDataTable;
        //System Performance
        public float ToatalRevenueFromSales;
        public float TotalCostOfNewsPapers;
        public float TotalLostProfitFromExcessDemand;
        public float TotalSalvage;
        public float NetProfit;
        public int NoOfDaysHavingExcessDemand;
        public int NoOfDaysHavingUnsoldPapers;
        public Model(int daysNo, int bundelsAmount, double scrapPerUnit,
            double buyingPricePerUnit, double sellingPricePerUnit,
            IDistribution dayTypeDist,
            IDistribution demandGoodDist,
            IDistribution demandFairDist,
            IDistribution demandPoorDist)
        {
            this.ModelDataTable = new DataTable();
            this.InitializeModelDataTable();
            this.daysNo = daysNo;
            this.bundelsAmount = bundelsAmount;
            this.scrapPerUnit = scrapPerUnit;
            this.buyingPricePerUnit = buyingPricePerUnit;
            this.sellingPricePerUnit = sellingPricePerUnit;
            this.dayTypeDist = dayTypeDist;
            this.demandFairDist = demandFairDist;
            this.demandGoodDist = demandGoodDist;
            this.demandPoorDist = demandPoorDist;
            Days = new List<Day>(daysNo);
            ToatalRevenueFromSales = 0;
            TotalCostOfNewsPapers = 0;
            TotalLostProfitFromExcessDemand = 0;
            TotalSalvage = 0;
            NetProfit = 0;
            NoOfDaysHavingExcessDemand = 0;
            NoOfDaysHavingUnsoldPapers = 0;
        }
        public void AddRowToModelDataTable(int DayNo, float RandomNewDay, float RandomDemand, Day day)
        {
            DataRow row;
            row = ModelDataTable.NewRow();
            row["Day"] = DayNo;
            row["Random Digits For Type Of NewsDay"] = RandomNewDay * 100;
            row["Type Of NewsDay"] = day.dayType.ToString();
            row["Random Digits For Demand"] = RandomDemand * 100;
            row["Demand"] = day.demand;
            row["Revenue From Sales"] = day.revenueFromSales;
            row["Lost Profit From Excess Demand"] = day.excessDemand;
            row["Salvage From Sale Of Scrap"] = day.SalvageFromSaleOfScrap;
            row["Daily Profit"] = day.profit;

            ModelDataTable.Rows.Add(row);
        }
        
        public void InitializeModelDataTable()
        {
            
            ModelDataTable.Columns.Add("Day");
            ModelDataTable.Columns.Add("Random Digits For Type Of NewsDay");
            ModelDataTable.Columns.Add("Type Of NewsDay");
            ModelDataTable.Columns.Add("Random Digits For Demand");
            ModelDataTable.Columns.Add("Demand");
            ModelDataTable.Columns.Add("Revenue From Sales");
            ModelDataTable.Columns.Add("Lost Profit From Excess Demand");
            ModelDataTable.Columns.Add("Salvage From Sale Of Scrap");
            ModelDataTable.Columns.Add("Daily Profit");
        }
        //Todo Document and implement GenerateDay 
        private Day GenerateDay()
        {
            float RandomNewDay = LCGRandomNumberGenerator.GetVariate();
            int dayType = (int)dayTypeDist.GetValue(RandomNewDay);
            float RandomDemand = LCGRandomNumberGenerator.GetVariate();
            int demand = 0;
            if (dayType == (int)DayType.Good)
            {
                demand = (int)demandGoodDist.GetValue(RandomDemand);
            }
            else if (dayType == (int)DayType.Fair)
            {
                demand = (int)demandFairDist.GetValue(RandomDemand);
            }
            else
            {
                demand = (int)demandPoorDist.GetValue(RandomDemand);
            }
            Day TempDay = new Day((DayType)dayType, demand, this.sellingPricePerUnit, this.buyingPricePerUnit, this.scrapPerUnit, this.bundelsAmount);
           
            TempDay.GetDayProfit();
            this.NoOfDaysHavingExcessDemand += (TempDay.excessDemand > 0) ? 1 : 0;
            this.NoOfDaysHavingUnsoldPapers += (TempDay.SalvageFromSaleOfScrap > 0) ? 1 : 0;
            this.TotalLostProfitFromExcessDemand += (float)TempDay.excessDemand;
            this.TotalSalvage += (float)TempDay.SalvageFromSaleOfScrap;
            this.NetProfit += (float)TempDay.profit;
            this.ToatalRevenueFromSales += (float)TempDay.revenueFromSales;
            this.AddRowToModelDataTable(++daysCounter, RandomNewDay, RandomDemand, TempDay);
            return TempDay;
        }

        //Todo Document and implement Run 
        public void Run()
        {
            for (int i = 0; i < daysNo; i++)
            {
                Days.Add(GenerateDay());
            }
        }

        //Todo Document and implement CalculateStatistics 
        public DataTable CalculateStatistics()
        {
            return this.ModelDataTable;
        }

    }
}
