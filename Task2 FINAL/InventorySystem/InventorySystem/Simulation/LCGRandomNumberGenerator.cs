﻿namespace InventorySystem.Simulation
{
    using System;

    static class LCGRandomNumberGenerator
    {
        static Random rand = new Random();
        static int M = 1000;
        static int a = 5;
        static int c = 3;
        static int X = 1;
        public static float GetVariate()
        {
            return (float)rand.NextDouble();
        }
    }
}
