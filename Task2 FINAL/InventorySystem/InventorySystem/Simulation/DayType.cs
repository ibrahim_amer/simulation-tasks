﻿//----------------------------------------------------------------------------------
//<copyright file="DayType.cs" company="FCIS-CS">
// You are NOT welcome to do whatever you want with the code in this file :P
// </copyright>
//----------------------------------------------------------------------------------

namespace InventorySystem.Simulation
{
    public enum DayType
    {
        Good,
        Fair,
        Poor
    }
}
