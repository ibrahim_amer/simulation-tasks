﻿
using System;
using InventorySystem.Distributions;
using System.Data;
using System.Collections.Generic;

namespace InventorySystem.Simulation
{
    class SimulationManager
    {
        public List<Model> SystemModels;
        public int daysNo;
        public int bundelsFrom; 
        public int bundelsTo;
        public double scrapPerUnit;
        public double buyingPricePerUnit;
        public double sellingPricePerUnit;
        public IDistribution dayTypeDist;
        public IDistribution demandGoodDist;
        public IDistribution demandFairDist;
        public IDistribution demandPoorDist;
        public int[] DayCount1;
        public  int [] revenueArray;
        //ToDo: Document and implement StartSimulation
        public void StartSimulation(int daysNo, int bundelsFrom, int bundelsTo,
            double scrapPerUnit, double buyingPricePerUnit, double sellingPricePerUnit,
            IDistribution dayTypeDist,IDistribution demandGoodDist,
            IDistribution demandFairDist, IDistribution demandPoorDist)
        {
            this.SystemModels = new List<Model>();;
            this.bundelsFrom = bundelsFrom;

            this.daysNo = daysNo;
            this.bundelsFrom = bundelsFrom;
            this.bundelsTo = bundelsTo;
            this.scrapPerUnit = scrapPerUnit;
            this.buyingPricePerUnit = buyingPricePerUnit;
            this.sellingPricePerUnit = sellingPricePerUnit;
            this.dayTypeDist = dayTypeDist;
            this.demandGoodDist = demandGoodDist;
            this.demandFairDist = demandFairDist;
            this.demandPoorDist = demandPoorDist;
            this.RunAll();
        }

        public void RunAll()
        {
            for (int i = this.bundelsFrom; i <= bundelsTo; i += 10)
            {
                this.SystemModels.Add(new Model(daysNo, i, scrapPerUnit, buyingPricePerUnit, sellingPricePerUnit
                  , dayTypeDist, demandGoodDist, demandFairDist, demandPoorDist));
            }

            for (int i = 0; i < this.SystemModels.Count; i++)
            {
                this.SystemModels[i].Run();
            }
        }
        public DataTable CalculateStatistics(int i)
        {
            return this.SystemModels[i].CalculateStatistics();
        }

        public int[] DrawCharts1(int m)
        {
        
            int[] DayCount = new int[daysNo];
            DayCount1 = new int[daysNo];
            for (int j = 0; j < daysNo; j++)
            {
                DayCount[j]++;
                DayCount1[j] = (int)SystemModels[m].Days[j].profit;
                for (int k = 0; k < daysNo; k++)
                    if (SystemModels[m].Days[j].profit == SystemModels[m].Days[k].profit)
                    {
                        if( k!=j)
                            DayCount[j]++;
                    }
            }
            return DayCount;
        }

        public int[] DrawCharts2()
        {

            int m = 0;
            int [] budelArray = new int [SystemModels.Count];
             revenueArray = new int [SystemModels.Count];
            for (int i = 0; i < SystemModels.Count; i++)
            {
                budelArray[i] = bundelsFrom + m;
                revenueArray[i] =(int) SystemModels[i].ToatalRevenueFromSales;
                m += 10;
            }
            return budelArray;
        }
    }
}
