﻿namespace MultipleQueue
{
    partial class MultipleServerSimulation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Input_TabControl = new System.Windows.Forms.TabPage();
            this.NoOfRuns_textbox = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.InterarrivalTimeDist_Grpbox = new System.Windows.Forms.GroupBox();
            this.MeanExponentialDist_InterarrivalTime_txtbox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.UniformDistB_txtbox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.UniformDistA_txtbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.CustomerStoppingCondition_txtbox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Simulate_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ServiceTimeData_GridView = new System.Windows.Forms.DataGridView();
            this.ServiceTimeValues = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServiceTimeProbability = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.Interarrival_GridView = new System.Windows.Forms.DataGridView();
            this.InterarrivalValues = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InterarrivalProbabilities = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ServerDistribution_grpbox = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.MeanExponentialDist_Server_txtbox = new System.Windows.Forms.TextBox();
            this.AddServer_btn = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.UniformB_Server_txtbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.UniformA_Server_txtbox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Exponentioal_Server_rbtn = new System.Windows.Forms.RadioButton();
            this.Uniform_Server_rbtn = new System.Windows.Forms.RadioButton();
            this.Discrete_Server_rbtn = new System.Windows.Forms.RadioButton();
            this.ServerPriority_Grpbox = new System.Windows.Forms.GroupBox();
            this.HighestPriority_rbtn = new System.Windows.Forms.RadioButton();
            this.LowestUtilization_rbtn = new System.Windows.Forms.RadioButton();
            this.Random_rbtn = new System.Windows.Forms.RadioButton();
            this.ServerState_Grpbox = new System.Windows.Forms.GroupBox();
            this.NoOfServers_txtbox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.MultipleServer_rbtn = new System.Windows.Forms.RadioButton();
            this.SingleServer_rbtn = new System.Windows.Forms.RadioButton();
            this.AVGTimeBetweenInterarrivals_txtbox = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AVGTimeCustomersSpendInSys_txtbox = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.AVGWaitingTimeOfThoseWhoWait_txtbox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox = new System.Windows.Forms.TextBox();
            this.AverageServiceTime_txtbox = new System.Windows.Forms.TextBox();
            this.ProbOfIdleServer_txtbox = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.AVGWaitingTime_txtbox = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.RunNo_comboBox = new System.Windows.Forms.ComboBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.Input_TabControl.SuspendLayout();
            this.InterarrivalTimeDist_Grpbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimeData_GridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Interarrival_GridView)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.ServerDistribution_grpbox.SuspendLayout();
            this.ServerPriority_Grpbox.SuspendLayout();
            this.ServerState_Grpbox.SuspendLayout();
            this.AVGTimeBetweenInterarrivals_txtbox.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Input_TabControl);
            this.tabControl1.Controls.Add(this.AVGTimeBetweenInterarrivals_txtbox);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1330, 602);
            this.tabControl1.TabIndex = 0;
            // 
            // Input_TabControl
            // 
            this.Input_TabControl.Controls.Add(this.NoOfRuns_textbox);
            this.Input_TabControl.Controls.Add(this.label13);
            this.Input_TabControl.Controls.Add(this.InterarrivalTimeDist_Grpbox);
            this.Input_TabControl.Controls.Add(this.CustomerStoppingCondition_txtbox);
            this.Input_TabControl.Controls.Add(this.label3);
            this.Input_TabControl.Controls.Add(this.Simulate_btn);
            this.Input_TabControl.Controls.Add(this.label2);
            this.Input_TabControl.Controls.Add(this.ServiceTimeData_GridView);
            this.Input_TabControl.Controls.Add(this.label1);
            this.Input_TabControl.Controls.Add(this.Interarrival_GridView);
            this.Input_TabControl.Controls.Add(this.groupBox1);
            this.Input_TabControl.Location = new System.Drawing.Point(4, 22);
            this.Input_TabControl.Margin = new System.Windows.Forms.Padding(2);
            this.Input_TabControl.Name = "Input_TabControl";
            this.Input_TabControl.Padding = new System.Windows.Forms.Padding(2);
            this.Input_TabControl.Size = new System.Drawing.Size(1322, 576);
            this.Input_TabControl.TabIndex = 0;
            this.Input_TabControl.Text = "Inputs";
            this.Input_TabControl.UseVisualStyleBackColor = true;
            // 
            // NoOfRuns_textbox
            // 
            this.NoOfRuns_textbox.Location = new System.Drawing.Point(748, 98);
            this.NoOfRuns_textbox.Margin = new System.Windows.Forms.Padding(2);
            this.NoOfRuns_textbox.Name = "NoOfRuns_textbox";
            this.NoOfRuns_textbox.Size = new System.Drawing.Size(76, 20);
            this.NoOfRuns_textbox.TabIndex = 12;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(748, 83);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "#Trials";
            // 
            // InterarrivalTimeDist_Grpbox
            // 
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.MeanExponentialDist_InterarrivalTime_txtbox);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.label9);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.UniformDistB_txtbox);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.label5);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.UniformDistA_txtbox);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.label4);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.Exponential_rbtn);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.Uniform_rbtn);
            this.InterarrivalTimeDist_Grpbox.Controls.Add(this.Discrete_rbtn);
            this.InterarrivalTimeDist_Grpbox.Location = new System.Drawing.Point(330, 349);
            this.InterarrivalTimeDist_Grpbox.Margin = new System.Windows.Forms.Padding(2);
            this.InterarrivalTimeDist_Grpbox.Name = "InterarrivalTimeDist_Grpbox";
            this.InterarrivalTimeDist_Grpbox.Padding = new System.Windows.Forms.Padding(2);
            this.InterarrivalTimeDist_Grpbox.Size = new System.Drawing.Size(290, 103);
            this.InterarrivalTimeDist_Grpbox.TabIndex = 4;
            this.InterarrivalTimeDist_Grpbox.TabStop = false;
            this.InterarrivalTimeDist_Grpbox.Text = "Interarrival Distribution";
            // 
            // MeanExponentialDist_InterarrivalTime_txtbox
            // 
            this.MeanExponentialDist_InterarrivalTime_txtbox.Location = new System.Drawing.Point(196, 80);
            this.MeanExponentialDist_InterarrivalTime_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.MeanExponentialDist_InterarrivalTime_txtbox.Name = "MeanExponentialDist_InterarrivalTime_txtbox";
            this.MeanExponentialDist_InterarrivalTime_txtbox.Size = new System.Drawing.Size(76, 20);
            this.MeanExponentialDist_InterarrivalTime_txtbox.TabIndex = 18;
            this.MeanExponentialDist_InterarrivalTime_txtbox.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(196, 54);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Mean";
            this.label9.Visible = false;
            // 
            // UniformDistB_txtbox
            // 
            this.UniformDistB_txtbox.Location = new System.Drawing.Point(102, 74);
            this.UniformDistB_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.UniformDistB_txtbox.Name = "UniformDistB_txtbox";
            this.UniformDistB_txtbox.Size = new System.Drawing.Size(44, 20);
            this.UniformDistB_txtbox.TabIndex = 14;
            this.UniformDistB_txtbox.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(85, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "b";
            this.label5.Visible = false;
            // 
            // UniformDistA_txtbox
            // 
            this.UniformDistA_txtbox.Location = new System.Drawing.Point(102, 50);
            this.UniformDistA_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.UniformDistA_txtbox.Name = "UniformDistA_txtbox";
            this.UniformDistA_txtbox.Size = new System.Drawing.Size(44, 20);
            this.UniformDistA_txtbox.TabIndex = 12;
            this.UniformDistA_txtbox.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(85, 47);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "a";
            this.label4.Visible = false;
            // 
            // Exponential_rbtn
            // 
            this.Exponential_rbtn.AutoSize = true;
            this.Exponential_rbtn.Location = new System.Drawing.Point(196, 28);
            this.Exponential_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Exponential_rbtn.Name = "Exponential_rbtn";
            this.Exponential_rbtn.Size = new System.Drawing.Size(80, 17);
            this.Exponential_rbtn.TabIndex = 2;
            this.Exponential_rbtn.TabStop = true;
            this.Exponential_rbtn.Text = "Exponential";
            this.Exponential_rbtn.UseVisualStyleBackColor = true;
            this.Exponential_rbtn.CheckedChanged += new System.EventHandler(this.Exponential_rbtn_CheckedChanged);
            // 
            // Uniform_rbtn
            // 
            this.Uniform_rbtn.AutoSize = true;
            this.Uniform_rbtn.Location = new System.Drawing.Point(87, 28);
            this.Uniform_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Uniform_rbtn.Name = "Uniform_rbtn";
            this.Uniform_rbtn.Size = new System.Drawing.Size(61, 17);
            this.Uniform_rbtn.TabIndex = 1;
            this.Uniform_rbtn.TabStop = true;
            this.Uniform_rbtn.Text = "Uniform";
            this.Uniform_rbtn.UseVisualStyleBackColor = true;
            this.Uniform_rbtn.CheckedChanged += new System.EventHandler(this.Uniform_rbtn_CheckedChanged);
            // 
            // Discrete_rbtn
            // 
            this.Discrete_rbtn.AutoSize = true;
            this.Discrete_rbtn.Location = new System.Drawing.Point(5, 28);
            this.Discrete_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Discrete_rbtn.Name = "Discrete_rbtn";
            this.Discrete_rbtn.Size = new System.Drawing.Size(64, 17);
            this.Discrete_rbtn.TabIndex = 0;
            this.Discrete_rbtn.TabStop = true;
            this.Discrete_rbtn.Text = "Discrete";
            this.Discrete_rbtn.UseVisualStyleBackColor = true;
            this.Discrete_rbtn.CheckedChanged += new System.EventHandler(this.Discrete_rbtn_CheckedChanged);
            // 
            // CustomerStoppingCondition_txtbox
            // 
            this.CustomerStoppingCondition_txtbox.Location = new System.Drawing.Point(748, 51);
            this.CustomerStoppingCondition_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.CustomerStoppingCondition_txtbox.Name = "CustomerStoppingCondition_txtbox";
            this.CustomerStoppingCondition_txtbox.Size = new System.Drawing.Size(76, 20);
            this.CustomerStoppingCondition_txtbox.TabIndex = 10;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(746, 35);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Customer Stopping Condition";
            // 
            // Simulate_btn
            // 
            this.Simulate_btn.Location = new System.Drawing.Point(901, 376);
            this.Simulate_btn.Margin = new System.Windows.Forms.Padding(2);
            this.Simulate_btn.Name = "Simulate_btn";
            this.Simulate_btn.Size = new System.Drawing.Size(56, 33);
            this.Simulate_btn.TabIndex = 8;
            this.Simulate_btn.Text = "Simulate";
            this.Simulate_btn.UseVisualStyleBackColor = true;
            this.Simulate_btn.Click += new System.EventHandler(this.btnSimulate_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(536, 19);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Service Time Data";
            // 
            // ServiceTimeData_GridView
            // 
            this.ServiceTimeData_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ServiceTimeData_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ServiceTimeValues,
            this.ServiceTimeProbability});
            this.ServiceTimeData_GridView.Location = new System.Drawing.Point(538, 35);
            this.ServiceTimeData_GridView.Margin = new System.Windows.Forms.Padding(2);
            this.ServiceTimeData_GridView.Name = "ServiceTimeData_GridView";
            this.ServiceTimeData_GridView.RowTemplate.Height = 24;
            this.ServiceTimeData_GridView.Size = new System.Drawing.Size(182, 310);
            this.ServiceTimeData_GridView.TabIndex = 6;
            // 
            // ServiceTimeValues
            // 
            this.ServiceTimeValues.HeaderText = "Values";
            this.ServiceTimeValues.Name = "ServiceTimeValues";
            // 
            // ServiceTimeProbability
            // 
            this.ServiceTimeProbability.HeaderText = "Probability";
            this.ServiceTimeProbability.Name = "ServiceTimeProbability";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(328, 19);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Interarrival Time Data";
            // 
            // Interarrival_GridView
            // 
            this.Interarrival_GridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Interarrival_GridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InterarrivalValues,
            this.InterarrivalProbabilities});
            this.Interarrival_GridView.Location = new System.Drawing.Point(328, 35);
            this.Interarrival_GridView.Margin = new System.Windows.Forms.Padding(2);
            this.Interarrival_GridView.Name = "Interarrival_GridView";
            this.Interarrival_GridView.RowTemplate.Height = 24;
            this.Interarrival_GridView.Size = new System.Drawing.Size(183, 310);
            this.Interarrival_GridView.TabIndex = 4;
            // 
            // InterarrivalValues
            // 
            this.InterarrivalValues.HeaderText = "Values";
            this.InterarrivalValues.Name = "InterarrivalValues";
            // 
            // InterarrivalProbabilities
            // 
            this.InterarrivalProbabilities.HeaderText = "Probability";
            this.InterarrivalProbabilities.Name = "InterarrivalProbabilities";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ServerDistribution_grpbox);
            this.groupBox1.Controls.Add(this.ServerPriority_Grpbox);
            this.groupBox1.Controls.Add(this.ServerState_Grpbox);
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(317, 531);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server";
            // 
            // ServerDistribution_grpbox
            // 
            this.ServerDistribution_grpbox.Controls.Add(this.label12);
            this.ServerDistribution_grpbox.Controls.Add(this.textBox1);
            this.ServerDistribution_grpbox.Controls.Add(this.label11);
            this.ServerDistribution_grpbox.Controls.Add(this.MeanExponentialDist_Server_txtbox);
            this.ServerDistribution_grpbox.Controls.Add(this.AddServer_btn);
            this.ServerDistribution_grpbox.Controls.Add(this.label10);
            this.ServerDistribution_grpbox.Controls.Add(this.UniformB_Server_txtbox);
            this.ServerDistribution_grpbox.Controls.Add(this.label7);
            this.ServerDistribution_grpbox.Controls.Add(this.UniformA_Server_txtbox);
            this.ServerDistribution_grpbox.Controls.Add(this.label8);
            this.ServerDistribution_grpbox.Controls.Add(this.Exponentioal_Server_rbtn);
            this.ServerDistribution_grpbox.Controls.Add(this.Uniform_Server_rbtn);
            this.ServerDistribution_grpbox.Controls.Add(this.Discrete_Server_rbtn);
            this.ServerDistribution_grpbox.Location = new System.Drawing.Point(4, 313);
            this.ServerDistribution_grpbox.Margin = new System.Windows.Forms.Padding(2);
            this.ServerDistribution_grpbox.Name = "ServerDistribution_grpbox";
            this.ServerDistribution_grpbox.Padding = new System.Windows.Forms.Padding(2);
            this.ServerDistribution_grpbox.Size = new System.Drawing.Size(290, 164);
            this.ServerDistribution_grpbox.TabIndex = 15;
            this.ServerDistribution_grpbox.TabStop = false;
            this.ServerDistribution_grpbox.Text = "Server Distribution";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(167, 124);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 19;
            this.label12.Text = "priority";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(217, 121);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(47, 20);
            this.textBox1.TabIndex = 18;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(4, 102);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Server    ";
            // 
            // MeanExponentialDist_Server_txtbox
            // 
            this.MeanExponentialDist_Server_txtbox.Location = new System.Drawing.Point(198, 80);
            this.MeanExponentialDist_Server_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.MeanExponentialDist_Server_txtbox.Name = "MeanExponentialDist_Server_txtbox";
            this.MeanExponentialDist_Server_txtbox.Size = new System.Drawing.Size(76, 20);
            this.MeanExponentialDist_Server_txtbox.TabIndex = 16;
            this.MeanExponentialDist_Server_txtbox.Visible = false;
            // 
            // AddServer_btn
            // 
            this.AddServer_btn.Location = new System.Drawing.Point(7, 120);
            this.AddServer_btn.Margin = new System.Windows.Forms.Padding(2);
            this.AddServer_btn.Name = "AddServer_btn";
            this.AddServer_btn.Size = new System.Drawing.Size(102, 28);
            this.AddServer_btn.TabIndex = 3;
            this.AddServer_btn.Text = "Add Server";
            this.AddServer_btn.UseVisualStyleBackColor = true;
            this.AddServer_btn.Click += new System.EventHandler(this.AddServer_btn_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(199, 54);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 13);
            this.label10.TabIndex = 17;
            this.label10.Text = "Mean";
            this.label10.Visible = false;
            // 
            // UniformB_Server_txtbox
            // 
            this.UniformB_Server_txtbox.Location = new System.Drawing.Point(102, 74);
            this.UniformB_Server_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.UniformB_Server_txtbox.Name = "UniformB_Server_txtbox";
            this.UniformB_Server_txtbox.Size = new System.Drawing.Size(44, 20);
            this.UniformB_Server_txtbox.TabIndex = 14;
            this.UniformB_Server_txtbox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(85, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "b";
            this.label7.Visible = false;
            // 
            // UniformA_Server_txtbox
            // 
            this.UniformA_Server_txtbox.Location = new System.Drawing.Point(102, 50);
            this.UniformA_Server_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.UniformA_Server_txtbox.Name = "UniformA_Server_txtbox";
            this.UniformA_Server_txtbox.Size = new System.Drawing.Size(44, 20);
            this.UniformA_Server_txtbox.TabIndex = 12;
            this.UniformA_Server_txtbox.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(85, 47);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "a";
            this.label8.Visible = false;
            // 
            // Exponentioal_Server_rbtn
            // 
            this.Exponentioal_Server_rbtn.AutoSize = true;
            this.Exponentioal_Server_rbtn.Location = new System.Drawing.Point(196, 28);
            this.Exponentioal_Server_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Exponentioal_Server_rbtn.Name = "Exponentioal_Server_rbtn";
            this.Exponentioal_Server_rbtn.Size = new System.Drawing.Size(80, 17);
            this.Exponentioal_Server_rbtn.TabIndex = 2;
            this.Exponentioal_Server_rbtn.TabStop = true;
            this.Exponentioal_Server_rbtn.Text = "Exponential";
            this.Exponentioal_Server_rbtn.UseVisualStyleBackColor = true;
            this.Exponentioal_Server_rbtn.CheckedChanged += new System.EventHandler(this.Exponentioal_Server_rbtn_CheckedChanged);
            // 
            // Uniform_Server_rbtn
            // 
            this.Uniform_Server_rbtn.AutoSize = true;
            this.Uniform_Server_rbtn.Location = new System.Drawing.Point(87, 28);
            this.Uniform_Server_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Uniform_Server_rbtn.Name = "Uniform_Server_rbtn";
            this.Uniform_Server_rbtn.Size = new System.Drawing.Size(61, 17);
            this.Uniform_Server_rbtn.TabIndex = 1;
            this.Uniform_Server_rbtn.TabStop = true;
            this.Uniform_Server_rbtn.Text = "Uniform";
            this.Uniform_Server_rbtn.UseVisualStyleBackColor = true;
            this.Uniform_Server_rbtn.CheckedChanged += new System.EventHandler(this.Uniform_Server_rbtn_CheckedChanged);
            // 
            // Discrete_Server_rbtn
            // 
            this.Discrete_Server_rbtn.AutoSize = true;
            this.Discrete_Server_rbtn.Location = new System.Drawing.Point(5, 28);
            this.Discrete_Server_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Discrete_Server_rbtn.Name = "Discrete_Server_rbtn";
            this.Discrete_Server_rbtn.Size = new System.Drawing.Size(64, 17);
            this.Discrete_Server_rbtn.TabIndex = 0;
            this.Discrete_Server_rbtn.TabStop = true;
            this.Discrete_Server_rbtn.Text = "Discrete";
            this.Discrete_Server_rbtn.UseVisualStyleBackColor = true;
            this.Discrete_Server_rbtn.CheckedChanged += new System.EventHandler(this.Discrete_Server_rbtn_CheckedChanged);
            // 
            // ServerPriority_Grpbox
            // 
            this.ServerPriority_Grpbox.Controls.Add(this.HighestPriority_rbtn);
            this.ServerPriority_Grpbox.Controls.Add(this.LowestUtilization_rbtn);
            this.ServerPriority_Grpbox.Controls.Add(this.Random_rbtn);
            this.ServerPriority_Grpbox.Location = new System.Drawing.Point(4, 171);
            this.ServerPriority_Grpbox.Margin = new System.Windows.Forms.Padding(2);
            this.ServerPriority_Grpbox.Name = "ServerPriority_Grpbox";
            this.ServerPriority_Grpbox.Padding = new System.Windows.Forms.Padding(2);
            this.ServerPriority_Grpbox.Size = new System.Drawing.Size(299, 108);
            this.ServerPriority_Grpbox.TabIndex = 4;
            this.ServerPriority_Grpbox.TabStop = false;
            this.ServerPriority_Grpbox.Text = "Server Priority";
            // 
            // HighestPriority_rbtn
            // 
            this.HighestPriority_rbtn.AutoSize = true;
            this.HighestPriority_rbtn.Location = new System.Drawing.Point(201, 40);
            this.HighestPriority_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.HighestPriority_rbtn.Name = "HighestPriority_rbtn";
            this.HighestPriority_rbtn.Size = new System.Drawing.Size(95, 17);
            this.HighestPriority_rbtn.TabIndex = 2;
            this.HighestPriority_rbtn.TabStop = true;
            this.HighestPriority_rbtn.Text = "Highest Priority";
            this.HighestPriority_rbtn.UseVisualStyleBackColor = true;
            this.HighestPriority_rbtn.CheckedChanged += new System.EventHandler(this.HighestPriority_rbtn_CheckedChanged);
            // 
            // LowestUtilization_rbtn
            // 
            this.LowestUtilization_rbtn.AutoSize = true;
            this.LowestUtilization_rbtn.Location = new System.Drawing.Point(76, 40);
            this.LowestUtilization_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.LowestUtilization_rbtn.Name = "LowestUtilization_rbtn";
            this.LowestUtilization_rbtn.Size = new System.Drawing.Size(111, 17);
            this.LowestUtilization_rbtn.TabIndex = 1;
            this.LowestUtilization_rbtn.TabStop = true;
            this.LowestUtilization_rbtn.Text = "Lowest Utilazation";
            this.LowestUtilization_rbtn.UseVisualStyleBackColor = true;
            this.LowestUtilization_rbtn.CheckedChanged += new System.EventHandler(this.LowestUtilization_rbtn_CheckedChanged);
            // 
            // Random_rbtn
            // 
            this.Random_rbtn.AutoSize = true;
            this.Random_rbtn.Location = new System.Drawing.Point(4, 40);
            this.Random_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.Random_rbtn.Name = "Random_rbtn";
            this.Random_rbtn.Size = new System.Drawing.Size(65, 17);
            this.Random_rbtn.TabIndex = 0;
            this.Random_rbtn.TabStop = true;
            this.Random_rbtn.Text = "Random";
            this.Random_rbtn.UseVisualStyleBackColor = true;
            this.Random_rbtn.CheckedChanged += new System.EventHandler(this.Random_rbtn_CheckedChanged);
            // 
            // ServerState_Grpbox
            // 
            this.ServerState_Grpbox.Controls.Add(this.NoOfServers_txtbox);
            this.ServerState_Grpbox.Controls.Add(this.label6);
            this.ServerState_Grpbox.Controls.Add(this.MultipleServer_rbtn);
            this.ServerState_Grpbox.Controls.Add(this.SingleServer_rbtn);
            this.ServerState_Grpbox.Location = new System.Drawing.Point(4, 30);
            this.ServerState_Grpbox.Margin = new System.Windows.Forms.Padding(2);
            this.ServerState_Grpbox.Name = "ServerState_Grpbox";
            this.ServerState_Grpbox.Padding = new System.Windows.Forms.Padding(2);
            this.ServerState_Grpbox.Size = new System.Drawing.Size(234, 102);
            this.ServerState_Grpbox.TabIndex = 4;
            this.ServerState_Grpbox.TabStop = false;
            this.ServerState_Grpbox.Text = "Server State";
            // 
            // NoOfServers_txtbox
            // 
            this.NoOfServers_txtbox.Location = new System.Drawing.Point(143, 63);
            this.NoOfServers_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.NoOfServers_txtbox.Name = "NoOfServers_txtbox";
            this.NoOfServers_txtbox.Size = new System.Drawing.Size(76, 20);
            this.NoOfServers_txtbox.TabIndex = 11;
            this.NoOfServers_txtbox.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(140, 48);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "#Of Servers";
            this.label6.Visible = false;
            // 
            // MultipleServer_rbtn
            // 
            this.MultipleServer_rbtn.AutoSize = true;
            this.MultipleServer_rbtn.Location = new System.Drawing.Point(142, 28);
            this.MultipleServer_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.MultipleServer_rbtn.Name = "MultipleServer_rbtn";
            this.MultipleServer_rbtn.Size = new System.Drawing.Size(95, 17);
            this.MultipleServer_rbtn.TabIndex = 0;
            this.MultipleServer_rbtn.TabStop = true;
            this.MultipleServer_rbtn.Text = "Multiple Server";
            this.MultipleServer_rbtn.UseVisualStyleBackColor = true;
            this.MultipleServer_rbtn.CheckedChanged += new System.EventHandler(this.MultipleServer_rbtn_CheckedChanged);
            // 
            // SingleServer_rbtn
            // 
            this.SingleServer_rbtn.AutoSize = true;
            this.SingleServer_rbtn.Location = new System.Drawing.Point(4, 28);
            this.SingleServer_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.SingleServer_rbtn.Name = "SingleServer_rbtn";
            this.SingleServer_rbtn.Size = new System.Drawing.Size(85, 17);
            this.SingleServer_rbtn.TabIndex = 1;
            this.SingleServer_rbtn.TabStop = true;
            this.SingleServer_rbtn.Text = "SingleServer";
            this.SingleServer_rbtn.UseVisualStyleBackColor = true;
            this.SingleServer_rbtn.CheckedChanged += new System.EventHandler(this.SingleServer_rbtn_CheckedChanged);
            // 
            // AVGTimeBetweenInterarrivals_txtbox
            // 
            this.AVGTimeBetweenInterarrivals_txtbox.Controls.Add(this.groupBox2);
            this.AVGTimeBetweenInterarrivals_txtbox.Controls.Add(this.label14);
            this.AVGTimeBetweenInterarrivals_txtbox.Controls.Add(this.RunNo_comboBox);
            this.AVGTimeBetweenInterarrivals_txtbox.Controls.Add(this.dataGridView1);
            this.AVGTimeBetweenInterarrivals_txtbox.Location = new System.Drawing.Point(4, 22);
            this.AVGTimeBetweenInterarrivals_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.AVGTimeBetweenInterarrivals_txtbox.Name = "AVGTimeBetweenInterarrivals_txtbox";
            this.AVGTimeBetweenInterarrivals_txtbox.Padding = new System.Windows.Forms.Padding(2);
            this.AVGTimeBetweenInterarrivals_txtbox.Size = new System.Drawing.Size(1322, 576);
            this.AVGTimeBetweenInterarrivals_txtbox.TabIndex = 1;
            this.AVGTimeBetweenInterarrivals_txtbox.Text = "tabPage2";
            this.AVGTimeBetweenInterarrivals_txtbox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AVGTimeCustomersSpendInSys_txtbox);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.AVGWaitingTimeOfThoseWhoWait_txtbox);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.ProbabilityOfCustomerHasToWaitIQueue_txtbox);
            this.groupBox2.Controls.Add(this.AverageServiceTime_txtbox);
            this.groupBox2.Controls.Add(this.ProbOfIdleServer_txtbox);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.AVGWaitingTime_txtbox);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Location = new System.Drawing.Point(1025, 146);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(294, 336);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "System ";
            // 
            // AVGTimeCustomersSpendInSys_txtbox
            // 
            this.AVGTimeCustomersSpendInSys_txtbox.Location = new System.Drawing.Point(140, 270);
            this.AVGTimeCustomersSpendInSys_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.AVGTimeCustomersSpendInSys_txtbox.Name = "AVGTimeCustomersSpendInSys_txtbox";
            this.AVGTimeCustomersSpendInSys_txtbox.ReadOnly = true;
            this.AVGTimeCustomersSpendInSys_txtbox.Size = new System.Drawing.Size(76, 20);
            this.AVGTimeCustomersSpendInSys_txtbox.TabIndex = 16;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 261);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(107, 26);
            this.label16.TabIndex = 5;
            this.label16.Text = "Avg. time cus. \r\nSpends in the system";
            // 
            // AVGWaitingTimeOfThoseWhoWait_txtbox
            // 
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.Location = new System.Drawing.Point(140, 230);
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.Name = "AVGWaitingTimeOfThoseWhoWait_txtbox";
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.ReadOnly = true;
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.Size = new System.Drawing.Size(76, 20);
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(11, 221);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 26);
            this.label17.TabIndex = 6;
            this.label17.Text = "Avg. waiting time \r\nof those who wait ";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(154, 182);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(76, 20);
            this.textBox2.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 186);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(146, 13);
            this.label18.TabIndex = 7;
            this.label18.Text = "Average time betweenarrivals";
            // 
            // ProbabilityOfCustomerHasToWaitIQueue_txtbox
            // 
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.Location = new System.Drawing.Point(151, 75);
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.Name = "ProbabilityOfCustomerHasToWaitIQueue_txtbox";
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.ReadOnly = true;
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.Size = new System.Drawing.Size(76, 20);
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.TabIndex = 13;
            // 
            // AverageServiceTime_txtbox
            // 
            this.AverageServiceTime_txtbox.Cursor = System.Windows.Forms.Cursors.No;
            this.AverageServiceTime_txtbox.Location = new System.Drawing.Point(138, 150);
            this.AverageServiceTime_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.AverageServiceTime_txtbox.Name = "AverageServiceTime_txtbox";
            this.AverageServiceTime_txtbox.ReadOnly = true;
            this.AverageServiceTime_txtbox.Size = new System.Drawing.Size(76, 20);
            this.AverageServiceTime_txtbox.TabIndex = 12;
            // 
            // ProbOfIdleServer_txtbox
            // 
            this.ProbOfIdleServer_txtbox.Location = new System.Drawing.Point(140, 116);
            this.ProbOfIdleServer_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.ProbOfIdleServer_txtbox.Name = "ProbOfIdleServer_txtbox";
            this.ProbOfIdleServer_txtbox.ReadOnly = true;
            this.ProbOfIdleServer_txtbox.Size = new System.Drawing.Size(76, 20);
            this.ProbOfIdleServer_txtbox.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(11, 150);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(109, 13);
            this.label19.TabIndex = 8;
            this.label19.Text = "Average service time ";
            // 
            // AVGWaitingTime_txtbox
            // 
            this.AVGWaitingTime_txtbox.Location = new System.Drawing.Point(138, 38);
            this.AVGWaitingTime_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.AVGWaitingTime_txtbox.Name = "AVGWaitingTime_txtbox";
            this.AVGWaitingTime_txtbox.ReadOnly = true;
            this.AVGWaitingTime_txtbox.Size = new System.Drawing.Size(76, 20);
            this.AVGWaitingTime_txtbox.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 116);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(121, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "Probability of idle server ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(11, 65);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(134, 26);
            this.label21.TabIndex = 5;
            this.label21.Text = "Probability that a customer \r\nhas to wait in the queue\r\n";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 38);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 13);
            this.label15.TabIndex = 4;
            this.label15.Text = "AVG Waiting Time";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1025, 59);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Run #";
            // 
            // RunNo_comboBox
            // 
            this.RunNo_comboBox.FormattingEnabled = true;
            this.RunNo_comboBox.Location = new System.Drawing.Point(1025, 87);
            this.RunNo_comboBox.Margin = new System.Windows.Forms.Padding(2);
            this.RunNo_comboBox.Name = "RunNo_comboBox";
            this.RunNo_comboBox.Size = new System.Drawing.Size(92, 21);
            this.RunNo_comboBox.TabIndex = 1;
            this.RunNo_comboBox.SelectedIndexChanged += new System.EventHandler(this.RunNo_comboBox_SelectedIndexChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(33, 23);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(949, 515);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.zedGraphControl1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1322, 576);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Chart";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(488, 493);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 42);
            this.button1.TabIndex = 1;
            this.button1.Text = "Draw Chart";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(32, 6);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(957, 448);
            this.zedGraphControl1.TabIndex = 0;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(766, 493);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(167, 42);
            this.button2.TabIndex = 2;
            this.button2.Text = "Clear Chart";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // MultipleServerSimulation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1298, 602);
            this.Controls.Add(this.tabControl1);
            this.Name = "MultipleServerSimulation";
            this.Text = "Multi Server Simulation";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tabControl1.ResumeLayout(false);
            this.Input_TabControl.ResumeLayout(false);
            this.Input_TabControl.PerformLayout();
            this.InterarrivalTimeDist_Grpbox.ResumeLayout(false);
            this.InterarrivalTimeDist_Grpbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ServiceTimeData_GridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Interarrival_GridView)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ServerDistribution_grpbox.ResumeLayout(false);
            this.ServerDistribution_grpbox.PerformLayout();
            this.ServerPriority_Grpbox.ResumeLayout(false);
            this.ServerPriority_Grpbox.PerformLayout();
            this.ServerState_Grpbox.ResumeLayout(false);
            this.ServerState_Grpbox.PerformLayout();
            this.AVGTimeBetweenInterarrivals_txtbox.ResumeLayout(false);
            this.AVGTimeBetweenInterarrivals_txtbox.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Input_TabControl;
        private System.Windows.Forms.TabPage AVGTimeBetweenInterarrivals_txtbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox ServerState_Grpbox;
        private System.Windows.Forms.RadioButton MultipleServer_rbtn;
        private System.Windows.Forms.RadioButton SingleServer_rbtn;
        private System.Windows.Forms.GroupBox ServerPriority_Grpbox;
        private System.Windows.Forms.RadioButton HighestPriority_rbtn;
        private System.Windows.Forms.RadioButton LowestUtilization_rbtn;
        private System.Windows.Forms.RadioButton Random_rbtn;
        private System.Windows.Forms.GroupBox InterarrivalTimeDist_Grpbox;
        private System.Windows.Forms.RadioButton Exponential_rbtn;
        private System.Windows.Forms.RadioButton Uniform_rbtn;
        private System.Windows.Forms.RadioButton Discrete_rbtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView Interarrival_GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn InterarrivalValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn InterarrivalProbabilities;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView ServiceTimeData_GridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceTimeValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServiceTimeProbability;
        private System.Windows.Forms.Button Simulate_btn;
        private System.Windows.Forms.TextBox CustomerStoppingCondition_txtbox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UniformDistB_txtbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox UniformDistA_txtbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NoOfServers_txtbox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox ServerDistribution_grpbox;
        private System.Windows.Forms.TextBox UniformB_Server_txtbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox UniformA_Server_txtbox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton Exponentioal_Server_rbtn;
        private System.Windows.Forms.RadioButton Uniform_Server_rbtn;
        private System.Windows.Forms.RadioButton Discrete_Server_rbtn;
        private System.Windows.Forms.TextBox MeanExponentialDist_Server_txtbox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox MeanExponentialDist_InterarrivalTime_txtbox;
        private System.Windows.Forms.Button AddServer_btn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox NoOfRuns_textbox;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox RunNo_comboBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox AVGWaitingTime_txtbox;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox ProbOfIdleServer_txtbox;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox ProbabilityOfCustomerHasToWaitIQueue_txtbox;
        private System.Windows.Forms.TextBox AverageServiceTime_txtbox;
        private System.Windows.Forms.TextBox AVGTimeCustomersSpendInSys_txtbox;
        private System.Windows.Forms.TextBox AVGWaitingTimeOfThoseWhoWait_txtbox;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button1;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.Button button2;



    }
}

