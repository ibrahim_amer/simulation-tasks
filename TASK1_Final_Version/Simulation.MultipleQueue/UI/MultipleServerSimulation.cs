﻿using System;
using System.Windows.Forms;
using MultipleQueue.Simulation;
using MultipleQueue.Distributions;
using System.Collections.Generic;
using MultipleQueue.Entities;
using ZedGraph;
using System.Drawing;
namespace MultipleQueue
{
    public partial class MultipleServerSimulation : Form
    {
        Distributions.DistributionTypes ServerDistType, InterarrivalDistType;
        ServerSelectionType ServerSelectionCriteria;
        ServerSelection serverSelection;
        IDistribution ServerDist, InterarrivalDist;
        SimulationManager Manager = new SimulationManager();
        List<float> ServiceTimeValues_List, ServiceTimeProbabilties_List, InterarrivalTimeValues_List, InterarrivalTimeProbabilties_List;
        List<Server> Servers_List;
        int ServersCount = 0;
        bool ServiceTimeDataInitialized = false;



        public MultipleServerSimulation()
        {
            this.InitializeComponent();
            this.InitializeForm();
        }

        public void InitializeForm()
        {
            this.ServiceTimeValues_List = new List<float>();
            
            this.tabControl1.TabPages[0].Text = "Inputs";
            this.tabControl1.TabPages[1].Text = "Outputs";
            // this.ServiceTimeDist_Grpbox.Visible = false;
            //this.ServiceTimeData_GridView.Visible = false;
            this.UniformDistA_txtbox.Visible = this.UniformDistB_txtbox.Visible = false;

            this.ServiceTimeProbabilties_List = new List<float>();
            this.ServiceTimeProbabilties_List = new List<float>();
            this.InterarrivalTimeValues_List = new List<float>();
            this.InterarrivalTimeProbabilties_List = new List<float>();
            this.Servers_List = new List<Server>();
            
        }

        private void btnSimulate_Click(object sender, EventArgs e)
        {
            this.SetInterarrivalTimeData();
            this.SetInterarrivalTimeDistributionType();
            //this.IntializeServersList();
            this.InitializeNoOfRunsComboBox();
            this.Manager = new SimulationManager();
            
            this.Manager.StartSimulation(this.serverSelection, this.InterarrivalDist, this.Servers_List,
                int.Parse(this.CustomerStoppingCondition_txtbox.Text), int.Parse(this.NoOfRuns_textbox.Text));
            this.ServersCount = 0;
        }
       
        public void InitializeNoOfRunsComboBox()
        {
            int NoOfRuns = int.Parse(this.NoOfRuns_textbox.Text);
            for (int i = 0; i < NoOfRuns; i++)
            {
                this.RunNo_comboBox.Items.Add("Run " + i.ToString());
            }
        }
        //Unused Function
        private void IntializeServersList()
        {
            if (this.SingleServer_rbtn.Checked)
               this.Servers_List.Add(new Server(this.ServerDist, this.GetServerPriority()));
            else if (this.MultipleServer_rbtn.Checked)
            {
                int serversCount = int.Parse(this.NoOfServers_txtbox.Text);
                for (int i = 0; i < serversCount; i++)
                {
                    this.Servers_List.Add(new Server(this.ServerDist, this.GetServerPriority()));
                }
            }
             
        }

        private int? GetServerPriority()
        {
            if (this.ServerSelectionCriteria == ServerSelectionType.HighestPriority)
            {
                this.serverSelection = new ServerSelection(ServerSelectionType.HighestPriority);
                return (int)ServerSelectionType.HighestPriority;
            }
            else if (this.ServerSelectionCriteria == ServerSelectionType.LowestUtilization)
            {
                this.serverSelection = new ServerSelection(ServerSelectionType.LowestUtilization);
                return (int)ServerSelectionType.LowestUtilization;
            }
            else if (this.ServerSelectionCriteria == ServerSelectionType.Random)
            {
                this.serverSelection = new ServerSelection(ServerSelectionType.Random);
                return (int)ServerSelectionType.Random;
            }
            return null;
        }
        /// <summary>
        /// Initialize InterarrivalTime Distribution
        /// </summary>
        public void SetInterarrivalTimeDistributionType()
        {
             if (InterarrivalDistType == DistributionTypes.Discrete)
                 this.InterarrivalDist = new DiscreteDistribution(this.InterarrivalTimeValues_List, this.InterarrivalTimeProbabilties_List);
             else if (this.InterarrivalDistType == DistributionTypes.Uniform)
                 this.InterarrivalDist = new UniformDistribution(float.Parse(this.UniformDistA_txtbox.Text)
                     , float.Parse(this.UniformDistB_txtbox.Text));
             else
                 this.InterarrivalDist = new ExponentialDistribution(float.Parse(this.MeanExponentialDist_InterarrivalTime_txtbox.Text));

        }
        /// <summary>
        /// Intialize Server's Distribution
        /// </summary>
        public void SetServerDistributionType()
        {
            if (ServerDistType == DistributionTypes.Discrete)
                this.ServerDist = new DiscreteDistribution(this.ServiceTimeValues_List, this.ServiceTimeProbabilties_List);
            else if (this.ServerDistType == DistributionTypes.Uniform)
                this.ServerDist = new UniformDistribution(float.Parse(this.UniformA_Server_txtbox.Text),
                float.Parse(this.UniformB_Server_txtbox.Text));
            else
                this.ServerDist = new ExponentialDistribution(float.Parse(this.MeanExponentialDist_Server_txtbox.Text));
        }

        private void SetServiceTimeData()
        {
            DataGridViewRowCollection Rows = this.ServiceTimeData_GridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.ServiceTimeValues_List.Add(float.Parse(cells[0].Value.ToString()));
                this.ServiceTimeProbabilties_List.Add(float.Parse(cells[1].Value.ToString()));
            }
            this.ServiceTimeDataInitialized = true;
        }

        public void SetInterarrivalTimeData()
        {
            DataGridViewRowCollection Rows = this.Interarrival_GridView.Rows;
            DataGridViewCellCollection cells;
            Rows = this.Interarrival_GridView.Rows;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.InterarrivalTimeValues_List.Add(float.Parse(cells[0].Value.ToString()));
                this.InterarrivalTimeProbabilties_List.Add(float.Parse(cells[1].Value.ToString()));
            }
        }
        
        private void Random_rbtn_CheckedChanged(object sender, EventArgs e)
        {

            this.ServerSelectionCriteria = ServerSelectionType.Random;
        }

        private void LowestUtilization_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.ServerSelectionCriteria = ServerSelectionType.LowestUtilization;
        }

        private void HighestPriority_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.ServerSelectionCriteria = ServerSelectionType.HighestPriority;
        }

        private void Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.InterarrivalDistType = DistributionTypes.Discrete;
        }

        private void Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.MeanExponentialDist_InterarrivalTime_txtbox.Visible = !this.MeanExponentialDist_InterarrivalTime_txtbox.Visible;
            this.label9.Visible = !this.label9.Visible;
            this.InterarrivalDistType = DistributionTypes.Exponential;
        }

        private void Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.UniformDistA_txtbox.Visible = !this.UniformDistA_txtbox.Visible;
            this.UniformDistB_txtbox.Visible = !this.UniformDistB_txtbox.Visible;
            this.label4.Visible = !this.label4.Visible;
            this.label5.Visible = !this.label5.Visible;
            this.InterarrivalDistType = DistributionTypes.Uniform;
        }

        private void SingleServer_rbtn_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void MultipleServer_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label6.Visible = !this.label6.Visible;
            this.label11.Text = "Server " + (this.ServersCount).ToString();
            this.NoOfServers_txtbox.Visible = !this.NoOfServers_txtbox.Visible;
        }

        private void Discrete_Server_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.ServerDistType = DistributionTypes.Discrete;
        }

        private void Uniform_Server_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.UniformA_Server_txtbox.Visible = !this.UniformDistA_txtbox.Visible;
            this.UniformB_Server_txtbox.Visible = !this.UniformDistB_txtbox.Visible;
            this.label8.Visible = !this.label4.Visible;
            this.label7.Visible = !this.label5.Visible;
            this.ServerDistType = DistributionTypes.Uniform;
        }

        private void Exponentioal_Server_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.MeanExponentialDist_Server_txtbox.Visible = !this.MeanExponentialDist_Server_txtbox.Visible;
            this.label10.Visible = !this.label10.Visible;
            this.ServerDistType = DistributionTypes.Exponential;
        }

        private void AddServer_btn_Click(object sender, EventArgs e)
        {
            int priority = 0; 
            if (!this.ServiceTimeDataInitialized)
                this.SetServiceTimeData();
            this.GetServerPriority();
            if (this.SingleServer_rbtn.Checked)
            {
                this.SetServerDistributionType();
                this.Servers_List.Add(new Server(this.ServerDist, 0));
                this.ServersCount++;
            }
            else if (this.ServersCount != int.Parse(this.NoOfServers_txtbox.Text) && this.MultipleServer_rbtn.Checked)
            {
                //this.label11.Text = "Server " + (this.ServersCount ).ToString();
                if (this.serverSelection.priorityType == ServerSelectionType.HighestPriority)
                {
                    priority = int.Parse(textBox1.Text);
                }
                this.SetServerDistributionType();
                this.Servers_List.Add(new Server(this.ServerDist, priority));
                this.ServersCount++;
            }
            this.label11.Text = "Server " + (this.ServersCount).ToString();
        }

        private void RunNo_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = this.RunNo_comboBox.SelectedIndex;
            this.groupBox2.Text = "System " + i.ToString() + "Performance";
            this.dataGridView1.DataSource = this.Manager.DisplaySimulationTable(i);
            this.InitializePerformanceData(this.Manager.SystemModels[i]);
        }

        public void InitializePerformanceData(Model m)
        {
            this.AVGWaitingTime_txtbox.Text = m.AverageDelay.ToString();
            this.ProbabilityOfCustomerHasToWaitIQueue_txtbox.Text = m.ProbabilityOfCustomerHasToWaitIQueue.ToString();
            this.ProbOfIdleServer_txtbox.Text = m.ProbabilityOfIdleServer.ToString();
            this.AverageServiceTime_txtbox.Text = m.AVGServiceTime.ToString();
            this.textBox2.Text = m.AVGTimeBetweenArrivals.ToString();
            this.AVGWaitingTimeOfThoseWhoWait_txtbox.Text = m.AvgWaitingTimeOfThoseWhoWait_.ToString();
            this.AVGTimeCustomersSpendInSys_txtbox.Text = m.AVGTimeCustomerSpendsInTheSystem.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                int m = this.RunNo_comboBox.SelectedIndex;
                int[] waitCount;
                waitCount =  Manager.DrawCharts(m);
          
               // Draw

                GraphPane myPane = zedGraphControl1.GraphPane;
                myPane.Title.Text = "Frequency of Individual Customer Waiting in Queue";
                myPane.XAxis.Title.Text = "Wait for";
                myPane.YAxis.Title.Text = "Customer numbers";

                // Create Bar
                PointPairList list1 = new PointPairList();

                for (int i = 0; i < waitCount.Length; i++)
                {
                    list1.Add(i, waitCount[i] ); //Percentage  * 100) / Manager.SystemModels[m].CompletedCustomers.Count);
                }


                BarItem bar1 = myPane.AddBar("Bar 1", list1, Color.Red);
                bar1.Bar.Fill = new Fill(Color.Red, Color.White, Color.Red, 90);


                // Set BarBase to the XAxis for horizontal bars
                myPane.BarSettings.Base = BarBase.X;


                // Fill the axis background with a color gradient
                myPane.Chart.Fill = new Fill(Color.White,
                   Color.FromArgb(255, 255, 166), 45.0F);

                zedGraphControl1.AxisChange();

                // Create TextObj's to provide labels for each bar
                BarItem.CreateBarLabels(myPane, true, "f0");
                zedGraphControl1.AxisChange();
                zedGraphControl1.Invalidate();
                zedGraphControl1.Refresh();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.Refresh();
        }

        
    }
}
