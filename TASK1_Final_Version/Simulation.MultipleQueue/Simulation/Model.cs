﻿using System;
using System.Collections.Generic;
using MultipleQueue.Distributions;
using MultipleQueue.Entities;
using System.Collections;
using System.Windows.Forms;

namespace MultipleQueue.Simulation
{
    public class Model
    {
        #region Data & properties
        public float ProbaThatTheCusHaveToWait_;
        public float[] AvgServicePerServer { get; set; }
        public float AvgWaitingTimeOfThoseWhoWait_ ;
        public float[] UtilizationOfEachServer { get; set; }

        /// <summary>
        /// The server priority rule
        /// </summary>
        private ServerSelection serverSelection;

        /// <summary>
        /// Customers inter-arrival distribution
        /// </summary>
        private Distributions.IDistribution customerInterArrivalDistribution;

        /// <summary>
        /// Number of customer at which to stop simulation.
        /// </summary>
        public int CustomerCountStoppingCondition;

        /// <summary>
        /// Gets the servers.
        /// </summary>
        public List<Server> Servers { get; private set; }

        /// <summary>
        /// The customers queue, the queue is changed with time according to the current number of customer waiting.
        /// </summary>
        private Queue<Customer> customerQueue;

        /// <summary>
        /// The list of customers who finished their service.
        /// </summary>
        public List<Customer> CompletedCustomers;

        /// <summary>
        /// Contains the future events sorted by time ASC
        /// </summary>
        private SortedList<Customer, EventType> eventsList;

        /// <summary>
        /// The clock of the simulation, changes to the time of the next event with every cycle of the run loop
        /// </summary>
        public float clock;

        /// <summary>
        /// Gets or sets the average waiting time of customers in the system.
        /// </summary>
        /// <value>
        /// The average waiting time.
        /// </value>
        public float AverageDelay { get; set; }

        /// <summary>
        /// Gets or sets the probability that a customer will wait in the system.
        /// </summary>
        /// <value>
        /// The probability of wait.
        /// </value>
        public float AVGWaitedCustomers { get; set; }
        public float ProbabilityOfCustomerHasToWaitIQueue { get; set; }

        public float ProbabilityOfIdleServer { get; set; }

        public float AVGServiceTime { get; set; }
        public float AVGWaitingTimeOfThoseWhoWait { get; set; }
        public float AVGTimeBetweenArrivals { get; set; }
        public float AVGTimeCustomerSpendsInTheSystem { get; set; }

        float lastArrivalTime = 0;

        float arrivalTime = 0;
        #endregion

        #region Constructor
        //TODO: Implement and document Initialize
        public Model(ServerSelection serverSelection,
                    IDistribution customerInterArrivalDistribution,
                    List<Server> servers,
                    int customerCountStoppingCondition)
        {
            this.serverSelection = serverSelection;
            this.customerInterArrivalDistribution = customerInterArrivalDistribution;
            this.Servers = servers;
            this.CustomerCountStoppingCondition = customerCountStoppingCondition;

            Initialize();
            Run();
            // CalculateSystemPerformance();            
        }
        #endregion

        #region Initialize
        private void Initialize()
        {
            clock = 0;
            CompletedCustomers = new List<Customer>();
            customerQueue = new Queue<Customer>();

            for (int i = 0; i < Servers.Count; i++)
            {
                Servers[i] = new Server(Servers[i].serviceTimeDistribution, Servers[i].Priority);
            }

            // Aya : i still need to intialize objects related to the preformence 

        }
        #endregion

        #region Generate Customer
        private Customer GenerateCustomer()
        {
            float value = customerInterArrivalDistribution.GetValue(LCGRandomNumberGenerator.GetVariate());
            lastArrivalTime = value + lastArrivalTime;
            Customer newCustomer = new Customer(value, lastArrivalTime);

            return newCustomer;
        }
        #endregion

        #region System Events
        #region Arrive Customer
        //TODO: Implement and document ArriveCustomer
        private void ArriveCustomer(Customer arrivingCustomer)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region DepartCustomer
        //TODO: Implement and document DepartCustomer
        private void DepartCustomer(Customer departingCustomer)
        {
            throw new NotImplementedException();
        }
        #endregion
        #endregion

        #region Assign to Server

        void NewEvent(Customer customer)
        {
            //First: Check all waited customers : 
            CheckWaitedCustomers(customer);
            //Then: handel the current customer :
            if (customerQueue.Count == 0)
                AssignServerToNewCustomer(customer);
            else
                customerQueue.Enqueue(customer);
        }
        void CheckWaitedCustomers(Customer customer)
        {
            while (true)
            {
                if (customerQueue.Count == 0)
                {
                    break;
                }
                Customer myCustomer = customerQueue.Peek();

                SortedList<float, Server> freeServers = GetTheFreeServers(customer);
                //No More Free Servers :
                if (freeServers.Count == 0)
                    break;
                else
                {
                    customerQueue.Dequeue();
                    Server myServer = freeServers.Values[0];

                    //Add it to a list
                    List<Server> SelectedServer = new List<Server>();
                    SelectedServer.Add(myServer);

                    //Get the time since it was free  
                    float t = myServer.ServerStatusList[myServer.ServerStatusList.Count - 1].ToTime;

                    foreach (Server s in freeServers.Values)
                    {
                        if ((s.ServerStatusList[s.ServerStatusList.Count - 1]).ToTime == t)
                            SelectedServer.Add(s);
                    }
                    updateUtlization(SelectedServer, t);

                   int index =  serverSelection.GetServerByPriority(SelectedServer, t);

                    //Assign To Server:
                    AssignCustomerToServer(myCustomer, index);

                    myCustomer.WaitTime = t - myCustomer.ArrivalTime;
                    myCustomer.DepartureTime = t + myCustomer.ServiceTime;
                }
            }
        }
        void AssignServerToNewCustomer(Customer customer)
        {
            SortedList<float, Server> freeServers = GetTheFreeServers(customer);
            // convert the sortedlist to list : 
            List<Server> list = new List<Server>();
            list.AddRange(freeServers.Values);

            if (list.Count != 0)
            {
                updateUtlization(list, customer.ArrivalTime);

                int index = serverSelection.GetServerByPriority(list, customer.ArrivalTime);
                Server myServer = list[index];
                //Get the real index of this server 
                index = Servers.IndexOf(myServer);

                AssignCustomerToServer(customer, index);

                customer.WaitTime = 0;
                customer.DepartureTime = customer.ArrivalTime + customer.ServiceTime;
            }
            else
                customerQueue.Enqueue(customer);
        }
        private void AssignCustomerToServer(Customer customer, int serverIndex)
        {
            Server myServer = Servers[serverIndex];

            //Updates:
            customer.ServerIndex = serverIndex;
            CompletedCustomers.Add(customer);

            //Assign a service time for this cutomer 
            float serviceTime = myServer.GetNextServerTimeDistribution();
            customer.ServiceTime = serviceTime;
            float clock;
            //Get the current clock
            if (myServer.ServerStatusList.Count != 0)
            {
                clock = myServer.ServerStatusList[myServer.ServerStatusList.Count - 1].ToTime;
                if (customer.ArrivalTime > clock)
                    clock = customer.ArrivalTime;
            }
            else
                clock = customer.ArrivalTime;

            //Add new "ServerBusyTime" to this server
            myServer.ServerStatusList.Add(new ServerBusyTime(clock, clock + serviceTime));

            myServer.TotalCustomerServed++;
            myServer.TotalServiceTime += serviceTime;

            // Aya : be noted that u still need updates related to the performance  
        }
        SortedList<float, Server> GetTheFreeServers(Customer customer)
        {
            SortedList<float, Server> freeServers = new SortedList<float, Server>();
            int hh = 0;

            foreach (Server server in Servers)
            {
                int lenght = server.ServerStatusList.Count;
                // Get last "serverBusyTime" for this server 
                if (server.ServerStatusList.Count != 0)
                {
                    ServerBusyTime serverBusyTime = server.ServerStatusList[server.ServerStatusList.Count - 1];
                    // Check if it's "ToTime" were less than or equal the Arrival time of the new customer 
                    if (serverBusyTime.ToTime <= customer.ArrivalTime)
                        if (freeServers.ContainsKey(serverBusyTime.ToTime))
                            try { freeServers.Add(serverBusyTime.ToTime + 1, server); }
                            catch { }
                        else
                            freeServers.Add(serverBusyTime.ToTime, server);
                }
                else// first assign for this server
                {
                    try
                    {
                        freeServers.Add(customer.ArrivalTime + hh, server);
                        hh++;
                    }
                    catch
                    {
                        freeServers.Add(customer.ArrivalTime + hh + 1, server);
                        hh++;
                    }
                }
            }
            return freeServers;

        }
        void updateUtlization(List<Server> freeServers, float clock)
        {
            foreach (Server serv in freeServers)
            {
                serv.Utilization = serv.TotalServiceTime / clock;
            }
        }

        #endregion

        #region Run

        public void Run()
        {
            Customer newCustomer = new Customer(0, 0);
            for (int i = 0; i < CustomerCountStoppingCondition; i++)
            {
                newCustomer = GenerateCustomer();
                NewEvent(newCustomer);
            }

            if (customerQueue.Count != 0)
            {
                float lasArival = newCustomer.ArrivalTime;
                lasArival = lasArival * 10;
                newCustomer = new Customer(0, lasArival);
                CheckWaitedCustomers(newCustomer);
            }
        }

        #endregion

        #region System Performance
        /// <summary>
        /// Get Average Wating Time and average waiting time of those who wait
        /// </summary>
        /// <returns></returns>
        /// 
        //1)
        private void SetAvgWaitingTime()
        {
            float sum = 0;
            for (int i = 0; i < this.CompletedCustomers.Count; i++)
                sum += this.CompletedCustomers[i].WaitTime;

            this.AverageDelay = sum / this.CustomerCountStoppingCondition;
        }
        //2)            
        private float GetQueueWaitingProbability()
        {
            int count = 0;
            for (int i = 0; i < this.CompletedCustomers.Count; i++)
                if (this.CompletedCustomers[i].WaitTime != 0)
                    count++;

            return (float)count /(float)this.CustomerCountStoppingCondition;
        }
        //3)
        public float GetProbabilityOfIdleServer()
        {
            clock = CompletedCustomers[CustomerCountStoppingCondition - 1].DepartureTime;
            float totalIdle = 0;
            for (int i = 0; i < this.Servers.Count; i++)
                totalIdle += clock - this.Servers[i].TotalServiceTime;

            return totalIdle / this.clock;
        }
        //4)
        private float GetAVGServiceTime()
        {
            float TotalServiceTime = 0;
            for (int i = 0; i < this.Servers.Count; i++)
                TotalServiceTime += this.Servers[i].TotalServiceTime;

            return TotalServiceTime / this.CustomerCountStoppingCondition;
        }
        //5)
        public float GetAVGTimeBetweenArrivals()
        {
            float InterarrivalsSum = 0;
            for (int i = 0; i < this.CompletedCustomers.Count; i++)
                InterarrivalsSum += this.CompletedCustomers[i].InterarrivalTime;
           
            return InterarrivalsSum / this.CustomerCountStoppingCondition - 1;
        }
        //6)
        public float AvgWaitingTimeOfThoseWhoWait()
        {
            //Total time cus. wait in queue(min)/total num. of cus. that wait = 8/3 = 2.67
            int count = 0;
            float sum = 0;
            for (int i = 0; i < this.CompletedCustomers.Count; i++)
                if (this.CompletedCustomers[i].WaitTime != 0)
                {
                    count++;
                    sum += CompletedCustomers[i].WaitTime;
                }            
            return sum/count;
        }
        //7)
        public float GetAVGTimeCustomerSpendsInTheSystem()
        {
            clock = CompletedCustomers[CustomerCountStoppingCondition - 1].DepartureTime - 1;
            //return clock / this.CustomerCountStoppingCondition;
            float jj =0;
            foreach (Customer v in CompletedCustomers)
                jj += v.ServiceTime + v.WaitTime;
            return jj / this.CustomerCountStoppingCondition;



        }

        public void CalculateSystemPerformance()
        {
            this.SetAvgWaitingTime();
            this.ProbabilityOfCustomerHasToWaitIQueue = this.GetQueueWaitingProbability();
            this.AVGWaitedCustomers = this.GetQueueWaitingProbability();
            this.ProbabilityOfIdleServer = this.GetProbabilityOfIdleServer();
            this.AVGServiceTime = this.GetAVGServiceTime();
            this.AvgWaitingTimeOfThoseWhoWait_ = AvgWaitingTimeOfThoseWhoWait();
            this.AVGTimeBetweenArrivals = this.GetAVGTimeBetweenArrivals();
            this.AVGTimeCustomerSpendsInTheSystem = this.GetAVGTimeCustomerSpendsInTheSystem();
        }
        #endregion
    }
}
