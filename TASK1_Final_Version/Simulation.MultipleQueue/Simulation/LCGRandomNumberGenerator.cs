﻿using System;

namespace MultipleQueue.Simulation
{
    /// <summary>
    /// Factory of random variable
    /// </summary>
    static class LCGRandomNumberGenerator
    {
        #region Local Member
        static Random rand = new Random();
        #endregion 

        #region Get Variate
        /// <summary>
        /// Get variate from 0 to 1
        /// </summary>
        /// <returns>a random number from 0 to 1</returns>
        public static float GetVariate()
        {           
            return (float)rand.NextDouble();
        }
        #endregion
    }
}
