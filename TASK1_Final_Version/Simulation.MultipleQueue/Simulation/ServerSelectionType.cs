﻿namespace MultipleQueue.Simulation
{
    public enum ServerSelectionType
    {
        HighestPriority,
        LowestUtilization,
        Random
    }
}
