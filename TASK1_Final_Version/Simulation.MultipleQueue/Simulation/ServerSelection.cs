﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MultipleQueue.Simulation
{
    public class ServerSelection
    {
        #region Local Member and Constructor
        /// <summary>
        /// HighestPriority,
        ///LowestUtilization,
        ///Random
        /// </summary>
        public ServerSelectionType priorityType;

        /// <summary>
        /// init server selection 
        /// </summary>
        /// <param name="priorityType"></param>
        public ServerSelection(ServerSelectionType priorityType)
        {
            this.priorityType = priorityType;
        }
        #endregion

        public int GetServerByPriority(List<Entities.Server> servers, float clock)
        {
            if (servers.Count == 1)
                 return 0;
            // Else : 


            if (priorityType == ServerSelectionType.Random)
                return (int)(RandomType(servers));
            else if (priorityType == ServerSelectionType.LowestUtilization)  
                return LowestUtilizationType(servers);

            else// aya : lazem te3mel update le mawdo3 al lista lama yeb2o fe kaza wa7da zay ba3d
                return HighestPriorityType(servers);

        }

        float RandomType(List<Entities.Server> servers)
        {
            MultipleQueue.Distributions.UniformDistribution u = new Distributions.UniformDistribution(0, servers.Count);
            return u.GetValue(LCGRandomNumberGenerator.GetVariate());
        }

        int LowestUtilizationType(List<Entities.Server> servers)
        {
            Entities.Server Lowest = servers[0];
            for (int i = 1; i < servers.Count; i++)
            {
                if (Lowest.Utilization > servers[i].Utilization)
                    Lowest = servers[i];
            }
            return servers.IndexOf(Lowest);
        }

        int HighestPriorityType(List<Entities.Server> servers)
        {
            Entities.Server highest = servers[0];
            for (int i = 1; i < servers.Count; i++)
            {
                if (highest.Priority < servers[i].Priority)
                    highest = servers[i];
            }
            return servers.IndexOf(highest);
        }
    }
}
