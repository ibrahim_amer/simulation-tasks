﻿using System;
using System.Collections.Generic;
using MultipleQueue.Simulation;
using System.Data;

namespace MultipleQueue
{
    /// <summary>
    /// Works as a controller in MVC
    /// </summary>
    class SimulationManager
    {
        #region Local member

        public List<Model> SystemModels;
        public int NoOfRuns;
        #endregion

        #region Start Simulation
        //TODO: Implement and document StartSimulation
        public void StartSimulation(ServerSelection serverSelection ,
            Distributions.IDistribution customerInterArrivalDist ,
            List<Entities.Server> servers , 
            int customerCountsStopCondition, int NoOfRuns)
        {
            this.SystemModels = new List<Model>();
            this.InitializeModelsList(serverSelection, customerInterArrivalDist, servers, customerCountsStopCondition, NoOfRuns);
        }

        public void CalculatePerformance()
        {
            for (int i = 0; i < this.NoOfRuns; i++)
            {
                this.SystemModels[i].CalculateSystemPerformance();
            }
        }

        public void InitializeModelsList(ServerSelection serverSelection,
            Distributions.IDistribution customerInterArrivalDist,
            List<Entities.Server> servers,
            int customerCountsStopCondition, int NoOfRuns)
        {
            for (int i = 0; i < NoOfRuns; i++)
            {
                Model model = new Model(serverSelection, customerInterArrivalDist, servers, customerCountsStopCondition);
                model.CalculateSystemPerformance();
                this.SystemModels.Add(model);
            }
        }

      

        public DataTable DisplaySimulationTable(int ModelIndex)
        {
            DataTable simulationTable = new DataTable();
            simulationTable.Columns.Add("CustomerNo");
            simulationTable.Columns.Add("InterArrivalTime");
            simulationTable.Columns.Add("arrival time");
            simulationTable.Columns.Add("server index");
            simulationTable.Columns.Add("time service begin");
            simulationTable.Columns.Add("Service Time");
            simulationTable.Columns.Add("time service end");
            simulationTable.Columns.Add("total delay");

            DataRow row;
            for (int i = 0; i < SystemModels[ModelIndex].CompletedCustomers.Count; i++)
            {
                row = simulationTable.NewRow();
                row["CustomerNo"] = i; //SystemModel.CompletedCustomers[i];
                row["InterArrivalTime"] = SystemModels[ModelIndex].CompletedCustomers[i].InterarrivalTime;
                row["arrival time"] = SystemModels[ModelIndex].CompletedCustomers[i].ArrivalTime;
                row["server index"] = SystemModels[ModelIndex].CompletedCustomers[i].ServerIndex;
                row["time service begin"] = SystemModels[ModelIndex].CompletedCustomers[i].DepartureTime - SystemModels[ModelIndex].CompletedCustomers[i].ServiceTime;
                row["Service Time"] = SystemModels[ModelIndex].CompletedCustomers[i].ServiceTime;
                row["time service end"] = SystemModels[ModelIndex].CompletedCustomers[i].DepartureTime;
                row["total delay"] = SystemModels[ModelIndex].CompletedCustomers[i].WaitTime;

                simulationTable.Rows.Add(row);

            }

            return simulationTable;
        }

        public int[] DrawCharts(int ModelIndex)
        {



            int size = 1; // waitCount array size
            for (int k = 1; k < SystemModels[ModelIndex].CompletedCustomers.Count; k++)
            {

                if (SystemModels[ModelIndex].CompletedCustomers[k].WaitTime > size)
                    size = (int)SystemModels[ModelIndex].CompletedCustomers[k].WaitTime;
            }

            int[] waitCount = new int[size+1];

            for (int j = 0; j < SystemModels[ModelIndex].CompletedCustomers.Count; j++)
            {
                waitCount[(int)SystemModels[ModelIndex].CompletedCustomers[j].WaitTime]++; // index-> wait we el value-> # of customer
            }



            return waitCount;
        }
        #endregion

    }
}
