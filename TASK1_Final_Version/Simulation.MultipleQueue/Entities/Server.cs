﻿using System.Collections.Generic;

namespace MultipleQueue.Entities
{
    /// <summary>
    /// Represents a single server instance.
    /// </summary>
    public class Server
    {
        #region Local Members
        /// <summary>
        /// The service time distribution
        /// </summary>
        public Distributions.IDistribution serviceTimeDistribution;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is busy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is busy; otherwise, <c>false</c>.
        /// </value>
        public bool IsBusy { get; set; }

        /// <summary>
        /// Gets or sets the priority.
        /// </summary>
        /// <value>
        /// The priority.
        /// </value>
        public int? Priority { get; set; }

        /// <summary>
        /// Gets or sets the server status list.
        /// </summary>
        /// <value>
        /// The server status list.
        /// </value>
        public List<ServerBusyTime> ServerStatusList { get; set; }

        /// <summary>
        /// Gets the total service time for all customers served at this server.
        /// </summary>
        public float TotalServiceTime { get; set; }  

        /// <summary>
        /// Gets or sets the utilization of the server.
        /// </summary>
        /// <value>
        /// The utilization.
        /// </value>
        public float Utilization { get; set; }      

        /// <summary>
        /// Gets the average service time.
        /// </summary>
        public float AverageServiceTime
        {
            get { return this.TotalServiceTime / this.TotalCustomerServed; }
        }

        /// <summary>
        /// Gets or sets the total customer served.
        /// </summary>
        /// <value>
        /// The total customer served.
        /// </value>
        public int TotalCustomerServed { get; set; }   
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Server"/> class.
        /// </summary>
        /// <param name="serviceTimeDistribution">The service time distribution.</param>
        /// <param name="priority">The priority.</param>
        public Server(Distributions.IDistribution serviceTimeDistribution, int? priority)
        {
            TotalCustomerServed          = 0;
            this.serviceTimeDistribution = serviceTimeDistribution;
            IsBusy                       = false;
            Priority                     = priority;
            TotalServiceTime             = 0;
            ServerStatusList             = new List<ServerBusyTime>();
        }
        #endregion
        
        //If you need to generate any random input use the same style of this function
        /// <summary>
        /// Gets the next server time distribution.
        /// </summary>
        /// <returns>Next service time</returns>
        public float GetNextServerTimeDistribution()
        {
            float randomVariable = Simulation.LCGRandomNumberGenerator.GetVariate();
            float serviceTime = this.serviceTimeDistribution.GetValue(randomVariable);            
            return serviceTime;
        }
    }
}
