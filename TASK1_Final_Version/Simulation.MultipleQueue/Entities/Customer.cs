﻿namespace MultipleQueue.Entities
{
    public class Customer
    {
        #region Members

        /// <summary>
        /// Gets or sets a value indicating whether this instance is arrived.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is arrived; otherwise, <c>false</c>.
        /// </value>
        public bool IsArrived { get; set; }

        /// <summary>
        /// The arrival time.
        /// </summary>
        private float arrivalTime = -1;

        /// <summary>
        /// Gets or Sets The arrival time.
        /// </summary>
        public float ArrivalTime
        {
            get { return arrivalTime; }            
        }

        /// <summary>
        /// The generated interarrival time.
        /// </summary>
        public float InterarrivalTime { get; set; }

        /// <summary>
        /// The generated service time
        /// </summary>
        public float ServiceTime { get; set; }   

        /// <summary>
        /// The departure time which equals (arrivalTime+WaitTime+ServiceTime)
        /// </summary>
        private float departureTime = -1;

        /// <summary>
        /// The departure time which equals (arrivalTime+WaitTime+ServiceTime)
        /// </summary>
        public float DepartureTime
        {
            get { return departureTime; }
            set { departureTime = value; }
        }  // aya : should be updated 

        /// <summary>
        /// The total time the customer spent in the queue.
        /// </summary>
        public float WaitTime { get; set; } // aya : should be updated 

        /// <summary>
        /// The index of server that served this customer.
        /// </summary>
        public int ServerIndex { get; set; }

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="interarrivalTime">The interarrival time.</param>
        /// <param name="arrivalTime">The arrival time.</param>
        public Customer(float interarrivalTime, float arrivalTime)
        {
            IsArrived = false;
            InterarrivalTime = interarrivalTime;
            this.arrivalTime = arrivalTime;
        }        
    }
}
