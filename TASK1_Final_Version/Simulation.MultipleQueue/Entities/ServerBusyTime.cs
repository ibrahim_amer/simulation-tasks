﻿namespace MultipleQueue.Entities
{
    /// <summary>
    /// Represents a server's serving time (busy time).
    /// </summary>
    public class ServerBusyTime
    {
        #region Local Members
        /// <summary>
        /// Gets or sets from time.
        /// </summary>
        /// <value>
        /// From time.
        /// </value>
        public float FromTime { get; set; }

        /// <summary>
        /// Gets or sets to time.
        /// </summary>
        /// <value>
        /// To time.
        /// </value>
        public float ToTime { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ServerBusyTime"/> class.
        /// </summary>
        /// <param name="fromTime">From time.</param>
        /// <param name="toTime">To time.</param>
        public ServerBusyTime(float fromTime, float toTime)
        {
            FromTime = fromTime;
            ToTime   = toTime;
        }
        #endregion

    }
}
