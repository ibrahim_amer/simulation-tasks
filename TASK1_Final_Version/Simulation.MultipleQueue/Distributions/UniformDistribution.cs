﻿using System;
namespace MultipleQueue.Distributions
{
    public class UniformDistribution : IDistribution
    {
        #region local members
        /// <summary>
        /// uniform bounds a and b
        /// </summary>
        private float a, b;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="UniformDistribution"/> class.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        public UniformDistribution(float a, float b)
        {
            this.a = a;
            this.b = b;
        }
        #endregion

        #region IDistribution Members
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="randomVariable">The random variable.</param>
        /// <returns></returns>
        public double GetValue(float randomVariable)
        {
            float u = randomVariable;
            double[] arr = new double[7];
            double p = u, f, pi = 1;
            //for (int i = 0; i < 7; i++)
            //{
            //    pi = (((7 - 1) / (i + 1)) * (p / 1 - p)) * pi;
            //}
            for (int i = 0; i < 7; i++)
            {
                arr[i] = (fact(7) / (fact(i) * fact((i - 1)))) * Math.Pow((double)p, (double)i) * Math.Pow((double)(1 - p), (double)i);

                try
                {
                    arr[i] = arr[i - 1] + arr[i];
                }
                catch (Exception)
                {
                    arr[i] = arr[i];
                    throw;
                }
            }

           
            return pi;
        }
        float fact(float n)
        {
            if (n == 0 || n == 1)
                return 1;
            return n * fact(n - 1);
        }
        #endregion
    }
}
