﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BearingSystem
{
    public class UniformDistribution : IDistribution
    {
         #region local members
        /// <summary>
        /// uniform bounds a and b
        /// </summary>
        private float a, b;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="UniformDistribution"/> class.
        /// </summary>
        /// <param name="a">A.</param>
        /// <param name="b">The b.</param>
        public UniformDistribution(float a, float b)
        {
            this.a = a;
            this.b = b;
        }
        #endregion

        #region IDistribution Members
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="randomVariable">The random variable.</param>
        /// <returns></returns>
        public float GetValue(float randomVariable)
        {
            return this.a + randomVariable * (this.b - this.a);
        }
        #endregion
    }
    
}
