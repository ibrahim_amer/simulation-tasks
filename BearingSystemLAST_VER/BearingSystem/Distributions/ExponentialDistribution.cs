﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BearingSystem
{
    public class ExponentialDistribution : IDistribution
    {
         #region Local members
        /// <summary>
       /// mean or lambeda
       /// </summary>
        private double mean;
        #endregion 

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="ExponentialDistribution"/> class.
        /// </summary>
        /// <param name="Mean">The mean.</param>
        public ExponentialDistribution(float mean)
        {
            this.mean = mean;
        }
        #endregion

        #region IDistribution Members
        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="randomVariable">The random variable.</param>
        /// <returns>random vairable belongs to exponential distribution</returns>
        public float GetValue(float randomVariable)
        {
            if (randomVariable != 0)
            {
                return (float)(-this.mean * Math.Log(randomVariable, Math.Exp(1)));
            }
            return 0;
        }

        #endregion
    }
    
}
