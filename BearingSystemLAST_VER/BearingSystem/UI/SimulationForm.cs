﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace BearingSystem
{
    public partial class Form1 : Form
    {
        int NumOfHours ;
        int NumOfTrails;
        int bearingCost;
        int DowntimeCost;
        int RepairpersonCost;
       
        int CurrentRepairTimePerBearing;
        int ProposedRepairTimePerBearing;

        DataTable[] Table = new DataTable[3];
        

        //Delay Probability For Delay Time
        List<float> DelayTimeValuesList;
        List<float> DelayTimeProbabilitiesList;
        //Probability For LifeTime
        List<float> LifeTimeValuesList;
        List<float> LifeTimeProbabilitiesList;

        //Distribution Of Delay
        IDistribution DelayTimeDist;
        DistributionTypes DelayTimeDistEnum;
        //Distribution Of LifeTime
        IDistribution LifeTimeDist;
        DistributionTypes LifeTimeDistEnum;

        public float AvgTotalCostOfBearings;
        public float AvgtotalCostOfDelayTime;
        public float AvgtotalCostOfDowenTimeDuringRepair;
        public float AvgtotalCostOfRepairPerson;

        SimulationManager Manager;
        public Form1()
        {
            InitializeComponent();
            Manager = new SimulationManager();
            this.DelayTimeValuesList = new List<float>();
            this.DelayTimeProbabilitiesList = new List<float>();
            this.LifeTimeValuesList = new List<float>();
            this.LifeTimeProbabilitiesList = new List<float>();
            Table[0] = new DataTable();
            Table[1] = new DataTable();
            Table[2] = new DataTable();
           
            //this.DelayTime_dataGridView.Rows.Add();
            //this.DelayTime_dataGridView.Rows.Add();
            //this.LifeTime_dataGridView.Rows.Add();
            //this.LifeTime_dataGridView.Rows.Add();
        }

        public void SetDelayTimeData()
        {
            DataGridViewRowCollection Rows = this.DelayTime_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.DelayTimeValuesList.Add(float.Parse(cells[0].Value.ToString()));
                this.DelayTimeProbabilitiesList.Add(float.Parse(cells[1].Value.ToString()));
            }
        }

        public void SetLifeTimeData()
        {
            DataGridViewRowCollection Rows = this.LifeTime_dataGridView.Rows;
            DataGridViewCellCollection cells;
            for (int i = 0; i < Rows.Count - 1; i++)
            {
                cells = Rows[i].Cells;
                this.LifeTimeValuesList.Add(float.Parse(cells[0].Value.ToString()));
                this.LifeTimeProbabilitiesList.Add(float.Parse(cells[1].Value.ToString()));
            }
        }

        public void InitializeMetaData()
        {
            this.NumOfTrails = int.Parse(this.textBox1.Text);
            this.bearingCost = int.Parse(this.textBox2.Text);
            this.DowntimeCost = int.Parse(this.textBox3.Text);
            this.RepairpersonCost = int.Parse(this.textBox4.Text);
            this.NumOfHours = int.Parse(this.textBox5.Text);
            this.CurrentRepairTimePerBearing = int.Parse(this.textBox6.Text);
            this.ProposedRepairTimePerBearing = int.Parse(this.textBox7.Text);
            CurrentBearing1.ColumnCount = 0;
            Currentbearing2.ColumnCount = 0;
            Currentbearing3.ColumnCount = 0;
            ProposedTable.ColumnCount = 0;
            for (int j = 0; j < int.Parse(this.textBox1.Text); j++)
            {
                this.comboBox1.Items.Add("Trails Number"+j.ToString() );
              //  this.comboBox2.Items.Add("Trails Number" + j.ToString());
            }
        }

        public void InitializeDistributionData()
        {
            SetDelayTimeDist();
            SetLifeTimeDist();
        }

        public void SetDelayTimeDist()
        {
            if (this.DelayTimeDistEnum == DistributionTypes.Discrete)
            {
                this.SetDelayTimeData();
                this.DelayTimeDist = new DiscreteDistribution(DelayTimeValuesList, this.DelayTimeProbabilitiesList);
            }
            else if (this.DelayTimeDistEnum == DistributionTypes.Exponential)
                this.DelayTimeDist = new ExponentialDistribution(float.Parse(this.DelayDist_Mean_textBox.Text));
            else if (this.DelayTimeDistEnum == DistributionTypes.Uniform)
                this.DelayTimeDist = new UniformDistribution(float.Parse(this.Delay_Dist_UniformA_txtbox.Text),
               float.Parse(this.DelayDist_UniformB_txtbox.Text));
        }

        public void SetLifeTimeDist()
        {
            if (this.LifeTimeDistEnum == DistributionTypes.Discrete)
            {
                this.SetLifeTimeData();
                this.LifeTimeDist = new DiscreteDistribution(LifeTimeValuesList, this.LifeTimeProbabilitiesList);
            }
            else if (this.LifeTimeDistEnum == DistributionTypes.Exponential)
                this.LifeTimeDist = new ExponentialDistribution(float.Parse(this.LifeTimeDist_Mean_txtbox.Text));
            else if (this.LifeTimeDistEnum == DistributionTypes.Uniform)
                this.LifeTimeDist = new UniformDistribution(float.Parse(this.LifeTimeDist_UniformA_txtbox.Text),
               float.Parse(this.LifeTimeDist_UniformB_txtbox.Text));
        }

        private void Currentbearing2_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {
                this.InitializeMetaData();
                this.InitializeDistributionData();
                this.Manager.StartSimulation(this.NumOfHours, this.DelayTimeDist, this.LifeTimeDist , this.NumOfTrails, this.bearingCost ,
                this.DowntimeCost, this.RepairpersonCost, this.CurrentRepairTimePerBearing, this.ProposedRepairTimePerBearing);
                
        }

        public void ClearAll()
        {
            this.textBox1.Clear();
            this.textBox2.Clear();
            this.textBox3.Clear();
            this.textBox4.Clear();
            this.textBox18.Clear();
            this.textBox19.Clear();
            this.textBox6.Text = ""; 
            this.textBox7.Text = "";
            this.textBox20.Text = "";
            this.textBox21.Text = "";
            this.textBox22.Text = "";
            this.textBox23.Text = "";
            this.textBox24.Text = "";
            this.textBox25.Text = "";
            this.DelayTimeValuesList = new List<float>();
            this.DelayTimeProbabilitiesList = new List<float>();
            this.LifeTimeValuesList = new List<float>();
            this.LifeTimeProbabilitiesList = new List<float>();
            this.textBox8.Text = "";
            this.textBox9.Text = "";
            this.textBox10.Text = "";
            this.textBox11.Text = "";
            this.textBox12.Text = "";
            this.comboBox1.Items.Clear();
            this.textBox13.Text = "";
            this.textBox14.Text = "";
            this.textBox15.Text = "";
            this.textBox16.Text = "";
            this.textBox17.Text = "";
            
            this.CurrentBearing1.DataSource = "";
            this.Currentbearing2.DataSource = "";
            this.Currentbearing3.DataSource = "";
            this.ProposedTable.DataSource = "";
            this.DelayTime_dataGridView.Rows.Clear();
            this.LifeTime_dataGridView.Rows.Clear();
        }

        private void NewsDayTypeDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.DelayTime_dataGridView.Visible = true;
            this.DelayTimeDistEnum = DistributionTypes.Discrete;
        }

        private void DelayDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.Delay_Dist_UniformA_txtbox.Visible = !this.Delay_Dist_UniformA_txtbox.Visible;
            this.DelayDist_UniformB_txtbox.Visible = !this.DelayDist_UniformB_txtbox.Visible;
            this.label7.Visible = !this.label7.Visible;
            this.label8.Visible = !this.label8.Visible;
            this.DelayTimeDistEnum = DistributionTypes.Uniform;
        }

        private void DelayDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label9.Visible = !this.label9.Visible;
            this.DelayDist_Mean_textBox.Visible = !this.DelayDist_Mean_textBox.Visible;
            this.DelayTimeDist = new ExponentialDistribution(float.Parse(this.DelayDist_Mean_textBox.Text));
            this.DelayTimeDistEnum = DistributionTypes.Exponential;
        }

        private void LifetimeDist_Discrete_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.LifeTime_dataGridView.Visible = true;
            this.LifeTimeDistEnum = DistributionTypes.Discrete;
        }

        private void LifeTimeDist_Uniform_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.LifeTimeDist_UniformA_txtbox.Visible = !this.LifeTimeDist_UniformA_txtbox.Visible;
            this.LifeTimeDist_UniformB_txtbox.Visible = !this.LifeTimeDist_UniformB_txtbox.Visible;
            this.label11.Visible = !this.label11.Visible;
            this.label12.Visible = !this.label12.Visible;
            this.LifeTimeDistEnum = DistributionTypes.Uniform;
        }

        private void LifeTimeDist_Exponential_rbtn_CheckedChanged(object sender, EventArgs e)
        {
            this.label10.Visible = !this.label10.Visible;
            this.LifeTimeDist_Mean_txtbox.Visible = !this.LifeTimeDist_Mean_txtbox.Visible;
            this.LifeTimeDist = new ExponentialDistribution(float.Parse(this.LifeTimeDist_Mean_txtbox.Text));
            this.LifeTimeDistEnum = DistributionTypes.Exponential;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            int i = this.comboBox1.SelectedIndex;
            this.Table = this.Manager.CalculateStatistics(i);
            this.CurrentBearing1.DataSource = this.Table[0];
            this.Currentbearing2.DataSource = this.Table[1];
            this.Currentbearing3.DataSource = this.Table[2];
            this.ProposedTable.DataSource = this.Table[3];
            this.Manager.CalculatePerformance();

            // performance display
            this.textBox8.Text = this.Manager.Currnt_SystemModels[i].TotalCostOfBearings.ToString();
            this.textBox9.Text = this.Manager.Currnt_SystemModels[i].totalCostOfDelayTime.ToString();
            this.textBox10.Text = this.Manager.Currnt_SystemModels[i].totalCostOfRepairPerson.ToString();
            this.textBox11.Text = this.Manager.Currnt_SystemModels[i].totalCostOfDowenTimeDuringRepair.ToString();
            this.textBox12.Text = this.Manager.Currnt_SystemModels[i].Total_cost.ToString();

            this.textBox13.Text = this.Manager.Propsed_SystemModels[i].TotalCostOfBearings.ToString();
            this.textBox14.Text = this.Manager.Propsed_SystemModels[i].totalCostOfDelayTime.ToString();
            this.textBox15.Text = this.Manager.Propsed_SystemModels[i].totalCostOfRepairPerson.ToString();
            this.textBox16.Text = this.Manager.Propsed_SystemModels[i].totalCostOfDowenTimeDuringRepair.ToString();
            this.textBox17.Text = this.Manager.Propsed_SystemModels[i].Total_cost.ToString();

       
            // avg values 
            float s1=0, s2=0, s3=0, s4=0, s5=0, s6=0, s7=0, s8=0;

            for (int j = 0; j < NumOfTrails; j++)
            {
                s1 += this.Manager.Currnt_SystemModels[j].TotalCostOfBearings;
                s2 += this.Manager.Currnt_SystemModels[j].totalCostOfDelayTime;
                s3 += this.Manager.Currnt_SystemModels[j].totalCostOfRepairPerson;
                s4 += this.Manager.Currnt_SystemModels[j].totalCostOfDowenTimeDuringRepair;
                s5 += this.Manager.Propsed_SystemModels[j].TotalCostOfBearings;
                s6 += this.Manager.Propsed_SystemModels[j].totalCostOfDelayTime;
                s7 += this.Manager.Propsed_SystemModels[j].totalCostOfRepairPerson;
                s8 += this.Manager.Propsed_SystemModels[j].totalCostOfDowenTimeDuringRepair;
            }
            this.textBox18.Text = (s1 / NumOfTrails).ToString();
            this.textBox20.Text = (s2 / NumOfTrails).ToString();
            this.textBox21.Text = (s3 / NumOfTrails).ToString();
            this.textBox22.Text = (s4 / NumOfTrails).ToString();
            this.textBox19.Text = (s5 / NumOfTrails).ToString();
            this.textBox23.Text = (s6 / NumOfTrails).ToString();
            this.textBox24.Text = (s7 / NumOfTrails).ToString();
            this.textBox25.Text = (s8 / NumOfTrails).ToString();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.ClearAll();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void Draw_Chart_Current_Click(object sender, EventArgs e)
        {
            //int m = this.comboBox1.SelectedIndex;
            int[] y;
            int [] x;
            y = Manager.DrawCurrentPolicyChart(out x);
          
            // Draw

            GraphPane myPane = zedGraphControl1.GraphPane;
            myPane.Title.Text = "Frequency of Cost Range For Current Policy";
            myPane.XAxis.Title.Text = "Cost Range";
            myPane.YAxis.Title.Text = "Number Of Trails Having Total Cost In this Range";

            // Create Bar
            PointPairList list1 = new PointPairList();

            for (int i = 0; i < y.Length; i++)
            {
                list1.Add( x[i], y[i]);
            }

            BarItem bar1 = myPane.AddBar("Bar 1", list1, Color.Red);
            bar1.Bar.Fill = new Fill(Color.Red, Color.White, Color.Red, 90);


            // Set BarBase to the XAxis for horizontal bars
            myPane.BarSettings.Base = BarBase.X;


            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White,
               Color.FromArgb(255, 255, 166), 45.0F);

            zedGraphControl1.AxisChange();

            // Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, true, "f0"); 
            zedGraphControl1.AxisChange();
            zedGraphControl1.Invalidate();
            zedGraphControl1.Refresh();
        }

        private void Clear_Chart_Current_Click(object sender, EventArgs e)
        {

            zedGraphControl1.GraphPane.CurveList.Clear();
            zedGraphControl1.GraphPane.GraphObjList.Clear();
            zedGraphControl1.Refresh();
        }

        private void Draw_Chart_Proposed_Click(object sender, EventArgs e)
        {
            int[] y;
            int[] x;
            y = Manager.DrawProposedPolicyChart(out x);

            // Draw

            GraphPane myPane = zedGraphControl2.GraphPane;
            myPane.Title.Text = "Frequency of Cost Range For Proposed Policy";
            myPane.XAxis.Title.Text = "Cost Range";
            myPane.YAxis.Title.Text = "Number Of Trails Having Total Cost In this Range";

            // Create Bar
            PointPairList list1 = new PointPairList();
            for (int i = 0; i < y.Length; i++)
            {
                list1.Add(x[i], y[i]);
            }


            BarItem bar1 = myPane.AddBar("Bar 1", list1, Color.Red);
            bar1.Bar.Fill = new Fill(Color.Red, Color.White, Color.Red, 90);


            // Set BarBase to the XAxis for horizontal bars
            myPane.BarSettings.Base = BarBase.X;


            // Fill the axis background with a color gradient
            myPane.Chart.Fill = new Fill(Color.White,
               Color.FromArgb(255, 255, 166), 45.0F);

            zedGraphControl2.AxisChange();

            // Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, true, "f0");
            zedGraphControl2.AxisChange();
            zedGraphControl2.Invalidate();
            zedGraphControl2.Refresh();
        }

        private void Clear_Chart_Proposed_Click(object sender, EventArgs e)
        {

            zedGraphControl2.GraphPane.CurveList.Clear();
            zedGraphControl2.GraphPane.GraphObjList.Clear();
            zedGraphControl2.Refresh();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

       
    }
}
