﻿namespace BearingSystem
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.LifeTimeDist_Mean_txtbox = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.LifeTimeDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.LifeTimeDist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.LifeTimeDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.LifeTimeDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.LifetimeDist_Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.LifeTime_dataGridView = new System.Windows.Forms.DataGridView();
            this.Demand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Good = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.DelayDist_Mean_textBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DelayDist_UniformB_txtbox = new System.Windows.Forms.TextBox();
            this.Delay_Dist_UniformA_txtbox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DelayDist_Uniform_rbtn = new System.Windows.Forms.RadioButton();
            this.DelayDist_Exponential_rbtn = new System.Windows.Forms.RadioButton();
            this.DelayDist_Discrete_rbtn = new System.Windows.Forms.RadioButton();
            this.DelayTime_dataGridView = new System.Windows.Forms.DataGridView();
            this.TypeOfNewsday = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Probability = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.Currentbearing3 = new System.Windows.Forms.DataGridView();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label18 = new System.Windows.Forms.Label();
            this.Currentbearing2 = new System.Windows.Forms.DataGridView();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentBearing1 = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.ProposedTable = new System.Windows.Forms.DataGridView();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.Draw_Chart_Current = new System.Windows.Forms.Button();
            this.Clear_Chart_Current = new System.Windows.Forms.Button();
            this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.Clear_Chart_Proposed = new System.Windows.Forms.Button();
            this.Draw_Chart_Proposed = new System.Windows.Forms.Button();
            this.zedGraphControl2 = new ZedGraph.ZedGraphControl();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LifeTime_dataGridView)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DelayTime_dataGridView)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Currentbearing3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Currentbearing2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentBearing1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedTable)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(0, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1216, 521);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1208, 495);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Inputs";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(749, 416);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 42);
            this.button1.TabIndex = 15;
            this.button1.Text = "Simulate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.textBox7);
            this.groupBox1.Controls.Add(this.textBox6);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(596, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 404);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "MetaData";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "# Of Hours";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(225, 75);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(100, 20);
            this.textBox5.TabIndex = 15;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(225, 303);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(100, 20);
            this.textBox7.TabIndex = 14;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(225, 240);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(100, 20);
            this.textBox6.TabIndex = 13;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(225, 196);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 20);
            this.textBox4.TabIndex = 11;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(225, 151);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 20);
            this.textBox3.TabIndex = 10;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(225, 112);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 9;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(225, 40);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 8;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(22, 303);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(163, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "Prposed Repair Time Per Bearing";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "currnt Repair Time Per Bearing";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 199);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Repairperson Cost";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Downtime Cost";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bearing Cost";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "# Of Trails";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.LifeTime_dataGridView);
            this.groupBox4.Location = new System.Drawing.Point(306, -4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(285, 488);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Bearing Life Distribution";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(71, 411);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(89, 42);
            this.button5.TabIndex = 16;
            this.button5.Text = "Clear All";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.LifeTimeDist_Mean_txtbox);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.LifeTimeDist_UniformB_txtbox);
            this.groupBox5.Controls.Add(this.LifeTimeDist_UniformA_txtbox);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.LifeTimeDist_Uniform_rbtn);
            this.groupBox5.Controls.Add(this.LifeTimeDist_Exponential_rbtn);
            this.groupBox5.Controls.Add(this.LifetimeDist_Discrete_rbtn);
            this.groupBox5.Location = new System.Drawing.Point(4, 254);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox5.Size = new System.Drawing.Size(270, 128);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Distribution Of LifeTime";
            // 
            // LifeTimeDist_Mean_txtbox
            // 
            this.LifeTimeDist_Mean_txtbox.Location = new System.Drawing.Point(200, 68);
            this.LifeTimeDist_Mean_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTimeDist_Mean_txtbox.Name = "LifeTimeDist_Mean_txtbox";
            this.LifeTimeDist_Mean_txtbox.Size = new System.Drawing.Size(42, 20);
            this.LifeTimeDist_Mean_txtbox.TabIndex = 2;
            this.LifeTimeDist_Mean_txtbox.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(164, 71);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Mean";
            this.label10.Visible = false;
            // 
            // LifeTimeDist_UniformB_txtbox
            // 
            this.LifeTimeDist_UniformB_txtbox.Location = new System.Drawing.Point(109, 98);
            this.LifeTimeDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTimeDist_UniformB_txtbox.Name = "LifeTimeDist_UniformB_txtbox";
            this.LifeTimeDist_UniformB_txtbox.Size = new System.Drawing.Size(47, 20);
            this.LifeTimeDist_UniformB_txtbox.TabIndex = 2;
            this.LifeTimeDist_UniformB_txtbox.Visible = false;
            // 
            // LifeTimeDist_UniformA_txtbox
            // 
            this.LifeTimeDist_UniformA_txtbox.Location = new System.Drawing.Point(109, 67);
            this.LifeTimeDist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTimeDist_UniformA_txtbox.Name = "LifeTimeDist_UniformA_txtbox";
            this.LifeTimeDist_UniformA_txtbox.Size = new System.Drawing.Size(47, 20);
            this.LifeTimeDist_UniformA_txtbox.TabIndex = 3;
            this.LifeTimeDist_UniformA_txtbox.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(92, 98);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "b";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(92, 67);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "a";
            this.label12.Visible = false;
            // 
            // LifeTimeDist_Uniform_rbtn
            // 
            this.LifeTimeDist_Uniform_rbtn.AutoSize = true;
            this.LifeTimeDist_Uniform_rbtn.Location = new System.Drawing.Point(94, 32);
            this.LifeTimeDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTimeDist_Uniform_rbtn.Name = "LifeTimeDist_Uniform_rbtn";
            this.LifeTimeDist_Uniform_rbtn.Size = new System.Drawing.Size(62, 17);
            this.LifeTimeDist_Uniform_rbtn.TabIndex = 2;
            this.LifeTimeDist_Uniform_rbtn.TabStop = true;
            this.LifeTimeDist_Uniform_rbtn.Text = "Uniform";
            this.LifeTimeDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.LifeTimeDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.LifeTimeDist_Uniform_rbtn_CheckedChanged);
            // 
            // LifeTimeDist_Exponential_rbtn
            // 
            this.LifeTimeDist_Exponential_rbtn.AutoSize = true;
            this.LifeTimeDist_Exponential_rbtn.Location = new System.Drawing.Point(166, 32);
            this.LifeTimeDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTimeDist_Exponential_rbtn.Name = "LifeTimeDist_Exponential_rbtn";
            this.LifeTimeDist_Exponential_rbtn.Size = new System.Drawing.Size(81, 17);
            this.LifeTimeDist_Exponential_rbtn.TabIndex = 1;
            this.LifeTimeDist_Exponential_rbtn.TabStop = true;
            this.LifeTimeDist_Exponential_rbtn.Text = "Exponential";
            this.LifeTimeDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.LifeTimeDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.LifeTimeDist_Exponential_rbtn_CheckedChanged);
            // 
            // LifetimeDist_Discrete_rbtn
            // 
            this.LifetimeDist_Discrete_rbtn.AutoSize = true;
            this.LifetimeDist_Discrete_rbtn.Location = new System.Drawing.Point(16, 32);
            this.LifetimeDist_Discrete_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.LifetimeDist_Discrete_rbtn.Name = "LifetimeDist_Discrete_rbtn";
            this.LifetimeDist_Discrete_rbtn.Size = new System.Drawing.Size(64, 17);
            this.LifetimeDist_Discrete_rbtn.TabIndex = 0;
            this.LifetimeDist_Discrete_rbtn.TabStop = true;
            this.LifetimeDist_Discrete_rbtn.Text = "Discrete";
            this.LifetimeDist_Discrete_rbtn.UseVisualStyleBackColor = true;
            this.LifetimeDist_Discrete_rbtn.CheckedChanged += new System.EventHandler(this.LifetimeDist_Discrete_rbtn_CheckedChanged);
            // 
            // LifeTime_dataGridView
            // 
            this.LifeTime_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LifeTime_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Demand,
            this.Good});
            this.LifeTime_dataGridView.Location = new System.Drawing.Point(4, 17);
            this.LifeTime_dataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.LifeTime_dataGridView.Name = "LifeTime_dataGridView";
            this.LifeTime_dataGridView.RowTemplate.Height = 24;
            this.LifeTime_dataGridView.Size = new System.Drawing.Size(242, 131);
            this.LifeTime_dataGridView.TabIndex = 0;
            this.LifeTime_dataGridView.Visible = false;
            // 
            // Demand
            // 
            this.Demand.HeaderText = "Bearing Life";
            this.Demand.Name = "Demand";
            // 
            // Good
            // 
            this.Good.HeaderText = "Probability";
            this.Good.Name = "Good";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.DelayTime_dataGridView);
            this.groupBox2.Location = new System.Drawing.Point(5, 5);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(294, 385);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delay Time Distribution";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.DelayDist_Mean_textBox);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.DelayDist_UniformB_txtbox);
            this.groupBox3.Controls.Add(this.Delay_Dist_UniformA_txtbox);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.DelayDist_Uniform_rbtn);
            this.groupBox3.Controls.Add(this.DelayDist_Exponential_rbtn);
            this.groupBox3.Controls.Add(this.DelayDist_Discrete_rbtn);
            this.groupBox3.Location = new System.Drawing.Point(4, 254);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox3.Size = new System.Drawing.Size(270, 128);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Distribution Of Delay Time";
            // 
            // DelayDist_Mean_textBox
            // 
            this.DelayDist_Mean_textBox.Location = new System.Drawing.Point(200, 68);
            this.DelayDist_Mean_textBox.Margin = new System.Windows.Forms.Padding(2);
            this.DelayDist_Mean_textBox.Name = "DelayDist_Mean_textBox";
            this.DelayDist_Mean_textBox.Size = new System.Drawing.Size(42, 20);
            this.DelayDist_Mean_textBox.TabIndex = 2;
            this.DelayDist_Mean_textBox.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(164, 71);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Mean";
            this.label9.Visible = false;
            // 
            // DelayDist_UniformB_txtbox
            // 
            this.DelayDist_UniformB_txtbox.Location = new System.Drawing.Point(109, 98);
            this.DelayDist_UniformB_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.DelayDist_UniformB_txtbox.Name = "DelayDist_UniformB_txtbox";
            this.DelayDist_UniformB_txtbox.Size = new System.Drawing.Size(47, 20);
            this.DelayDist_UniformB_txtbox.TabIndex = 2;
            this.DelayDist_UniformB_txtbox.Visible = false;
            // 
            // Delay_Dist_UniformA_txtbox
            // 
            this.Delay_Dist_UniformA_txtbox.Location = new System.Drawing.Point(109, 67);
            this.Delay_Dist_UniformA_txtbox.Margin = new System.Windows.Forms.Padding(2);
            this.Delay_Dist_UniformA_txtbox.Name = "Delay_Dist_UniformA_txtbox";
            this.Delay_Dist_UniformA_txtbox.Size = new System.Drawing.Size(47, 20);
            this.Delay_Dist_UniformA_txtbox.TabIndex = 3;
            this.Delay_Dist_UniformA_txtbox.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(92, 98);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "b";
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(92, 67);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "a";
            this.label8.Visible = false;
            // 
            // DelayDist_Uniform_rbtn
            // 
            this.DelayDist_Uniform_rbtn.AutoSize = true;
            this.DelayDist_Uniform_rbtn.Location = new System.Drawing.Point(94, 32);
            this.DelayDist_Uniform_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.DelayDist_Uniform_rbtn.Name = "DelayDist_Uniform_rbtn";
            this.DelayDist_Uniform_rbtn.Size = new System.Drawing.Size(62, 17);
            this.DelayDist_Uniform_rbtn.TabIndex = 2;
            this.DelayDist_Uniform_rbtn.TabStop = true;
            this.DelayDist_Uniform_rbtn.Text = "Uniform";
            this.DelayDist_Uniform_rbtn.UseVisualStyleBackColor = true;
            this.DelayDist_Uniform_rbtn.CheckedChanged += new System.EventHandler(this.DelayDist_Uniform_rbtn_CheckedChanged);
            // 
            // DelayDist_Exponential_rbtn
            // 
            this.DelayDist_Exponential_rbtn.AutoSize = true;
            this.DelayDist_Exponential_rbtn.Location = new System.Drawing.Point(166, 32);
            this.DelayDist_Exponential_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.DelayDist_Exponential_rbtn.Name = "DelayDist_Exponential_rbtn";
            this.DelayDist_Exponential_rbtn.Size = new System.Drawing.Size(81, 17);
            this.DelayDist_Exponential_rbtn.TabIndex = 1;
            this.DelayDist_Exponential_rbtn.TabStop = true;
            this.DelayDist_Exponential_rbtn.Text = "Exponential";
            this.DelayDist_Exponential_rbtn.UseVisualStyleBackColor = true;
            this.DelayDist_Exponential_rbtn.CheckedChanged += new System.EventHandler(this.DelayDist_Exponential_rbtn_CheckedChanged);
            // 
            // DelayDist_Discrete_rbtn
            // 
            this.DelayDist_Discrete_rbtn.AutoSize = true;
            this.DelayDist_Discrete_rbtn.Location = new System.Drawing.Point(16, 32);
            this.DelayDist_Discrete_rbtn.Margin = new System.Windows.Forms.Padding(2);
            this.DelayDist_Discrete_rbtn.Name = "DelayDist_Discrete_rbtn";
            this.DelayDist_Discrete_rbtn.Size = new System.Drawing.Size(64, 17);
            this.DelayDist_Discrete_rbtn.TabIndex = 0;
            this.DelayDist_Discrete_rbtn.TabStop = true;
            this.DelayDist_Discrete_rbtn.Text = "Discrete";
            this.DelayDist_Discrete_rbtn.UseVisualStyleBackColor = true;
            this.DelayDist_Discrete_rbtn.CheckedChanged += new System.EventHandler(this.NewsDayTypeDist_Discrete_rbtn_CheckedChanged);
            // 
            // DelayTime_dataGridView
            // 
            this.DelayTime_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DelayTime_dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeOfNewsday,
            this.Probability});
            this.DelayTime_dataGridView.Location = new System.Drawing.Point(4, 17);
            this.DelayTime_dataGridView.Margin = new System.Windows.Forms.Padding(2);
            this.DelayTime_dataGridView.Name = "DelayTime_dataGridView";
            this.DelayTime_dataGridView.RowTemplate.Height = 24;
            this.DelayTime_dataGridView.Size = new System.Drawing.Size(242, 136);
            this.DelayTime_dataGridView.TabIndex = 0;
            this.DelayTime_dataGridView.Visible = false;
            // 
            // TypeOfNewsday
            // 
            this.TypeOfNewsday.HeaderText = "Delay Time";
            this.TypeOfNewsday.Name = "TypeOfNewsday";
            // 
            // Probability
            // 
            this.Probability.HeaderText = "Probability";
            this.Probability.Name = "Probability";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.comboBox1);
            this.tabPage2.Controls.Add(this.Currentbearing3);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.Currentbearing2);
            this.tabPage2.Controls.Add(this.CurrentBearing1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1208, 495);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Current Policy";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBox22);
            this.groupBox8.Controls.Add(this.textBox21);
            this.groupBox8.Controls.Add(this.textBox20);
            this.groupBox8.Controls.Add(this.label29);
            this.groupBox8.Controls.Add(this.label28);
            this.groupBox8.Controls.Add(this.label27);
            this.groupBox8.Controls.Add(this.textBox18);
            this.groupBox8.Controls.Add(this.label25);
            this.groupBox8.Location = new System.Drawing.Point(797, 235);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(295, 186);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Average Values";
            // 
            // textBox22
            // 
            this.textBox22.Location = new System.Drawing.Point(156, 120);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(100, 20);
            this.textBox22.TabIndex = 25;
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(156, 85);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(100, 20);
            this.textBox21.TabIndex = 24;
            // 
            // textBox20
            // 
            this.textBox20.Location = new System.Drawing.Point(156, 59);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(100, 20);
            this.textBox20.TabIndex = 23;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 120);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(150, 13);
            this.label29.TabIndex = 22;
            this.label29.Text = "Avg. DownTime During Repair";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 85);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(118, 13);
            this.label28.TabIndex = 21;
            this.label28.Text = "Avg Cost RepairPerson";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(6, 57);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(118, 13);
            this.label27.TabIndex = 20;
            this.label27.Text = "Average Cost Of Delay";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(156, 31);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(100, 20);
            this.textBox18.TabIndex = 19;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 31);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(131, 13);
            this.label25.TabIndex = 18;
            this.label25.Text = "Average Cost Per Bearing";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBox12);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.textBox11);
            this.groupBox6.Controls.Add(this.textBox10);
            this.groupBox6.Controls.Add(this.textBox9);
            this.groupBox6.Controls.Add(this.textBox8);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Location = new System.Drawing.Point(797, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(295, 214);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Current Performance";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(156, 162);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(100, 20);
            this.textBox12.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(23, 162);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 13);
            this.label19.TabIndex = 10;
            this.label19.Text = "Total Cost";
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(156, 130);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 7;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(156, 98);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 6;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(156, 65);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(100, 20);
            this.textBox9.TabIndex = 5;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(156, 31);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(100, 20);
            this.textBox8.TabIndex = 4;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 130);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "DownTime During Repair";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 98);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 13);
            this.label16.TabIndex = 2;
            this.label16.Text = "Cost Of RepairPerson";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(99, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "Cost Of Delay Time";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 34);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Cost Of Bearing";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(921, 437);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // Currentbearing3
            // 
            this.Currentbearing3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Currentbearing3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18});
            this.Currentbearing3.Location = new System.Drawing.Point(530, 3);
            this.Currentbearing3.Name = "Currentbearing3";
            this.Currentbearing3.Size = new System.Drawing.Size(261, 489);
            this.Currentbearing3.TabIndex = 2;
            // 
            // Column13
            // 
            this.Column13.HeaderText = "NumOfChange";
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            this.Column14.HeaderText = "RandomLifeTime";
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            this.Column15.HeaderText = "LifeTime";
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            this.Column16.HeaderText = "AccumlatedLife";
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            this.Column17.HeaderText = "RandomDelay";
            this.Column17.Name = "Column17";
            // 
            // Column18
            // 
            this.Column18.HeaderText = "Delay";
            this.Column18.Name = "Column18";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(816, 445);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(87, 13);
            this.label18.TabIndex = 9;
            this.label18.Text = "Number Of Trails";
            // 
            // Currentbearing2
            // 
            this.Currentbearing2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Currentbearing2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11,
            this.Column12});
            this.Currentbearing2.Location = new System.Drawing.Point(264, 3);
            this.Currentbearing2.Name = "Currentbearing2";
            this.Currentbearing2.Size = new System.Drawing.Size(260, 489);
            this.Currentbearing2.TabIndex = 1;
            this.Currentbearing2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Currentbearing2_CellContentClick);
            // 
            // Column7
            // 
            this.Column7.HeaderText = "NumOfChange";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "RandomLifeTiem";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.HeaderText = "LifeTime";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.HeaderText = "AccumlatedLifeTime";
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            this.Column11.HeaderText = "RandomDelay";
            this.Column11.Name = "Column11";
            // 
            // Column12
            // 
            this.Column12.HeaderText = "Delay";
            this.Column12.Name = "Column12";
            // 
            // CurrentBearing1
            // 
            this.CurrentBearing1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CurrentBearing1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.CurrentBearing1.Location = new System.Drawing.Point(0, 3);
            this.CurrentBearing1.Name = "CurrentBearing1";
            this.CurrentBearing1.Size = new System.Drawing.Size(258, 489);
            this.CurrentBearing1.TabIndex = 0;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "NumOfChange";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "RandomLifeTime";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "LifeTime";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "AccumlatedLifeTime";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "RandomDelay";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "DelayTime";
            this.Column6.Name = "Column6";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.ProposedTable);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1208, 495);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Proposed Policy";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox25);
            this.groupBox9.Controls.Add(this.textBox24);
            this.groupBox9.Controls.Add(this.textBox23);
            this.groupBox9.Controls.Add(this.label32);
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.label30);
            this.groupBox9.Controls.Add(this.textBox19);
            this.groupBox9.Controls.Add(this.label26);
            this.groupBox9.Location = new System.Drawing.Point(798, 255);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(319, 237);
            this.groupBox9.TabIndex = 12;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Average Values";
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(171, 166);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(100, 20);
            this.textBox25.TabIndex = 17;
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(171, 122);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(100, 20);
            this.textBox24.TabIndex = 16;
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(171, 73);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(100, 20);
            this.textBox23.TabIndex = 15;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 173);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(153, 13);
            this.label32.TabIndex = 14;
            this.label32.Text = "Avg.  DownTime During Repair";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 125);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(137, 13);
            this.label31.TabIndex = 13;
            this.label31.Text = "Avg. Cost Of RepairPerson";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(12, 76);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 13);
            this.label30.TabIndex = 12;
            this.label30.Text = "Avg. Cost Of Delay";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(171, 30);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(100, 20);
            this.textBox19.TabIndex = 11;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(12, 37);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(109, 13);
            this.label26.TabIndex = 10;
            this.label26.Text = "Avg. Cost Of Bearing";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBox17);
            this.groupBox7.Controls.Add(this.textBox16);
            this.groupBox7.Controls.Add(this.textBox15);
            this.groupBox7.Controls.Add(this.textBox14);
            this.groupBox7.Controls.Add(this.textBox13);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.label22);
            this.groupBox7.Controls.Add(this.label21);
            this.groupBox7.Controls.Add(this.label20);
            this.groupBox7.Location = new System.Drawing.Point(798, 3);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(300, 246);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Proposed Performance";
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(150, 193);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(100, 20);
            this.textBox17.TabIndex = 9;
            // 
            // textBox16
            // 
            this.textBox16.Location = new System.Drawing.Point(150, 152);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(100, 20);
            this.textBox16.TabIndex = 8;
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(150, 108);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(100, 20);
            this.textBox15.TabIndex = 7;
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(150, 68);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(100, 20);
            this.textBox14.TabIndex = 6;
            // 
            // textBox13
            // 
            this.textBox13.Location = new System.Drawing.Point(150, 29);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(100, 20);
            this.textBox13.TabIndex = 5;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(6, 200);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(56, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "Total Cost";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(2, 152);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(124, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "DownTime During Repair";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 111);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(111, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "Cost Of RepairPerson";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 68);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(99, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Cost Of Delay Time";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 32);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Cost Of Bearing";
            // 
            // ProposedTable
            // 
            this.ProposedTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ProposedTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column19,
            this.Column20,
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25,
            this.Column26});
            this.ProposedTable.Location = new System.Drawing.Point(0, 0);
            this.ProposedTable.Name = "ProposedTable";
            this.ProposedTable.Size = new System.Drawing.Size(775, 495);
            this.ProposedTable.TabIndex = 0;
            // 
            // Column19
            // 
            this.Column19.HeaderText = "NumOfChange";
            this.Column19.Name = "Column19";
            // 
            // Column20
            // 
            this.Column20.HeaderText = "BearingLifeTime1";
            this.Column20.Name = "Column20";
            // 
            // Column21
            // 
            this.Column21.HeaderText = "BearingLifeTime2";
            this.Column21.Name = "Column21";
            // 
            // Column22
            // 
            this.Column22.HeaderText = "BearingLifeTime3";
            this.Column22.Name = "Column22";
            // 
            // Column23
            // 
            this.Column23.HeaderText = "FirstFailure";
            this.Column23.Name = "Column23";
            // 
            // Column24
            // 
            this.Column24.HeaderText = "AccumulatedLife";
            this.Column24.Name = "Column24";
            // 
            // Column25
            // 
            this.Column25.HeaderText = "RandomDelay";
            this.Column25.Name = "Column25";
            // 
            // Column26
            // 
            this.Column26.HeaderText = "DelayTime";
            this.Column26.Name = "Column26";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.Draw_Chart_Current);
            this.tabPage4.Controls.Add(this.Clear_Chart_Current);
            this.tabPage4.Controls.Add(this.zedGraphControl1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1208, 495);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "ChartForCurrent";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // Draw_Chart_Current
            // 
            this.Draw_Chart_Current.Location = new System.Drawing.Point(203, 409);
            this.Draw_Chart_Current.Name = "Draw_Chart_Current";
            this.Draw_Chart_Current.Size = new System.Drawing.Size(117, 42);
            this.Draw_Chart_Current.TabIndex = 3;
            this.Draw_Chart_Current.Text = "Draw Chart";
            this.Draw_Chart_Current.UseVisualStyleBackColor = true;
            this.Draw_Chart_Current.Click += new System.EventHandler(this.Draw_Chart_Current_Click);
            // 
            // Clear_Chart_Current
            // 
            this.Clear_Chart_Current.Location = new System.Drawing.Point(499, 409);
            this.Clear_Chart_Current.Name = "Clear_Chart_Current";
            this.Clear_Chart_Current.Size = new System.Drawing.Size(117, 42);
            this.Clear_Chart_Current.TabIndex = 2;
            this.Clear_Chart_Current.Text = "Clear Chart";
            this.Clear_Chart_Current.UseVisualStyleBackColor = true;
            this.Clear_Chart_Current.Click += new System.EventHandler(this.Clear_Chart_Current_Click);
            // 
            // zedGraphControl1
            // 
            this.zedGraphControl1.Location = new System.Drawing.Point(76, 34);
            this.zedGraphControl1.Name = "zedGraphControl1";
            this.zedGraphControl1.ScrollGrace = 0D;
            this.zedGraphControl1.ScrollMaxX = 0D;
            this.zedGraphControl1.ScrollMaxY = 0D;
            this.zedGraphControl1.ScrollMaxY2 = 0D;
            this.zedGraphControl1.ScrollMinX = 0D;
            this.zedGraphControl1.ScrollMinY = 0D;
            this.zedGraphControl1.ScrollMinY2 = 0D;
            this.zedGraphControl1.Size = new System.Drawing.Size(653, 339);
            this.zedGraphControl1.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.Clear_Chart_Proposed);
            this.tabPage5.Controls.Add(this.Draw_Chart_Proposed);
            this.tabPage5.Controls.Add(this.zedGraphControl2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1208, 495);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ChartForProposed";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // Clear_Chart_Proposed
            // 
            this.Clear_Chart_Proposed.Location = new System.Drawing.Point(544, 421);
            this.Clear_Chart_Proposed.Name = "Clear_Chart_Proposed";
            this.Clear_Chart_Proposed.Size = new System.Drawing.Size(123, 40);
            this.Clear_Chart_Proposed.TabIndex = 2;
            this.Clear_Chart_Proposed.Text = "Clear Chart";
            this.Clear_Chart_Proposed.UseVisualStyleBackColor = true;
            this.Clear_Chart_Proposed.Click += new System.EventHandler(this.Clear_Chart_Proposed_Click);
            // 
            // Draw_Chart_Proposed
            // 
            this.Draw_Chart_Proposed.Location = new System.Drawing.Point(202, 421);
            this.Draw_Chart_Proposed.Name = "Draw_Chart_Proposed";
            this.Draw_Chart_Proposed.Size = new System.Drawing.Size(109, 40);
            this.Draw_Chart_Proposed.TabIndex = 1;
            this.Draw_Chart_Proposed.Text = "Draw Chart";
            this.Draw_Chart_Proposed.UseVisualStyleBackColor = true;
            this.Draw_Chart_Proposed.Click += new System.EventHandler(this.Draw_Chart_Proposed_Click);
            // 
            // zedGraphControl2
            // 
            this.zedGraphControl2.Location = new System.Drawing.Point(47, 26);
            this.zedGraphControl2.Name = "zedGraphControl2";
            this.zedGraphControl2.ScrollGrace = 0D;
            this.zedGraphControl2.ScrollMaxX = 0D;
            this.zedGraphControl2.ScrollMaxY = 0D;
            this.zedGraphControl2.ScrollMaxY2 = 0D;
            this.zedGraphControl2.ScrollMinX = 0D;
            this.zedGraphControl2.ScrollMinY = 0D;
            this.zedGraphControl2.ScrollMinY2 = 0D;
            this.zedGraphControl2.Size = new System.Drawing.Size(713, 362);
            this.zedGraphControl2.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 565);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LifeTime_dataGridView)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DelayTime_dataGridView)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Currentbearing3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Currentbearing2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentBearing1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProposedTable)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        //private System.Windows.Forms.Button Simulation_Click;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox LifeTimeDist_Mean_txtbox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox LifeTimeDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox LifeTimeDist_UniformA_txtbox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton LifeTimeDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton LifeTimeDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton LifetimeDist_Discrete_rbtn;
        private System.Windows.Forms.DataGridView LifeTime_dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn Demand;
        private System.Windows.Forms.DataGridViewTextBoxColumn Good;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox DelayDist_Mean_textBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox DelayDist_UniformB_txtbox;
        private System.Windows.Forms.TextBox Delay_Dist_UniformA_txtbox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton DelayDist_Uniform_rbtn;
        private System.Windows.Forms.RadioButton DelayDist_Exponential_rbtn;
        private System.Windows.Forms.RadioButton DelayDist_Discrete_rbtn;
        private System.Windows.Forms.DataGridView DelayTime_dataGridView;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView Currentbearing2;
        private System.Windows.Forms.DataGridView CurrentBearing1;
        private System.Windows.Forms.DataGridView Currentbearing3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView ProposedTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column26;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeOfNewsday;
        private System.Windows.Forms.DataGridViewTextBoxColumn Probability;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label26;
        private ZedGraph.ZedGraphControl zedGraphControl1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button Draw_Chart_Current;
        private System.Windows.Forms.Button Clear_Chart_Current;
        private System.Windows.Forms.Button Clear_Chart_Proposed;
        private System.Windows.Forms.Button Draw_Chart_Proposed;
        private ZedGraph.ZedGraphControl zedGraphControl2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
    }
}

