﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BearingSystem
{
    public class DiscreteDistribution : IDistribution
    {
        #region Loacl Members
        /// <summary>
        /// Density function
        /// </summary>
        private List<float> probabilities;
        /// <summary>
        /// Values 
        /// </summary>
        private List<float> values;
        /// <summary>
        /// Cumlative probabilities
        /// </summary>
        private List<float> cumulativeProbabilities;
        #endregion

        #region constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DiscreteDistribution"/> class.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <param name="probabilities">The probabilities.</param>
        public DiscreteDistribution(List<float> values, List<float> probabilities)
        {
            this.probabilities = probabilities;
            this.cumulativeProbabilities = new List<float>();
            this.values = values;
            float sum = 0;
            foreach (float prob in this.probabilities)
            {
                sum += prob;
                this.cumulativeProbabilities.Add(sum);
            }
        }
        #endregion

        #region IDistribution Members

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="randomVariable">The random variable.</param>
        /// <returns></returns>
        public float GetValue(float randomVariable)
        {
            int i;
            for (i = 0; randomVariable > this.cumulativeProbabilities[i]; i++);//Empty loop
            return this.values[i];
        }
        #endregion
    }
    
}
