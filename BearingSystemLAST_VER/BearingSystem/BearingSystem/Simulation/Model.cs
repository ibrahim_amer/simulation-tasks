﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
//using BearingSystem.Distributions;
using System.Threading.Tasks;


namespace BearingSystem
{
    public struct Bearinges_lifeTime
    {
        public List<int> lifeTime;
    }
    public class Model
    {

        public Bearinges_lifeTime[] all_lifeTimes;
        public int noOfHours;
        public IDistribution delayDist;
        public IDistribution lifeTimeDist;
        public int noOfRuns;
        int DownTimeCost;
        int RepairPersonCost;
        int BearingCost;
       // int CostOfDelayTime;
        public Model() { }
        public Model(int noOfHours, IDistribution delayDist, IDistribution lifeTimeDist, int noOfRuns,
            int DownTimeCost, int RepairPersonCost, int BearingCost)
        {
            this.noOfHours = noOfHours;  // 20,000
            this.lifeTimeDist = lifeTimeDist;
            this.delayDist = delayDist;
            all_lifeTimes = new Bearinges_lifeTime[3];
            this.noOfRuns = noOfRuns;
            this.DownTimeCost = DownTimeCost;
            this.RepairPersonCost = RepairPersonCost;
            this.BearingCost = BearingCost;
          //  this.CostOfDelayTime = CostOfDelayTime;
        }



    }

    public class currnt_model : Model
    {
        List<DataTable> ModelDataTable;
        int[] changes;
        int[] Delay;
        int[] accumlative_lifeTime;
        int ChangeOneBearingCost;
        //System Performance
        public float TotalCostOfBearings;
        public float totalCostOfDelayTime;
        public float totalCostOfDowenTimeDuringRepair;
        public float totalCostOfRepairPerson;
        public float Total_cost; // (sum of all the above)
        public currnt_model(int noOfHours, IDistribution delayDist, IDistribution lifeTimeDist, int noOfRuns, int DownTimeCost, int RepairPersonCost, int ChangeOneBearingCost,
             int BearingCost)
            : base(noOfHours, delayDist, lifeTimeDist, noOfRuns, DownTimeCost, RepairPersonCost,
               BearingCost)
        {
            InitializeModelDataTable();
            this.ChangeOneBearingCost = ChangeOneBearingCost;
            ModelDataTable = new List<DataTable>(3);
            changes = new int[3];
            Delay = new int[3];
            accumlative_lifeTime = new int[3];
            TotalCostOfBearings = 0;
            totalCostOfDelayTime = 0;
            totalCostOfDowenTimeDuringRepair = 0;
            totalCostOfRepairPerson = 0;
            Total_cost = 0;
        }


        private void Generate(int listIndex)
        {
            float RandomDelay = LCGRandomNumberGenerator.GetVariate();
            int delay = (int)delayDist.GetValue(RandomDelay);
            float RandomlifeTime = LCGRandomNumberGenerator.GetVariate();
            int lifeTime = (int)lifeTimeDist.GetValue(RandomlifeTime);
            Bearing b = new Bearing(lifeTime, 32);
            b.LifeTime = lifeTime;
            all_lifeTimes[listIndex].lifeTime.Add(lifeTime);
            b.DelayTime = delay;
            Delay[listIndex] += delay;
            b.noOfChange = changes[listIndex]++;
            accumlative_lifeTime[listIndex] += lifeTime;
            b.AccLifetime = accumlative_lifeTime[listIndex];
            this.AddRowToModelDataTable(listIndex, b, RandomlifeTime, RandomDelay);

        }

        public void AddRowToModelDataTable(int listIndex, Bearing b, float lifeTimeRN, float delayRN)
        {

            DataRow row;
            row = ModelDataTable[listIndex].NewRow();
            row["i"] = b.noOfChange;
            row["Rundom life Time"] = lifeTimeRN;
            row["life Time"] = b.LifeTime;
            row["Accumlative life time"] = b.AccLifetime;
            row["Rundom Delay Time"] = delayRN;
            row["Delay"] = b.DelayTime;


            ModelDataTable[listIndex].Rows.Add(row);
        }

        public void InitializeModelDataTable()
        {
            for (int i = 0; i < 3; i++)
            {
                ModelDataTable[i].Columns.Add("i");
                ModelDataTable[i].Columns.Add("Rundom life Time");
                ModelDataTable[i].Columns.Add("life Time");
                ModelDataTable[i].Columns.Add("Accumlative life time");
                ModelDataTable[i].Columns.Add("Rundom Delay Time");
                ModelDataTable[i].Columns.Add("Delay");
            }



        }

        public DataTable CalculateStatistics(int listIndex)  // 7an3mlha call 3 mraat fe gui :)
        {
            return this.ModelDataTable[listIndex];
        }

        public void Run()
        {

            for (int i = 0; i < 3; i++)
            {
                while (accumlative_lifeTime[i] < noOfHours)
                {
                    Generate(i);
                }
            }
        }
    }


    public class proposed_model : Model
    {
        DataTable ModelDataTable;
        int FirstFailure;
        int AccumulatedLife = 0;
        int ChangeThreeBearingCost;
        //System Performance
        public float TotalCostOfBearings;
        public float totalCostOfDelayTime;
        public float totalCostOfDowenTimeDuringRepair;
        public float totalCostOfRepairPerson;
        public float Total_cost; // (sum of all the above)
        public proposed_model(int noOfHours, IDistribution delayDist, IDistribution lifeTimeDist, int noOfRuns, int DownTimeCost, int RepairPersonCost, 
           int ChangeThreeBearingCost, int BearingCost)
            : base(noOfHours, delayDist, lifeTimeDist, noOfRuns, DownTimeCost, RepairPersonCost,
               BearingCost )
        {
            this.ChangeThreeBearingCost = ChangeThreeBearingCost;
            ModelDataTable = new DataTable();
            this.InitializeModelDataTable();

            TotalCostOfBearings = 0;
            totalCostOfDelayTime = 0;
            totalCostOfDowenTimeDuringRepair = 0;
            totalCostOfRepairPerson = 0;
            Total_cost = 0;
        }

        public void AddRowToModelDataTable(int NumOfChange, int LifeTime1, int LifeTime2, int LifeTime3, int FirstFailure, float DelayTime, int PersonDelay)
        {
            DataRow Row;
            Row = ModelDataTable.NewRow();
            Row["Num. Of Changed Bearing"] = NumOfChange;
            Row["LifeTime For First Bearing"] = LifeTime1;
            Row["LifeTime For Second Bearing"] = LifeTime2;
            Row["LifeTime For Third Bearing"] = LifeTime3;
            Row["First Failure"] = FirstFailure;
            Row["Accumulated Life"] = AccumulatedLife;
            Row["Rundom Delay Time"] = DelayTime * 10;
            Row["Delay"] = PersonDelay;
            ModelDataTable.Rows.Add(Row);
        }

        public void InitializeModelDataTable()
        {
            ModelDataTable.Columns.Add("Num. Of Changed Bearing");
            ModelDataTable.Columns.Add("LifeTime For First Bearing");
            ModelDataTable.Columns.Add("LifeTime For Second Bearing");
            ModelDataTable.Columns.Add("LifeTime For Third Bearing");
            ModelDataTable.Columns.Add("First Failure");
            ModelDataTable.Columns.Add("Accumulated Life");
            ModelDataTable.Columns.Add("Rundom Delay Time");
            ModelDataTable.Columns.Add("Delay");
        }

        public void Generate()
        {
            int NumOfChange = 0;
            int TotalDelay = 0;
            int index = 0;
            int FirstFailure;
            int LifeTime1 = 0, LifeTime2 = 0, LifeTime3 = 0;
            float DelayTime = LCGRandomNumberGenerator.GetVariate();
            int PersonDelay = (int)delayDist.GetValue(DelayTime);

            if (index < all_lifeTimes[0].lifeTime.Count)
            {
                LifeTime1 = all_lifeTimes[0].lifeTime[index];
            }
            else
            {
                LifeTime1 = GenerateLifeTime();
            }

            if (index < all_lifeTimes[1].lifeTime.Count)
            {
                LifeTime2 = all_lifeTimes[1].lifeTime[index];
            }
            else { LifeTime2 = GenerateLifeTime(); }

            if (index < all_lifeTimes[2].lifeTime.Count)
            {
                LifeTime3 = all_lifeTimes[2].lifeTime[index];
            }
            else { LifeTime3 = GenerateLifeTime(); }
            index++;
            TotalDelay += PersonDelay;
            NumOfChange++;
            FirstFailure = GetFirstFailure(LifeTime1, LifeTime2, LifeTime3);
            AccumulatedLife += FirstFailure;
            Run();
            AddRowToModelDataTable(NumOfChange, LifeTime1, LifeTime2, LifeTime3, FirstFailure, DelayTime, PersonDelay);

        }

        public int GetFirstFailure(int LifeTimee1, int LifeTimee2, int LifeTimee3)
        {
            FirstFailure = LifeTimee1;
            if (LifeTimee2 <= LifeTimee1)
                FirstFailure = LifeTimee2;
            else if (LifeTimee3 <= LifeTimee2)
                FirstFailure = LifeTimee3;
            return FirstFailure;
        }



        public int GenerateLifeTime()
        {
            float LifeTime = LCGRandomNumberGenerator.GetVariate();
            int BearingLifeTime = (int)lifeTimeDist.GetValue(LifeTime);
            return BearingLifeTime;
        }



        public void Run()
        {

            while (AccumulatedLife < noOfHours)
            {
                Generate();
            }
        }

        public DataTable CalculateStatistics()
        {
            return this.ModelDataTable;
        }

    }
}
