﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BearingSystem
{
    class LCGRandomNumberGenerator
    {
        static Random rand = new Random();
        
        public static float GetVariate()
        {
            return (float)rand.NextDouble();
        }
    }
}
