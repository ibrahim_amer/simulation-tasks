﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace BearingSystem
{
    class SimulationManager
    {
        List<currnt_model> Currnt_SystemModels;
        List<proposed_model> Propsed_SystemModels;

       int noOfHours; //(stopping condition)
        public IDistribution delayDist;
        public IDistribution lifeTimeDist;
         int Number_of_trials;
        int Bearing_Cost ;
      int Downtime_Cost ;
     int Repairperson_Cost;
     int CostOfDelayTime;
     int currntRepairTimePerBearing; // 7ay5talef men model le model 20 min (currnt) , 40 min (prposed);
     int prposedtRepairTimePerBearing;

     public SimulationManager(int noOfHours ,IDistribution delayDist, IDistribution lifeTimeDist, int Number_of_trials,
int Bearing_Cost, int Downtime_Cost, int Repairperson_Cost, int currntRepairTimePerBearing, int prposedtRepairTimePerBearing)
     {
         this.noOfHours = noOfHours;
         this.delayDist = delayDist;
         this.lifeTimeDist = lifeTimeDist;
         this.Number_of_trials = Number_of_trials;
         this.Bearing_Cost = Bearing_Cost;
         this.Downtime_Cost = Downtime_Cost;
         this.Repairperson_Cost = Repairperson_Cost;
        
         this.currntRepairTimePerBearing = currntRepairTimePerBearing;
         this.prposedtRepairTimePerBearing = prposedtRepairTimePerBearing;
        RunAll();
     }

     public void RunAll()
     {
         for (int i = 0; i < Number_of_trials; i++)
         {
             this.Currnt_SystemModels.Add(new currnt_model(noOfHours, delayDist, lifeTimeDist , Number_of_trials,  Downtime_Cost,  Repairperson_Cost,  currntRepairTimePerBearing,
              Bearing_Cost ));
             this.Propsed_SystemModels.Add(new proposed_model(noOfHours, delayDist, lifeTimeDist, Number_of_trials, Downtime_Cost, Repairperson_Cost, prposedtRepairTimePerBearing,
              Bearing_Cost));
         }

         for (int i = 0; i < this.Currnt_SystemModels.Count; i++)
         {
             this.Currnt_SystemModels[i].Run();
             this.Propsed_SystemModels[i].Run();
         }
     }
     public DataTable [] CalculateStatistics(int i)
     {
         
         DataTable [] ModelDataTable = new DataTable [4];
        ModelDataTable[0] = Currnt_SystemModels[i].CalculateStatistics(0);
        ModelDataTable[1] = Currnt_SystemModels[i].CalculateStatistics(1);
        ModelDataTable[2] = Currnt_SystemModels[i].CalculateStatistics(2);
        ModelDataTable[3]=  Propsed_SystemModels[i].CalculateStatistics();
        return ModelDataTable;
     }

    }
}
