﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
namespace BearingSystem
{
    class SimulationManager
    {
        public List<currnt_model> Currnt_SystemModels;
        public List<proposed_model> Propsed_SystemModels;

        int noOfHours; //(stopping condition)
        public IDistribution delayDist;
        public IDistribution lifeTimeDist;
        int Number_of_trials;
        int Bearing_Cost;
        int Downtime_Cost;
        float Repairperson_Cost;
      
        int currntRepairTimePerBearing; // 7ay5talef men model le model 20 min (currnt) , 40 min (prposed);
        int prposedtRepairTimePerBearing;

        public int SumTotalCostOfBearings=0;
        public int SumtotalCostOfDelayTime = 0;
        public int SumtotalCostOfDowenTimeDuringRepair = 0;
        public int SumtotalCostOfRepairPerson = 0;

        public void StartSimulation(int noOfHours, IDistribution delayDist, IDistribution lifeTimeDist, int Number_of_trials,
   int Bearing_Cost, int Downtime_Cost, int Repairperson_Cost, int currntRepairTimePerBearing, int prposedtRepairTimePerBearing)
        {
            this.noOfHours = noOfHours;
            this.delayDist = delayDist;
            this.lifeTimeDist = lifeTimeDist;
            this.Number_of_trials = Number_of_trials;
            this.Bearing_Cost = Bearing_Cost;
            this.Downtime_Cost = Downtime_Cost;
            this.Repairperson_Cost = Repairperson_Cost;
           
            Currnt_SystemModels = new List<currnt_model>();
            Propsed_SystemModels = new List<proposed_model>();
            this.currntRepairTimePerBearing = currntRepairTimePerBearing;
            this.prposedtRepairTimePerBearing = prposedtRepairTimePerBearing;
            RunAll();
        }

        public void RunAll()
        {
            for (int i = 0; i < Number_of_trials; i++)
            {
                this.Currnt_SystemModels.Add(new currnt_model(noOfHours, delayDist, lifeTimeDist, Number_of_trials, Downtime_Cost, Repairperson_Cost, currntRepairTimePerBearing,
                 Bearing_Cost));
                this.Propsed_SystemModels.Add(new proposed_model(noOfHours, delayDist, lifeTimeDist, Number_of_trials, Downtime_Cost, Repairperson_Cost, prposedtRepairTimePerBearing,
                 Bearing_Cost));
            }

            for (int i = 0; i < this.Currnt_SystemModels.Count; i++)
            {
                
                this.Currnt_SystemModels[i].Run();
                this.Propsed_SystemModels[i].Run();
                
                Model.all_lifeTimes = new Bearinges_lifeTime[3];
                Model.all_lifeTimes[0].lifeTime = new List<int>();
                Model.all_lifeTimes[1].lifeTime = new List<int>();
                Model.all_lifeTimes[2].lifeTime = new List<int>();
            }
        }

        public DataTable[] CalculateStatistics(int i)
        {
          
            DataTable[] ModelDataTable = new DataTable[4];
            ModelDataTable[0] = Currnt_SystemModels[i].CalculateStatistics(0);
            ModelDataTable[1] = Currnt_SystemModels[i].CalculateStatistics(1);
            ModelDataTable[2] = Currnt_SystemModels[i].CalculateStatistics(2);
            ModelDataTable[3] = Propsed_SystemModels[i].CalculateStatistics();
            return ModelDataTable;
        }



        public void CalculatePerformance()
        {
            for (int i = 0; i < this.Number_of_trials; i++)
            {
                this.Currnt_SystemModels[i].CalculateSystemPerformance();
                this.Propsed_SystemModels[i].CalculateSystemPerformance();
            }
        }


        public int[] DrawCurrentPolicyChart(out int[] arrRange)
        {

            double[] CostRange = new double[Number_of_trials];
            arrRange = new int[Number_of_trials + 1];
            
            for (int i = 0; i < Number_of_trials; i++)
            {
                CostRange[i] = Currnt_SystemModels[i].Total_cost; 
            }
            double max = CostRange[0];
            for (int i = 0; i < Number_of_trials; i++)
            {
                if (CostRange[i] > max)
                    max = CostRange[i];
               
            }

            int s = (int)max / Number_of_trials;
            int s2 = 0;
            int[] arr1 = new int[Number_of_trials+1];

            for (int i = 0; i <= Number_of_trials; i++)
            {
                arrRange[i] = s2;
                s2 += s;
            }
            for (int i = 0; i < Number_of_trials ; i++)
            {
                for (int j = 0; j < Number_of_trials; j++)
                {
                    if (CostRange[j] >= arrRange[i] && CostRange[j] < arrRange[i + 1])
                        arr1[i]++;
                }
            }
            return arr1;
        }

        public int[] DrawProposedPolicyChart(out int[] arrRange)
        {
            double[] CostRangeProposed = new double[Number_of_trials];
            arrRange = new int[Number_of_trials + 1];

            for (int i = 0; i < Number_of_trials; i++)
            {
                CostRangeProposed[i] = Propsed_SystemModels[i].Total_cost;
            }
            double max = CostRangeProposed[0];
            for (int i = 0; i < Number_of_trials; i++)
            {
                if (CostRangeProposed[i] > max)
                    max = CostRangeProposed[i];

            }

            int s = (int)max / Number_of_trials;
            int s2 = 0;
            int[] arr1 = new int[Number_of_trials + 1];

            for (int i = 0; i <= Number_of_trials; i++)
            {
                arrRange[i] = s2;
                s2 += s;
            }
            for (int i = 0; i < Number_of_trials; i++)
            {
                for (int j = 0; j < Number_of_trials; j++)
                {
                    if (CostRangeProposed[j] >= arrRange[i] && CostRangeProposed[j] < arrRange[i + 1])
                        arr1[i]++;
                }
            }
            return arr1;
        }
    }
}

//01006908825